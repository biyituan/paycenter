package com.choosefine.paycenter.account.service.impl;

import com.choosefine.paycenter.account.dao.ShiXiaoBaoTrxMapper;
import com.choosefine.paycenter.account.model.ShiXiaoBaoTrx;
import com.choosefine.paycenter.account.service.ShiXiaoBaoTrxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Comments：会影响我们账户总金额的都要记录下来(且明确是成功的交易)
 * Author：Jay Chang
 * Create Date：2017/3/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Service
public class ShiXiaoBaoTrxServiceImpl implements ShiXiaoBaoTrxService {
    @Autowired
    private ShiXiaoBaoTrxMapper shiXiaoBaoTrxMapper;

    @Override
    public int recordShixiaobaoTrx(ShiXiaoBaoTrx shiXiaoBaoTrx) {
        return  shiXiaoBaoTrxMapper.insertSelective(shiXiaoBaoTrx);
    }

    @Override
    public ShiXiaoBaoTrx selectByTradeSn(String tradeSn){
        return shiXiaoBaoTrxMapper.selectByTradeSn(tradeSn);
    }
}
