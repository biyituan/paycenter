package com.choosefine.paycenter.account.service.impl;

import com.choosefine.paycenter.account.dao.BankcardBinMapper;
import com.choosefine.paycenter.account.model.BankcardBin;
import com.choosefine.paycenter.account.service.BankcardBinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-23 11:23
 **/
@Service
public class BankcardBinServiceImpl implements BankcardBinService {
    public final static int[] BIN_DIGITS = new int[]{6, 9, 8, 7, 5, 10, 4, 3, 2};

    @Autowired
    private BankcardBinMapper bankcardBinMapper;

    @Override
    public BankcardBin findBankcardBinByCardNum(String bankcardNum) {
        for (int i = 0; i < BIN_DIGITS.length; i++) {
            String cardBin = bankcardNum.substring(0, BIN_DIGITS[i]);
            BankcardBin bankcardBin = bankcardBinMapper.selectBankcardBinByCardNum(cardBin);
            if (null != bankcardBin) {
                return bankcardBin;
            }
        }
        return null;
    }
}
