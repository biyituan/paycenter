package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.AccountBalanceLog;
import com.choosefine.paycenter.common.dao.BaseMapper;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface AccountBalanceLogMapper extends BaseMapper<AccountBalanceLog>{
}
