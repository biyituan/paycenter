package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.common.dao.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface AccountMapper extends BaseMapper<Account> {
    @Update("UPDATE paycenter_account SET pay_pass = NULL,salt = NULL WHERE id = #{id}")
    int updatePayPassAsNull(@Param("id") Long id);

    @Update("UPDATE paycenter_account SET mobile=#{mobile},name = #{name} WHERE user_code = #{userCode}")
    int bindMobile(@Param("userCode") String userCode, @Param("mobile") String mobile,@Param("name") String name);

    @Select("SELECT id FROM paycenter_account WHERE user_code = #{userCode}")
    Long selectAccountIdByUserCode(@Param("userCode") String userCode);

    @Select("SELECT id,user_code,name,real_name,balance,created_at,updated_at,role_id,role_name,`status`,mobile,pay_pass,salt,available_balance,freeze_balance FROM paycenter_account WHERE user_code = #{userCode}")
    Account selectByUserCode(@Param("userCode") String userCode);

    //这里的金额包括了（所有资金账号的冻结金+可用余额）
    @Select("SELECT sum(balance) FROM paycenter_account")
    BigDecimal selectTotalAmount();

    @Update("UPDATE paycenter_account SET status = #{status} WHERE id = #{id}")
    int updateStatusById(@Param("id") Long id, @Param("status") String status);

    //注意这里查询的是可用余额
    @Select("SELECT available_balance FROM paycenter_account WHERE id = #{id}")
    BigDecimal selectAvailableBalanceById(@Param("id") Long id);

    //注意这里查询的是总的余额
    @Select("SELECT balance FROM paycenter_account WHERE id = #{id}")
    BigDecimal selectBalanceById(@Param("id") Long id);

    /**
     *
     * @param id 账户id
     * @param balance 总余额
     * @return
     */
    @Update("UPDATE paycenter_account SET balance = #{balance} WHERE id = #{id}")
    int updateBalanceById(@Param("id") Long id, @Param("balance") BigDecimal balance);


    @Update("UPDATE paycenter_account SET pay_pass = #{payPass},salt = #{salt} WHERE id = #{id}")
    int updatePayPassById(@Param("id") Long id, @Param("payPass") String payPass, @Param("salt") String salt);

    @Select("SELECT count('x') FROM paycenter_account WHERE id = #{id}")
    int selectExistById(@Param("id") Long id);

    @Select("SELECT count('x') FROM paycenter_account WHERE user_code = #{userCode}")
    int selectExistByUserCode(@Param("userCode") String userCode);

    @Select("SELECT real_name FROM paycenter_account WHERE id = #{id}")
    String selectRealNameById(@Param("id") Long id);

    @Select("SELECT role_id FROM paycenter_account WHERE id = #{id}")
    Long selectRoleIdById(@Param("id") Long accountId);

    @Select("SELECT role_id FROM paycenter_account WHERE user_code = #{userCode}")
    Long selectRoleIdByUserCode(@Param("userCode") String userCode);

    @Select("SELECT user_code FROM paycenter_account WHERE name = #{name}")
    String selectUserCodeByAccountName(@Param("name") String name);

    @Select("SELECT available_balance FROM paycenter_account WHERE user_code = #{userCode}")
    BigDecimal selectBalanceByUserCode(@Param("userCode") String userCode);

    @Select("SELECT real_name FROM paycenter_account WHERE user_code = #{userCode}")
    String selectRealNameByUserCode(@Param("userCode") String userCode);

    @Update("UPDATE paycenter_account SET balance = #{balance},freeze_balance = #{freezeBalance},available_balance = #{availableBalance} WHERE id = #{acctId}")
    int updateAccountBalanceBeforWithDraw(@Param("acctId") Long acctId,@Param("balance")BigDecimal balance,@Param("freezeBalance")BigDecimal freezeBalance,@Param("availableBalance")BigDecimal availableBalance);

    /**
     *
     * @param id 账户id
     * @param availableBalance 可用余额
     * @param balance 总余额
     * @return
     */
    @Update("UPDATE paycenter_account SET available_balance = #{availableBalance},balance = #{balance} WHERE id = #{id}")
    int updateAvailableBalanceAndBalanceById(@Param("id") Long id, @Param("availableBalance") BigDecimal availableBalance,@Param("balance") BigDecimal balance);

    /**
     *
     * @param id 账户id
     * @param freezeBalance 冻结余额
     * @param availableBalance 可用余额
     * @param balance 总余额
     * @return
     */
    @Update("UPDATE paycenter_account SET freeze_balance = #{freezeBalance},available_balance = #{availableBalance},balance = #{balance} WHERE id = #{id}")
    int updateFreezeBalanceAndAvailableBalanceAndBalanceById(@Param("id") Long id, @Param("freezeBalance") BigDecimal freezeBalance,@Param("availableBalance") BigDecimal availableBalance,@Param("balance") BigDecimal balance);

    /**
     * 更新资金账号的账户名与手机号为NULL
     * @param userCode
     * @return
     */
    @Update("UPDATE paycenter_account SET name = NULL,mobile = NULL WHERE user_code = #{userCode} AND role_id = #{roleId}")
    int updateAccountNameAndMobileAsNullByUserCode(@Param("userCode") String userCode,@Param("roleId") Long roleId);

    /**
     * 更新资金账号的账户名与手机号为新的手机号与新的账户名
     * @param userCode
     * @return
     */
    @Update("UPDATE paycenter_account SET name = #{name},mobile = #{newMobile} WHERE user_code = #{userCode} AND role_id = #{roleId}")
    int updateAccountNameAndMobileByUserCode(@Param("name") String name,@Param("newMobile")String newMobile,@Param("userCode")String userCode, @Param("roleId")Long roleId);

    @Select("SELECT id,user_code,name,real_name,role_id,role_name,mobile,status,freeze_balance,available_balance,balance FROM paycenter_account WHERE user_code = #{userCode}")
    Account selectSimpleByUserCode(@Param("userCode") String userCode);
}
