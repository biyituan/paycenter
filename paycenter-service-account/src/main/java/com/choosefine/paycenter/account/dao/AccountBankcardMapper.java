package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.AccountBankcard;
import com.choosefine.paycenter.common.dao.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface AccountBankcardMapper extends BaseMapper<AccountBankcard> {
    @Select("SELECT count('x') FROM paycenter_account_bankcard WHERE account_id = #{accountId}")
    int selectBankcardNumByAccountId(@Param("accountId") Long accountId);

    @Select("SELECT * FROM paycenter_account_bankcard WHERE account_id = #{accountId} and valid_status=1")
    List<AccountBankcard> selectUsedBankcardByAccountId(@Param("accountId") Long accountId);

    @Update("UPDATE paycenter_account_bankcard SET is_default = 0 WHERE account_id = #{accountId}")
    int updateNoDefault(@Param("accountId") Long accountId);

    @Select("SELECT * FROM paycenter_account_bankcard WHERE id=#{bankCardId} and account_id = #{accountId}")
    AccountBankcard selectBankcardById(@Param("bankCardId") Long bankCardId,@Param("accountId") Long accountId);

    @Update("UPDATE paycenter_account_bankcard SET valid_status = 1  WHERE id=#{bankCardId} and account_id = #{accountId}")
    int validBankcardPass(@Param("bankCardId") Long bankCardId,@Param("accountId") Long accountId);

    //这里查询某个账号是否有有效的银行卡已经添加过，需要valid_status = 1 过滤
    @Select("SELECT COUNT(id) FROM paycenter_account_bankcard WHERE bankcard_no = #{bankcardNo} AND account_id = #{accountId} AND valid_status = 1")
    int selectCountByAccountIdAndBankcardNo(@Param("accountId")Long accountId,@Param("bankcardNo") String bankcardNo);

    //这里查询某个账号是否有银行卡已经添加过，不需要valid_status = 1 过滤
    @Select("SELECT COUNT(id) FROM paycenter_account_bankcard WHERE bankcard_no = #{bankcardNo} AND account_id = #{accountId}")
    int selectCountAllByAccountIdAndBankcardNo(@Param("accountId")Long accountId,@Param("bankcardNo") String bankcardNo);
}
