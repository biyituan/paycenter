package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.enums.BankcardTypeEnum;
import com.choosefine.paycenter.common.dao.BaseMapper;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface BankcardTypeMapper extends BaseMapper<BankcardTypeEnum> {
}
