package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.BankcardBin;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-23 11:24
 **/
public interface BankcardBinMapper {
    @Select("SELECT card_bin,bank_name,bank_id,card_name,card_type,bin_digits,card_digits From paycenter_bankcard_bin WHERE card_bin=#{cardBin}")
    BankcardBin selectBankcardBinByCardNum(@Param("cardBin") String cardBin);
}
