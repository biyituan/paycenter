package com.choosefine.paycenter.account.service.impl;

import com.choosefine.paycenter.account.dao.AccountBalanceLogMapper;
import com.choosefine.paycenter.account.dto.AccountBalanceLogDto;
import com.choosefine.paycenter.account.model.AccountBalanceLog;
import com.choosefine.paycenter.account.service.AccountBalanceLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtilsBean2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Service
public class AccountBalanceLogServiceImpl implements AccountBalanceLogService{

    @Autowired
    private AccountBalanceLogMapper accountBalanceLogMapper;

    public int recordAccountBalanceLog(AccountBalanceLog accountBalanceLog) {
        return accountBalanceLogMapper.insertSelective(accountBalanceLog);
    }
}
