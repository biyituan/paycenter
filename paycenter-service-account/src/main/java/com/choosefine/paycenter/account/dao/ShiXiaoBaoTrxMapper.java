package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.account.model.AccountBill;
import com.choosefine.paycenter.account.model.ShiXiaoBaoTrx;
import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.common.enums.BizzSys;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Jay Chang on 2017/3/7.
 */
public interface ShiXiaoBaoTrxMapper extends BaseMapper<ShiXiaoBaoTrx> {
    @ResultType(ShiXiaoBaoTrx.class)
    @Select("SELECT * FROM paycenter_shixiaobao_trx WHERE trade_sn = #{tradeSn}")
    ShiXiaoBaoTrx selectByTradeSn(@Param("tradeSn") String tradeSn);
}
