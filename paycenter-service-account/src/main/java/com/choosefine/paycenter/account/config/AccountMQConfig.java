package com.choosefine.paycenter.account.config;

import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.Properties;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Deprecated
//@Configuration
//@ImportResource(locations = {"classpath:config/account/spring-mq.xml"})
public class AccountMQConfig {

}
