package com.choosefine.paycenter.account.dao;

import com.choosefine.paycenter.common.enums.AccountBillStatus;
import com.choosefine.paycenter.account.model.AccountBill;
import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.FundFlow;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface AccountBillMapper extends BaseMapper<AccountBill> {

    @Update("UPDATE paycenter_account_bill SET status = #{status} WHERE bizz_sys = #{bizzSys} AND order_sn = #{orderSn}")
    int updateStatusByBizzSysAndOrderSn(@Param("bizzSys") BizzSys bizzSys, @Param("orderSn") String orderSn, @Param("status") AccountBillStatus accountBillStatus);

    @ResultType(AccountBill.class)
    @Select("SELECT bizz_sys,order_sn,main_account_id,main_account_real_name,main_account_user_code,opposite_account_id,opposite_account_user_code,opposite_account_real_name,trade_type,fund_flow,amount FROM paycenter_account_bill WHERE trade_sn = #{tradeSn} AND fund_flow = #{fund_flow}")
    List<AccountBill> selectByBizzSysAndOrderSnAndFundFlow(@Param("bizzSys") BizzSys bizzSys, @Param("orderSn") String orderSn, @Param("fund_flow") FundFlow fundFlow);

    @Select("SELECT id,main_account_user_code,status,bizz_sys,order_sn FROM paycenter_account_bill WHERE id = #{id}")
    AccountBill selectBizzSysAndOrderSnAndAccountBillStatusById(@Param("id") Long id);

    @ResultType(AccountBill.class)
    @Select("SELECT * FROM paycenter_account_bill WHERE bizz_sys = #{bizzSys} AND order_sn = #{orderSn}")
    List<AccountBill> selectByBizzSysAndOrderSn(@Param("bizzSys") BizzSys bizzSys, @Param("orderSn") String orderSn);

    @Select("SELECT bizz_sys,bizz_sn FROM paycenter_account_bill WHERE id = #{id}")
    AccountBill selectBizzSysAndBizzSnById(@Param("id") Long accountBillId);

    @Update("UPDATE paycenter_account_bill SET bank_name = #{bankName} WHERE bizz_sys = #{bizzSys} AND order_sn = #{orderSn}")
    int updateBankNameByBizzSysAndOrderSn(@Param("bizzSys") BizzSys bizzSys, @Param("orderSn") String orderSn,@Param("bankName") String bankName);

    int batchInsertAccountBill(@Param("accountBillList") List<AccountBill> accountBillList);
}
