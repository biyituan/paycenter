package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.pay.dao.CityMapper;
import com.choosefine.paycenter.pay.model.City;
import com.choosefine.paycenter.pay.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 15:04
 **/
@Service
public class CityServiceImpl implements CityService{
    @Autowired
    CityMapper cityMapper;

    public List<City> selectCityListByProvinceCode(String provinceCode){
        return cityMapper.selectCityListByProvinceCode(provinceCode);
    }
}
