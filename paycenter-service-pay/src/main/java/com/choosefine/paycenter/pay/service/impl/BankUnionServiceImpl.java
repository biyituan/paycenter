package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.pay.dao.BankUnionMapper;
import com.choosefine.paycenter.pay.dto.BankUnionDto;
import com.choosefine.paycenter.pay.model.BankUnion;
import com.choosefine.paycenter.pay.service.BankUnionService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 16:50
 **/
@Service
public class BankUnionServiceImpl implements BankUnionService {
    @Autowired
    BankUnionMapper bankUnionMapper;

    public List<BankUnion> selectBankUnionByCityCodeAndBankCode(String bankCode, String cityCode){
        List<BankUnion> bankUnionList = bankUnionMapper.selectBankUnionByCityCodeAndBankCode(bankCode, cityCode);
        return bankUnionList;
    }
}
