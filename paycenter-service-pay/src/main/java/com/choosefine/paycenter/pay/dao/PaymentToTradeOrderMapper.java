package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;
import com.choosefine.paycenter.pay.model.TradeOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:34
 **/
public interface PaymentToTradeOrderMapper extends BaseMapper<PaymentToTradeOrder>{
    @Select("SELECT order_sn from paycenter_payment_to_trade_order WHERE pay_sn=#{paySn}")
    List<String> selectOrderSnByPaySn(@Param("paySn") String paySn);

    @Select("SELECT pay_sn from paycenter_payment_to_trade_order WHERE bizz_sys=#{bizzSys} AND order_sn=#{orderSn}")
    String selectPaySnByBizzSysAndOrderSn(@Param("bizzSys")String bizzSys,@Param("orderSn")String orderSn);

    @Select("SELECT * FROM paycenter_payment_to_trade_order WHERE pay_sn = #{paySn}")
    PaymentToTradeOrder selectByPaySn(@Param("paySn") String paySn);

    int batchInsertPaymentToTradeOrder(@Param("paymentToTradeOrderList") List<PaymentToTradeOrder> paymentToTradeOrderList);

    @Select("SELECT * FROM paycenter_payment_to_trade_order WHERE bizz_sys=#{bizzSys} AND order_sn=#{orderSn}")
    List<PaymentToTradeOrder> findByOrderSn(@Param("bizzSys")String bizzSys,@Param("orderSn")String orderSn);
}
