package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.pay.api.PayConfigStorage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Setter
@Getter
@ToString
public abstract class AbstractPayConfigStorage  implements PayConfigStorage {
    /**商户id*/
    private String merchantId;
    /**签名方式*/
    private String signType;
    /**支付类型*/
    private String payType;
    /**公钥（如果所有支付类型使用的是同一个公钥，可以用这个字段）*/
    private String publicKey;

    /**该方法的默认实现，如果不同支付类型，使用不同的公钥，那么子类应该覆盖该方法*/
    public String getPublicKey(String payType){
        throw new UnsupportedOperationException();
    }
}
