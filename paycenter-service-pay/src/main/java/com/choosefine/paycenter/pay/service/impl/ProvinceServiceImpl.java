package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.pay.dao.ProvinceMapper;
import com.choosefine.paycenter.pay.model.Province;
import com.choosefine.paycenter.pay.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 15:04
 **/
@Service
public class ProvinceServiceImpl implements ProvinceService {
    @Autowired
    ProvinceMapper provinceMapper;

    public List<Province> provinceList() {
        return provinceMapper.provinceList();
    }

}
