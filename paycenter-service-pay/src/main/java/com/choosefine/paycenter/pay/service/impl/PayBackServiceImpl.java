package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.model.Recharge;
import com.choosefine.paycenter.account.service.AccountBalanceLogService;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.enums.PayStatus;
import com.choosefine.paycenter.common.enums.RechargeStatus;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.api.PayBackService;
import com.choosefine.paycenter.pay.api.RechargeService;
import com.choosefine.paycenter.pay.model.Payment;
import com.choosefine.paycenter.pay.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * TODO 改造成使用消息中间件解耦后，可删除
 * Comments：支付回调处理服务
 * Author：Jay Chang
 * Create Date：2017/4/6
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Service
@Deprecated
public class PayBackServiceImpl implements PayBackService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private AccountBalanceLogService accountBalanceLogService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private SerialNumberUtils serialNumberUtils;

    /**
     * 处理成功回调
     */
    @Transactional
    public void doProcess(String sn) {
        if (serialNumberUtils.isPaySn(sn)) {
            final Payment payment = paymentService.findByPaySn(sn);
            //支付单状态必须为待支付，才能进行以下操作
            if (PayStatus.WAIT_PAY.equals(payment.getStatus())) {
                //更新支付单状态为已支付
                updatePaymentPaidSuccess(payment.getPaySn());

                //TODO 发送支付完成的事务消息
                //TODO 由消息监听器去更新更新账单状态为已完成
                //updateAccountBillSuccess(payment.getPaySn());
                //TODO 记录施小包流水
                //shiXiaoBaoTrxService.recordShiXiaoBaoTransferTrx(payment.getPaySn(), payment.getAccountId(), payment.getAccountUserCode(), payment.getAccountName(), payment.getAmount(), payment.getCreatedAt());
                //TODO 记录到异步通知业务系统的任务记录

                //TODO 记录银行回调日志
            }
        } else if (serialNumberUtils.isRechargeSn(sn)) {
            final Recharge recharge = rechargeService.findByRechargeSn(sn);
            //充值单状态必须为待支付，才能进行以下操作
            if (RechargeStatus.WAITING.equals(recharge.getStatus())) {
                //更新充值单状态为已完成

                rechargeService.rechargeComplted(sn);

                //TODO 发送充值完成的事务消息
                //TODO 由account模块接受消息完成更新账单状态为已完成
                //accountBillService.updateAccountBillSuccess(sn);
                //TODO 余额变更由account模块接收消息完成余额变更，增加余额
//                Account account = accountService.findById(recharge.getAccountId());
//                BigDecimal oldBalance = account.getBalance();
//                BigDecimal newBalance = oldBalance.add(recharge.getAmount());
//                //更新账户余额
//                accountService.updateAccountBalance(recharge.getAccountId(), newBalance);
                //记录余额变更日志
                //accountBalanceLogService.recordAccountBalanceLog(recharge.getAccountId(), account.getUserCode(), oldBalance, newBalance);

                //TODO 只发消息。记录施小包流水（由account-service去记录）
                //shiXiaoBaoTrxService.recordShiXiaoBaoRechargeTrx(rechargeDto, account, recharge.getCreatedTime());
                //TODO 记录银行回调日志
            }
        }
    }

    /**
     * 处理失败回调
     */
    public void doFailureProcess(String sn) {
        if (serialNumberUtils.isPaySn(sn)) {
            final Payment payment = paymentService.findByPaySn(sn);
            //支付单状态必须为待支付，才能进行以下操作
            if (PayStatus.WAIT_PAY.equals(payment.getStatus())) {
                //更新支付单状态为付款失败
                //updatePaymentPaidFail(payment.getPaySn());
                //TODO 放到account模块处理 更新账单状态为付款失败
                //updateAccountBillFail(payment.getPaySn());
            }
        } else if (serialNumberUtils.isRechargeSn(sn)) {
            final Recharge recharge = rechargeService.findByRechargeSn(sn);
            //充值单状态必须为待支付，才能进行以下操作
            if (RechargeStatus.WAITING.equals(recharge.getStatus())) {
                //更新充值单状态为付款失败
                rechargeService.rechargeFailure(sn);
                //TODO 放到account模块处理，更新账单状态为付款失败
                //accountBillService.updateAccountBillFail(sn);
            }
        }
    }

    /**
     * 更新支付单状态为成功
     *
     * @param paySn
     */
    public void updatePaymentPaidSuccess(String paySn) {
        paymentService.updatePaymentPaidSuccess(paySn);
    }

    /**
     * 更新支付单状态为失败
     *
     * @param paySn
     */
    public void updatePaymentPaidFail(String paySn) {
        paymentService.updatePaymentPaidFailure(paySn);
    }
}

