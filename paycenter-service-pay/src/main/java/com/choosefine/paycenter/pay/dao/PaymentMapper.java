package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.PayStatus;
import com.choosefine.paycenter.pay.model.Payment;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface PaymentMapper extends BaseMapper<Payment>{

    @Select("SELECT pay_sn FROM paycenter_payment WHERE bizz_sn = #{bizzSn}")
    String selectPaySnByBizzSn(@Param("bizzSn") String bizzSn);

    @Select("SELECT count('x') FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn} AND status = #{payStatus}")
    int selectPaidCountByBizzSn(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn, @Param("payStatus") PayStatus payStatus);

    @Update("UPDATE paycenter_payment SET status = #{status},finished_at = UNIX_TIMESTAMP()*1000 WHERE pay_sn = #{paySn}")
    int updateStatusByPaySn(@Param("paySn") String paySn, @Param("status") PayStatus status);

    @Select("SELECT count('x') FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    int selectPaymentCountByBizz(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn);

    @Update("UPDATE paycenter_payment SET channel = #{channel},pay_type = #{payType} WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    int updatePayTypeByBizzSnAndBizzSys(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn, @Param("channel") String channel, @Param("payType") String payType);

    @Update("UPDATE paycenter_payment SET channel = #{channel},pay_type = #{payType},return_url = #{returnUrl} WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    int updatePayTypeAndReturnUrlByBizzSnAndBizzSys(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn, @Param("channel") String channel, @Param("payType") String payType, @Param("returnUrl") String returnUrl);

    @Select("SELECT * FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    Payment selectByBizzSysAndBizzSn(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn);

    @Select("SELECT * FROM paycenter_payment WHERE pay_sn = #{paySn}")
    Payment selectByPaySn(@Param("paySn") String paySn);

    @Select("SELECT amount FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    BigDecimal selectAmountByBizzSysAndBizzSn(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn);

    @Select("SELECT status FROM paycenter_payment WHERE pay_sn = #{paySn}")
    @ResultType(PayStatus.class)
    PayStatus selectStatusByPaySn(String paySn);

    @Update("UPDATE paycenter_payment SET stats = #{status} WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    int updateStatusByBizzSysAndBizzSn(BizzSys bizzSys, String bizzSn, PayStatus payStatus);

    @Select("SELECT status FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    PayStatus selectStatusById(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn);

    @Select("SELECT return_url FROM paycenter_payment WHERE pay_sn = #{paySn}")
    String selectReturnUrlByPaySn(@Param("paySn") String paySn);

    @Select("SELECT bizz_sn,bizz_sys FROM paycenter_payment WHERE pay_sn = #{paySn}")
    Payment selectBizzSnAndBizzSysByPaySn(@Param("paySn") String paySn);

    @Select("SELECT id FROM paycenter_payment WHERE pay_sn = #{paySn}")
    Long existsWithPaySn(@Param("paySn") String paySn);

    @Select("SELECT finished_at FROM paycenter_payment WHERE pay_sn = #{paySn}")
    Long selectFinishedAtByPaySn(@Param("paySn") String paySn);

    @Select("SELECT finished_at FROM paycenter_payment WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    Long selectFinishedAtByBizzSysAndOrderSn(@Param("bizzSys") String bizzSys, @Param("bizzSn") String bizzSn);
}
