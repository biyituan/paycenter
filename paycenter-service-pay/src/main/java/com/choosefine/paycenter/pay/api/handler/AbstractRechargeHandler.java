package com.choosefine.paycenter.pay.api.handler;

import com.aliyun.openservices.ons.api.Producer;
import com.choosefine.paycenter.account.dto.RechargeDto;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.service.AccountBillService;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.constants.ExceptionCodeConstants;
import com.choosefine.paycenter.common.constants.ExceptionMessageConstants;
import com.choosefine.paycenter.common.exception.BusinessException;
import com.choosefine.paycenter.common.utils.MessageSourceUtils;
import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.RechargeService;
import com.choosefine.paycenter.pay.dto.PayOrderDto;
import com.choosefine.paycenter.pay.vo.PayResultVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Comments：充值处理抽象类
 * Author：Jay Chang
 * Create Date：2017/4/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Component
public abstract class AbstractRechargeHandler implements RechargeHandler {
    @Autowired
    private RechargeService rechargeService;
    @Autowired
    private AccountBillService accountBillService;
    @Autowired
    private AccountService accountService;

    @Autowired
    private MessageSourceUtils messageSourceUtils;

    protected PayService payService;

    @Autowired
    @Qualifier("delayCloseMessageProducer")
    private Producer delayCloseProducer;

    @Value("${aliyun.ons.topic.topicPay}")
    private String topicPay;

    @Value("${aliyun.ons.topic.topicDelayClose}")
    private String topicDelayClose;

    //延迟关闭的小时数单位
    @Value("${sys.pay.delayCloseHour}")
    private long delayCloseHour;

    @Transactional
    public PayResultVo doProcess(RechargeDto rechargeDto) {
        //必须传前端回调url
        if (rechargeDto.getPayType().indexOf("_NB") >= 0 && StringUtils.isBlank(rechargeDto.getReturnUrl())) {
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_CURRENT_PAYTYPE_MUST_SET_RETURN_URL, messageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_CURRENT_PAY_TYPE_MUST_SET_RETURN_URL));
        }

        Account account = accountService.findSimpleByUserCode(rechargeDto.getUserCode());

        //记录recharge记录
        rechargeService.saveRecharge(rechargeDto,account);

        //记录账单(仅在支付完成后再记录账单)
        //accountBillService.recordAccountBill(rechargeDto, account);

        //充值实质也是支付，这里也创建PayOrder实例
        PayOrderDto payOrderDto = new PayOrderDto(rechargeDto.getRSn(), rechargeDto.getAmount());
        PayResultVo payResultVo = new PayResultVo();
        payResultVo.setNeedRedirect(true);
        String gotoBankUrl = payService.buildRequest(payOrderDto, rechargeDto.getPayType());
        payResultVo.setUrl(gotoBankUrl);
        payResultVo.setAmount(rechargeDto.getAmount());
        payResultVo.setSn(rechargeDto.getRSn());
        return payResultVo;
    }

    public abstract void init();
}
