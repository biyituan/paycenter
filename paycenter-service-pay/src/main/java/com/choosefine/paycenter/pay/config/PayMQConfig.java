package com.choosefine.paycenter.pay.config;

import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.transaction.TransactionProducer;
import com.choosefine.paycenter.pay.mq.PayLocalTransactionCheckerImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.Properties;

/**
 * 支付事务消息生产者
 */
@Configuration
@ImportResource(locations = {"classpath:conf/pay/spring-mq.xml"})
public class PayMQConfig {
    @Value("${aliyun.ons.accessKey}")
    private String accessKey;

    @Value("${aliyun.ons.secrect}")
    private String secrect;

    @Value("${aliyun.ons.onsAddr}")
    private String onsAddr;

    @Value("${aliyun.ons.producer.producerPay}")
    private String producerId;

    @Bean
    public TransactionProducer createTransactionProducer(PayLocalTransactionCheckerImpl payLocalTransactionChecker){
        Properties properties = new Properties();
        //TODO 抽离到配置文件中
        properties.put(PropertyKeyConst.ProducerId, producerId); // 您在控制台创建的Producer ID
        properties.put(PropertyKeyConst.AccessKey, accessKey); // 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, secrect); // 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.ONSAddr,onsAddr);//使用公测环境
        //PropertyKeyConst.ONSAddr地址请根据实际情况对应以下几类进行输入：
        //公共云生产环境：http://onsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal
        //公共云公测环境：http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet
        //杭州金融云环境：http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal
        //杭州深圳云环境：http://mq4finance-sz.addr.aliyun.com:8080/rocketmq/nsaddr4client-internal
        //亚太东南1公共云环境（只适用于新加坡ECS）：http://ap-southeastaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal
        TransactionProducer producer = ONSFactory.createTransactionProducer(properties,payLocalTransactionChecker);
        producer.start();
        return producer;
    }
}
