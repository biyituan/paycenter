package com.choosefine.paycenter.pay.api.dispatcher.impl;

import com.choosefine.paycenter.pay.api.dispatcher.PayDispatcher;
import com.choosefine.paycenter.pay.api.handler.AbstractPayBackHandler;
import com.choosefine.paycenter.pay.api.handler.AbstractPayHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * Comments：支付通道分发器
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class PayDispatcherImpl implements PayDispatcher,BeanFactoryAware{

    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory){
        this.beanFactory = beanFactory;
    }

    public AbstractPayHandler getPayHandler(String channel) {
        if(StringUtils.isBlank(channel)){return null;}
        return beanFactory.getBean(channel.toUpperCase()+PAY_HANDLER_PREFIX,AbstractPayHandler.class);
    }

    public AbstractPayBackHandler getPayBackHandler(String channel) {
        if(StringUtils.isBlank(channel)){return null;}
        return beanFactory.getBean(channel.toUpperCase()+PAY_BACK_HANDLER_PREFIX,AbstractPayBackHandler.class);
    }
}
