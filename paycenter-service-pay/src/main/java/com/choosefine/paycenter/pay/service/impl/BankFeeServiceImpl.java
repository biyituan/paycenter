package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.common.exception.BusinessException;
import com.choosefine.paycenter.pay.constants.PayConstants;
import com.choosefine.paycenter.pay.dao.BankFeeMapper;
import com.choosefine.paycenter.pay.model.BankFee;
import com.choosefine.paycenter.pay.service.BankFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 银行手续费相关
 * Created by dyy on 2017/7/10.
 */
@Service
public class BankFeeServiceImpl implements BankFeeService{

    @Autowired
    BankFeeMapper bankFeeMapper;

    @Override
    public BigDecimal getBankFee(Integer rateType,String bankType,BigDecimal operAmount) throws  BusinessException{
        BankFee bankFee = bankFeeMapper.selectByBankType(bankType);
        final int rateTypeValue = rateType.intValue();
        if(rateTypeValue == PayConstants.RATETYPE_CONFIRMED_FEE){ //每笔收取固定手续费(元)
            return bankFee.getConfirmedFee();
        }
        if(rateTypeValue == PayConstants.RATETYPE_SAME_DEBITCARD_FEE){//同行借记卡充值手续费率%
           return bankFee.getSameDebitcardFee().divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP).multiply(operAmount);
        }
        if(rateTypeValue == PayConstants.RATETYPE_DIFFERENT_DEBITCARD_FEE){//非同行借记卡充值手续费率%
            return bankFee.getDifferentDebitcardFee().divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP).multiply(operAmount);
        }
        if(rateTypeValue == PayConstants.RATETYPE_SAME_CREDITCARD_FEE){//同行贷记卡充值手续费率%
            return bankFee.getSameCreditcardFee().divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP).multiply(operAmount);
        }
        if(rateTypeValue == PayConstants.RATETYPE_DIFFERENT_CREDITCARD_FEE){//非同行贷记卡充值手续费率%
            return bankFee.getDifferentCreditcardFee().divide(new BigDecimal(100),4,BigDecimal.ROUND_HALF_UP).multiply(operAmount);
        }
        if(rateTypeValue == PayConstants.RATETYPE_CASH_COMMISSION){//同行提现手续费(元)
            return bankFee.getCashCommission();
        }
        if(rateTypeValue == PayConstants.RATETYPE_NON_ATTENDANCE_RATE){//非同行提现手续费(元)
            return bankFee.getNonAttendanceRate();
        }
        else {
            throw new BusinessException(1005, "未配置该类型的费率");
        }
    }
}
