package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.City;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:37
 **/
public interface CityMapper extends BaseMapper<City> {
    @Select("Select area_code,city_name FROM paycenter_bank_city WHERE province_code=#{provinceCode} ORDER BY area_code ASC")
    public List<City> selectCityListByProvinceCode(@Param("provinceCode") String provinceCode);
}
