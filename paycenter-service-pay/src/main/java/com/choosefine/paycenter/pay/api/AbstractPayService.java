package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.common.utils.encrypt.SignUtils;
import com.choosefine.paycenter.pay.dto.PayOrderDto;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
public abstract class AbstractPayService implements PayService {

    protected PayConfigStorage payConfigStorage;

    public PayConfigStorage getPayConfigStorage() {
        return payConfigStorage;
    }

    /***
     * 初始化PayConfigStorage
     */
    public abstract void init();

    @Override
    public PayService setPayConfigStorage(PayConfigStorage payConfigStorage){this.payConfigStorage = payConfigStorage;return this;}

    @Override
    public String createSign(String sourceStr) {
        String signType = getPayConfigStorage().getSignType();
        if(StringUtils.isBlank(signType)){
            throw new RuntimeException("signType must be set in PayConigStorage object");
        }
        return SignUtils.valueOf(signType.toUpperCase()).sign(sourceStr);
    }

    @Override
    public PayOrderDto getPayOrderFromBackParam(Map<String, String> params, String payType){
        throw new UnsupportedOperationException("please override this method");
    }
}
