package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.PaymentChannel;

/**
 * Created by Jay Chang on 2017/3/4.
 */
public interface PaymentChannelMapper extends BaseMapper<PaymentChannel> {
}
