package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.Province;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:38
 **/
public interface ProvinceMapper extends BaseMapper<Province> {
    @Select("SELECT province_code,province_name FROM paycenter_bank_province order by sort")
    public List<Province> provinceList();
}
