package com.choosefine.paycenter.pay.api.dispatcher.impl;

import com.choosefine.paycenter.pay.api.dispatcher.RechargeDispatcher;
import com.choosefine.paycenter.pay.api.handler.RechargeHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * Comments：充值处理分发器
 * Author：Jay Chang
 * Create Date：2017/4/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class RechargeDispatcherImpl implements RechargeDispatcher,BeanFactoryAware {
    private BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
       this.beanFactory = beanFactory;
    }

    public RechargeHandler getRechargeHandler(String channel) {
        return beanFactory.getBean(channel.toUpperCase()+RECHARGE_HANDLER_PREFIX,RechargeHandler.class);
    }
}
