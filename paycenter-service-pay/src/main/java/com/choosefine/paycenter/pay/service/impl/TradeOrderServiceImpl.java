package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.constants.ExceptionCodeConstants;
import com.choosefine.paycenter.common.constants.ExceptionMessageConstants;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeOrderStatus;
import com.choosefine.paycenter.common.exception.BusinessException;
import com.choosefine.paycenter.common.utils.MessageSourceUtils;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.dao.TradeOrderMapper;
import com.choosefine.paycenter.pay.model.TradeOrder;
import com.choosefine.paycenter.pay.service.TradeOrderService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:53
 **/
@Slf4j
@Service
public class TradeOrderServiceImpl implements TradeOrderService {
    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Autowired
    private MessageSourceUtils localeMessageSourceUtils;

    @Override
    public boolean isComplete(String bizzSys, String orderSn) {
        TradeOrder tradeOrder=tradeOrderMapper.selectByBizzSysAndOrderSnAndStatus(bizzSys,orderSn, TradeOrderStatus.PAID.toString());
        if(null!=tradeOrder){
            return true;
        }
        return false;
    }

    @Override
    public int recordTradeOrder(PaymentDto paymentDto) {
        List<TradeOrder> batchInsertTradeOrderList = Lists.newArrayList();
        for (int i=0;i<paymentDto.getOrders().size();i++){
            OrderDto orderDto=paymentDto.getOrders().get(i);
            TradeOrder tradeOrder=new TradeOrder();
            tradeOrder.setUserCode(orderDto.getUserCode());
            tradeOrder.setAmount(orderDto.getAmount());
            tradeOrder.setBizzSys(paymentDto.getBizzSys());
            tradeOrder.setBizzSn(paymentDto.getBizzSn());
            if(serialNumberUtils.isTransferBizzSn(paymentDto.getBizzSn())){
                //如果为转账那么bizzSn与orderSn相同
                tradeOrder.setOrderSn(paymentDto.getBizzSn());
            }else {
                tradeOrder.setOrderSn(orderDto.getOrderSn());
            }
            tradeOrder.setStatus(TradeOrderStatus.WAIT_PAY);
            tradeOrder.setTradeMemo(orderDto.getTradeMemo());
            TradeOrder tradeOrderCheck = tradeOrderMapper.selectByBizzSysAndOrderSn(paymentDto.getBizzSys(),orderDto.getOrderSn());
            if(null == tradeOrderCheck){
                batchInsertTradeOrderList.add(tradeOrder);
            }
            if(null != tradeOrderCheck && TradeOrderStatus.CLOSED.equals(tradeOrderCheck.getStatus())){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_TRADEORDER_IS_ALREADY_CLOSED, localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_TRADEORDER_IS_ALREADY_CLOSED,new String[]{orderDto.getOrderSn()}));
            }
        }
        if(CollectionUtils.isNotEmpty(batchInsertTradeOrderList)) {
            return tradeOrderMapper.batchInsertTradeOrder(batchInsertTradeOrderList);
        }
        return 0;
    }

    @Override
    public TradeOrder findTradeOrderByBizzSysAndOrderSn(BizzSys bizzSys, String orderSn) {
        return tradeOrderMapper.selectByBizzSysAndOrderSn(bizzSys,orderSn);
    }

    @Override
    public List<TradeOrder> findOrdersByBizzSysAndOrderSns(BizzSys bizzSys, List<String> orderSns) {
        return tradeOrderMapper.selectByBizzSysAndOrderSns(bizzSys,orderSns);
    }

    @Override
    public List<TradeOrder> findOrdersByBizzSysAndBizzSn(BizzSys bizzSys, String bizzSn) {
        return tradeOrderMapper.selectByBizzSysAndBizzSn(bizzSys,bizzSn);
    }

    @Override
    public int tradeOrderPaid(String bizzSys, String orderSn) {
        return tradeOrderMapper.updateTradeOrderStatusByBizzSysAndOrderSn(bizzSys,orderSn,TradeOrderStatus.PAID);
    }

    @Override
    public int updateAsClosedStatusByBizzSysAndOrderSn(BizzSys bizzSys, String orderSn) {
        return tradeOrderMapper.updateTradeOrderStatusByBizzSysAndOrderSn(bizzSys.toString(),orderSn,TradeOrderStatus.CLOSED);
    }
}
