package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.BankFee;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.math.BigDecimal;

/**
 * 银行手续费相关
 * Created by dyy on 2017/7/10.
 */
public interface BankFeeMapper extends BaseMapper<BankFee> {
    @Select("SELECT * FROM paycenter_bank_fee WHERE bank_type = #{bankType}")
    BankFee selectByBankType(@Param("bankType") String bankType);

    @Select("SELECT * FROM paycenter_bank_fee WHERE bank_type = #{bankType} and amount_range_min<=#{amountRangeMin} and amount_range_max>=#{amountRangeMax}")
    BankFee selectByBankTypeAndAmountRange(@Param("bankType") String bankType, @Param("amountRangeMin") BigDecimal amountRangeMin, @Param("amountRangeMax") BigDecimal amountRangeMax);
}
