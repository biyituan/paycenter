package com.choosefine.paycenter.pay.api.handler;

import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.vo.PayResultVo;
import java.lang.reflect.InvocationTargetException;

/**
 * Comments：支付通道处理器抽象类(对接具体的支付通道需继承该类，实现相应的抽象方法)
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public abstract class AbstractPayAgencyPayHandler extends AbstractPayHandler{

    @Override
    public PayResultVo doProcessSpecific(PayBizzRequestDto payBizzRequest) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //生成跳转到支付通道（银行的）url
        String redirectuUrl = getGotoUrl(payBizzRequest);
        //生成结果vo
        return getPayResultVo(payBizzRequest, redirectuUrl);
    }

    private PayResultVo getPayResultVo(PayBizzRequestDto payBizzRequest, String redirectuUrl) {
        PayResultVo payResultVo = new PayResultVo();
        payResultVo.setAmount(payBizzRequest.getAmount());
        payResultVo.setSn(payBizzRequest.getPaySn());
        payResultVo.setNeedRedirect(true);
        payResultVo.setUrl(redirectuUrl);
        return payResultVo;
    }

    protected abstract String getGotoUrl(PayBizzRequestDto payBizzRequest) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException;
}
