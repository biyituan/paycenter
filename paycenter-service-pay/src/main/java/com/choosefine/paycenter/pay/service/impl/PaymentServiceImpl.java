package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.account.enums.RoleType;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.AccountBankcard;
import com.choosefine.paycenter.account.model.AccountBill;
import com.choosefine.paycenter.account.service.AccountBankcardService;
import com.choosefine.paycenter.account.service.AccountBillService;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.account.vo.AccountBankcardVo;
import com.choosefine.paycenter.common.constants.ExceptionCodeConstants;
import com.choosefine.paycenter.common.constants.ExceptionMessageConstants;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.FundFlow;
import com.choosefine.paycenter.common.enums.PayStatus;
import com.choosefine.paycenter.common.enums.TradeType;
import com.choosefine.paycenter.common.exception.BusinessException;
import com.choosefine.paycenter.common.utils.MessageSourceUtils;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.api.dispatcher.PayDispatcher;
import com.choosefine.paycenter.pay.api.handler.PayHandler;
import com.choosefine.paycenter.pay.dao.PaymentMapper;
import com.choosefine.paycenter.pay.dto.ChoosePayTypeDto;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.dto.PayDto;
import com.choosefine.paycenter.pay.model.Payment;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;
import com.choosefine.paycenter.pay.model.TradeOrder;
import com.choosefine.paycenter.pay.service.PayVerifyService;
import com.choosefine.paycenter.pay.service.PaymentService;
import com.choosefine.paycenter.pay.vo.PayFromAccountBillResultVo;
import com.choosefine.paycenter.pay.service.PaymentToTradeOrderService;
import com.choosefine.paycenter.pay.service.TradeOrderService;
import com.choosefine.paycenter.pay.vo.PayInfoVo;
import com.choosefine.paycenter.pay.vo.PayResultVo;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.management.relation.Role;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Comments：支付单服务
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountBillService accountBillService;

    @Autowired
    private AccountBankcardService accountBankcardService;

    @Autowired
    private PayVerifyService payVerifyService;

    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private PaymentToTradeOrderService paymentToTradeOrderService;

    @Autowired
    private PayDispatcher payDispatcher;

    @Autowired
    private MessageSourceUtils localeMessageSourceUtils;

    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Value("${aliyun.ons.topic.topicPay}")
    private String topicPay;

    /**
     * 创建支付单
     * @param paymentDto
     * @return
     */
    @Transactional
    public String createPayment(PaymentDto paymentDto){
        //判断该支付单中的所有订单是否都是支付完成的
       final List<OrderDto> ordersDtoList = paymentDto.getOrders();

        //查询交易主体账号信息
        Account mainAccount = accountService.findSimpleByUserCode(paymentDto.getUserCode());
        paymentDto.setAccountId(mainAccount.getId());
        paymentDto.setAccountRealName(mainAccount.getRealName());
        paymentDto.setAccountMobile(mainAccount.getMobile());

        final Long roleId = mainAccount.getRoleId();
        final Map<String, String> allTradeType = accountBillService.findAllTradeTypeByRoleId(roleId);
        if(!allTradeType.containsKey(paymentDto.getTradeType().name())){
            //由于分包单位与建筑公司的PAY_AGENT与SHOPPING是合并的，这里需要特殊处理
            if(!((roleId.longValue() == RoleType.CC.getValue().longValue() || roleId.longValue() == RoleType.SU.getValue().longValue()) && paymentDto.getTradeType().equals(TradeType.PAY_AGENT))){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_CURRENT_ACCOUNT_CANNOT_CREATE_THIS_TRADE_TYPE,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_CURRENT_ACCOUNT_CANNOT_CREATE_THIS_TRADE_TYPE,new Object[]{mainAccount.getRoleName(),paymentDto.getTradeType().getName()}));
            }
        }
        //校验交易主体账号是否存在,是否冻结和锁定(公共的前置检查)
        payVerifyService.checkMainAccountExistAndIsBlocked(mainAccount);
        if(CollectionUtils.isEmpty(paymentDto.getOrders())){
            throw new BusinessException(ExceptionCodeConstants.PAY_ACCOUNT_TRADEORDER_CANNOT_EMPTY,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAY_ACCOUNT_TRADEORDER_CANNOT_EMPTY));
        }
        for(OrderDto orderDto : paymentDto.getOrders()){
            boolean isComplete=tradeOrderService.isComplete(paymentDto.getBizzSys().toString(),orderDto.getOrderSn());
            if(isComplete){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_HAS_ORDER_IS_PAID,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_HAS_ORDER_IS_PAID));
            }
            if(BigDecimal.ZERO.compareTo(orderDto.getAmount()) == 0){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_BIZZ_ORDER_AMOUNT_CANNOT_BE_ZERO,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_BIZZ_ORDER_AMOUNT_CANNOT_BE_ZERO));
            }
            Account oppositeAccount = accountService.findSimpleByUserCode(orderDto.getUserCode());
            if(null == oppositeAccount){
                throw new BusinessException(ExceptionCodeConstants.ACCOUNT_CHECK_NOTEXIST,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.ACCOUNT_CHECK_NOTEXIST));
            }
            //校验不能自己付给自己
            if(mainAccount.getUserCode().equals(oppositeAccount.getUserCode())){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_CANNOT_PAY_TO_SELF,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_CANNOT_PAY_TO_SELF));
            }
            //转账的收款人只能是工人角色
            if(TradeType.TRANSFER.equals(paymentDto.getTradeType())){
                if(!serialNumberUtils.isTransferBizzSn(paymentDto.getBizzSn())){
                    throw new BusinessException(ExceptionCodeConstants.PAYMENT_NOT_VALID_TRANSFER_BIZZ_SN,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_NOT_VALID_TRANSFER_BIZZ_SN,new Object[]{paymentDto.getBizzSn()}));
                }
                if((RoleType.CL.getValue().longValue() == oppositeAccount.getRoleId().longValue() || RoleType.MG.getValue().longValue() == oppositeAccount.getRoleId().longValue())){
                    throw new BusinessException(ExceptionCodeConstants.PAYMENT_RECEIVER_NOT_MATCH,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_RECEIVER_NOT_MATCH));
                }
                List<OrderDto> transferOrders = paymentDto.getOrders();
                if(!CollectionUtils.isEmpty(transferOrders) && transferOrders.size() >= 2){
                    throw new BusinessException(ExceptionCodeConstants.PAYMENT_RECEIVER_ONLY_ONE,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_RECEIVER_ONLY_ONE));
                }
            }
            orderDto.setAccountId(oppositeAccount.getId());
            orderDto.setAccountMobile(oppositeAccount.getMobile());
            orderDto.setAccountRealName(oppositeAccount.getRealName());
        }
        //校验某个业务流水号的支付单是否已经创建了,是否已经支付完成了(避免重复支付同一笔业务流水)
        payVerifyService.checkPaymentExistsAndAlreadyPaid(paymentDto.getBizzSys(),paymentDto.getBizzSn());
        //计算交易总金额
        BigDecimal totalAmount = calculateTradeTotalAmount(paymentDto);
        if(totalAmount.compareTo(paymentDto.getAmount()) != 0){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_TRADE_AMOUNT_NOT_MATCH,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_TRADE_AMOUNT_NOT_MATCH));
        }

        //生成支付订单
        Payment payment = new Payment();
        BeanUtils.copyProperties(paymentDto,payment);
        //生成支付单号(支付单流水号)
        payment.setPaySn(serialNumberUtils.getPaySn());
        payment.setAccountRealName(paymentDto.getAccountRealName());
        payment.setAccountUserCode(paymentDto.getUserCode());
        paymentMapper.insertSelective(payment);

        //生成账单
        accountBillService.recordAccountBill(paymentDto,payment.getPaySn());

        //记录交易订单(转账的话，也记录下，只不过是没有用的，通用处理)
        tradeOrderService.recordTradeOrder(paymentDto);
        //记录支付流水业务订单关联表
        paymentToTradeOrderService.recordPaymentToTradeOrder(paymentDto, payment.getPaySn());

        return payment.getPaySn();
    }

    @Override
    public Payment findBizzSnAndBizzSysByPaySn(String tradeSn) {
        return  paymentMapper.selectBizzSnAndBizzSysByPaySn(tradeSn);
    }

    @Override
    public int savePayment(Payment payment) {
        return paymentMapper.insertSelective(payment);
    }

    @Override
    public Long findFinishedAtByPaySn(String paySn) {
        return paymentMapper.selectFinishedAtByPaySn(paySn);
    }

    @Override
    public Long findFinishedAtByBizzSysAndOrderSn(String bizzsys,String ordersn){
        return paymentMapper.selectFinishedAtByBizzSysAndOrderSn(bizzsys,ordersn);
    }

    @Override
    @Transactional
    public PayFromAccountBillResultVo createPayment(String userCode, Long accountBillId) {
        AccountBill accountBill = accountBillService.findAccountBillById(accountBillId);
        final BizzSys bizzSys = accountBill.getBizzSys();
        final String orderSn = accountBill.getOrderSn();
        //校验是否已经支付完成了
        boolean isComplete=tradeOrderService.isComplete(bizzSys.toString(),orderSn);
        if(isComplete){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_HAS_ORDER_IS_PAID,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_HAS_ORDER_IS_PAID));
        }
        if(!TradeType.RECHARGE.equals(accountBill.getTradeType()) && FundFlow.IN.equals(accountBill.getFundFlow())){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_TRADE_MUST_INITIATE_BY_INITIATOR,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_TRADE_MUST_INITIATE_BY_INITIATOR));
        }
        //未支付过，则创建一条支付流水
        Payment payment = new Payment();
        BeanUtils.copyProperties(accountBill,payment,"id","bizzSys","bizzSn");
        payment.setAccountUserCode(accountBill.getMainAccountUserCode());
        payment.setAccountId(accountBill.getMainAccountId());
        payment.setAccountRealName(accountBill.getMainAccountRealName());
        //生成支付单号(支付单流水号)
        payment.setPaySn(serialNumberUtils.getPaySn());
        //因为是支付系统自身发起故为SELF
        payment.setBizzSys(BizzSys.SELF);

        final String bizzSn = serialNumberUtils.getPayBizzSn();
        payment.setBizzSn(bizzSn);
        paymentMapper.insertSelective(payment);


        PayFromAccountBillResultVo payFromAccountBillResultVo = new PayFromAccountBillResultVo();
        //由于是从账单发起（即从支付系统自身发起），故这里的业务系统定义为SELF
        payFromAccountBillResultVo.setBizzSys(BizzSys.SELF);
        //业务流水号也是由支付系统自身生成
        payFromAccountBillResultVo.setBizzSn(bizzSn);

        PaymentToTradeOrder paymentToTradeOrder = new PaymentToTradeOrder(payment.getPaySn(),accountBill.getBizzSys(),accountBill.getOrderSn());
        //记录支付流水业务订单关联表
        paymentToTradeOrderService.savePaymentToTradeOrder(paymentToTradeOrder);

        return  payFromAccountBillResultVo;
    }

    /**
     * 支付页面信息
     * @param userCode
     * @param bizzSys
     * @param bizzSn
     */
    public PayInfoVo findPayInfo(String userCode, BizzSys bizzSys, String bizzSn) {
        PayInfoVo payInfoVo = new PayInfoVo();
        BigDecimal amount = paymentMapper.selectAmountByBizzSysAndBizzSn(bizzSys, bizzSn);
        payInfoVo.setAmount(amount);
        Account account = accountService.findByUserCode(userCode);
        payInfoVo.setAvailableBalance(account.getAvailableBalance());
        payInfoVo.setIsSetPayPass(account.getIsSetPayPass());
        AccountBankcardVo accountBankcardVo = new AccountBankcardVo();
        Long accountId = accountService.findAccountIdByUserCode(userCode);
        List<AccountBankcard> bankcardList = accountBankcardService.getUsedBankCardList(accountId);
        AccountBankcard defaultAcctBankcard = null;
        if (!CollectionUtils.isEmpty(bankcardList)) {
            for (AccountBankcard accountBankcard : bankcardList) {
                if ("true".equals(accountBankcard.getIsDefault())) {
                    defaultAcctBankcard = accountBankcard;
                }
            }
            if (null == defaultAcctBankcard) {
                defaultAcctBankcard = bankcardList.get(0);
            }
        }
        if (null != defaultAcctBankcard) {
            BeanUtils.copyProperties(defaultAcctBankcard,accountBankcardVo);
            payInfoVo.setAccountBankcardVo(accountBankcardVo);
        }
        return payInfoVo;
    }


    /**
     * 选择支付方式
     * @param choosePayTypeDto
     * @return
     */
    public int choosePayType(ChoosePayTypeDto choosePayTypeDto) {
        Payment payment = paymentMapper.selectByBizzSysAndBizzSn(choosePayTypeDto.getBizzSys(),choosePayTypeDto.getBizzSn());
        if(null == payment){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_BIZZSN_NOSUCHPAYMENT,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_BIZZSN_NOSUCHPAYMENT,new Object[]{choosePayTypeDto.getBizzSys(),choosePayTypeDto.getBizzSn()}));
        }
        //已经选好支付方式，就不能改了
        if(null != payment.getChannel() || null != payment.getPayType()){
            throw new BusinessException(ExceptionCodeConstants.PAY_PAYTYPE_NOTALLOWEDMODIFY,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAY_PAYTYPE_NOTALLOWEDMODIFY));
        }
        if(!PayStatus.WAIT_PAY.equals(payment.getStatus())){
            throw new BusinessException(ExceptionCodeConstants.PAY_PAYTYPE_NOTALLOWEDMODIFY,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAY_PAYTYPE_NOTALLOWEDMODIFY));
        }
        //若是建行b2b支付，需要校验下，金额是否大于10元
        if(choosePayTypeDto.getChannel().equalsIgnoreCase("ccb") && choosePayTypeDto.getPayType().equalsIgnoreCase("b2b_nb")){
            if(payment.getAmount().compareTo(new BigDecimal(10.00D)) <= 0){
                throw new BusinessException(ExceptionCodeConstants.PAYMENT_CURRENT_PAY_TYPE_MUST_BIGGER_THAN,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_CURRENT_PAY_TYPE_MUST_BIGGER_THAN,new Object[]{10}));
            }
        }
        //提示支付单不是由租户编码为[{0}]的资金账户创建的
        if(!choosePayTypeDto.getUserCode().equals(payment.getAccountUserCode())){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_ACCOUNT_ISNOTCREATEDBYCURRENTACCOUNT,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_ACCOUNT_ISNOTCREATEDBYCURRENTACCOUNT,new Object[]{choosePayTypeDto.getUserCode()}));
        }
        if(choosePayTypeDto.getPayType().indexOf("_NB") >= 0  && StringUtils.isBlank(choosePayTypeDto.getReturnUrl())){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_CURRENT_PAYTYPE_MUST_SET_RETURN_URL,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_CURRENT_PAY_TYPE_MUST_SET_RETURN_URL));
        }
        if(BALANCE_PAY_TYPE.equals(choosePayTypeDto.getPayType())){
            //若选择的是余额支付需要校验余额是否充足
            payVerifyService.checkMainAccountBalanceEnough(payment.getAccountId(),payment.getAmount());
        }
        int affectedRows = -1;
        if(StringUtils.isNotBlank(choosePayTypeDto.getReturnUrl())) {
            affectedRows = paymentMapper.updatePayTypeAndReturnUrlByBizzSnAndBizzSys(choosePayTypeDto.getBizzSys(), choosePayTypeDto.getBizzSn(), choosePayTypeDto.getChannel(), choosePayTypeDto.getPayType(),choosePayTypeDto.getReturnUrl());
        }else{
            affectedRows = paymentMapper.updatePayTypeByBizzSnAndBizzSys(choosePayTypeDto.getBizzSys(), choosePayTypeDto.getBizzSn(), choosePayTypeDto.getChannel().toUpperCase(), choosePayTypeDto.getPayType().toUpperCase());
        }
        return affectedRows;
    }
    public @interface ChoosePayType {}

    /**
     * 具体支付操作（若是网银支付，返回重定向到银行的url）
     * @param payDto
     * @return
     */
    @Transactional
    public PayResultVo doPay(PayDto payDto) throws Exception {
        Payment payment =paymentMapper.selectByBizzSysAndBizzSn(payDto.getBizzSys(),payDto.getBizzSn());
        if(null == payment){
            throw new BusinessException(ExceptionCodeConstants.PAYMENT_BIZZSN_NOSUCHPAYMENT,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYMENT_BIZZSN_NOSUCHPAYMENT,new Object[]{payDto.getBizzSys(),payDto.getBizzSn()}));
        }
        String payType = payment.getPayType();
        if(StringUtils.isBlank(payType)){
            throw new BusinessException(ExceptionCodeConstants.PAYTYPE_CHECK_NOTCHOOSE,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.PAYTYPE_CHECK_NOTCHOOSE));
        }

        PayBizzRequestDto payBizzRequestDto = new PayBizzRequestDto();
        BeanUtils.copyProperties(payment,payBizzRequestDto);
        //若不是网银支付（包括B2C,B2B则无需支付密码）
        payBizzRequestDto.setPayPass(payDto.getPayPass());
        //若是余额支付需要校验密码 TODO 若后期增加需要支付密码的支付类型（比如积分），则还需增加判断
        if(BALANCE_PAY_TYPE.equals(payBizzRequestDto.getPayType())){
            if(StringUtils.isEmpty(payDto.getPayPass())){
                throw new BusinessException(ExceptionCodeConstants.ACCOUNT_CHECK_PAYPASSNOTEMPTY,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.ACCOUNT_CHECK_PAYPASSNOTEMPTY));
            }
            Pattern pattern = Pattern.compile("^\\d{6}$");
            Matcher matcher = pattern.matcher(payDto.getPayPass());
            if(!matcher.find()){
                throw new BusinessException(ExceptionCodeConstants.ACCOUNT_CHECK_PAYPASSLENGHTERROR,localeMessageSourceUtils.getMessage(ExceptionMessageConstants.ACCOUNT_CHECK_PAYPASSLENGHTERROR));
            }
        }
        payBizzRequestDto.setUserCode(payment.getAccountUserCode());
        payBizzRequestDto.setAccountId(payment.getAccountId());
        //业务方订单列表
        List<OrderDto> orderDtoList = Lists.newArrayList();
        List<TradeOrder> tradeOrderList = Lists.newArrayList();
        //由账单发起的付款
        if(serialNumberUtils.isPayBizzSn(payBizzRequestDto.getBizzSn())){
            PaymentToTradeOrder paymentToTradeOrder = paymentToTradeOrderService.findByPaySn(payBizzRequestDto.getPaySn());
            List<String> orderSnList = Lists.newArrayList();
            orderSnList.add(paymentToTradeOrder.getOrderSn());
            tradeOrderList= tradeOrderService.findOrdersByBizzSysAndOrderSns(paymentToTradeOrder.getBizzSys(),orderSnList);
        }else {
            tradeOrderList = tradeOrderService.findOrdersByBizzSysAndBizzSn(payment.getBizzSys(), payment.getBizzSn());
        }
        for (TradeOrder tradeOrder : tradeOrderList) {
            OrderDto orderDto = new OrderDto();
            BeanUtils.copyProperties(tradeOrder, orderDto);
            orderDtoList.add(orderDto);
        }
        payBizzRequestDto.setOrders(orderDtoList);

        PayHandler payHandler = payDispatcher.getPayHandler(payment.getChannel());
        return payHandler.doProcess(payBizzRequestDto);
    }

    @Override
    public Payment findByBizzSysAndBizzSn(BizzSys bizzSys, String bizzSn) {
        return paymentMapper.selectByBizzSysAndBizzSn(bizzSys,bizzSn);
    }

    /**
     * 计算交易总金额
     *
     * @param paymentDto
     */
    private BigDecimal calculateTradeTotalAmount(PaymentDto paymentDto){
        BigDecimal totalAmount = BigDecimal.ZERO;
        for(OrderDto orderDto:paymentDto.getOrders()){
            totalAmount = totalAmount.add(orderDto.getAmount());
        }
        return totalAmount;
    }

    /**
     * 更新支付单状态为成功
     * @param paySn
     * @return
     */
    public int updatePaymentPaidSuccess(String paySn) {
        return paymentMapper.updateStatusByPaySn(paySn,PayStatus.PAID);
    }

    /**
     * 更新支付单状态为付款失败
     * @param paySn
     * @return
     */
    public int updatePaymentPaidFailure(String paySn) {
        return paymentMapper.updateStatusByPaySn(paySn,PayStatus.FAILURE);
    }

    /**
     * 查询支付状态
     * @param bizzSys
     * @param bizzSn
     * @return
     */
    public PayStatus findStatusByBizzSn(BizzSys bizzSys, String bizzSn) {
        return paymentMapper.selectStatusById(bizzSys,bizzSn);
    }

    /**
     * 查询支付状态
     * @param paySn
     * @return
     */
    public PayStatus findStatusByPaySn(String paySn) {
        return paymentMapper.selectStatusByPaySn(paySn);
    }

    /**
     * 根据paySn查找支付单信息
     * @param paySn
     * @return
     */
    public Payment findByPaySn(String paySn){
        return paymentMapper.selectByPaySn(paySn);
    }

    /**
     * 根据paySn查找支付单信息中的同步通知回调url
     * @param paySn
     * @return
     */
    public String findReturnUrlByPaySn(String paySn){
        return paymentMapper.selectReturnUrlByPaySn(paySn);
    }

    @Override
    public boolean isExists(String paySn) {
        Long id = paymentMapper.existsWithPaySn(paySn);
        if(null != id && id.longValue() > 0L){
            return true;
        }
        return false;
    }
}
