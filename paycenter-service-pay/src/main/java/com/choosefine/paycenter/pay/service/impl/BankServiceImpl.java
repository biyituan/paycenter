package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.pay.dao.BankMapper;
import com.choosefine.paycenter.pay.model.Bank;
import com.choosefine.paycenter.pay.model.Channel;
import com.choosefine.paycenter.pay.service.BankService;
import com.choosefine.paycenter.pay.service.ChannelService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 15:17
 **/
@Service
public class BankServiceImpl implements BankService {
    private final static int isSupport=1;
    @Autowired
    BankMapper bankMapper;

    private List<Bank> supportBankList;

    @Autowired
    private ChannelService channelService;


    @PostConstruct
    public void init(){
        supportBankList = bankMapper.selectSupportBankList(isSupport);
        if(CollectionUtils.isNotEmpty(supportBankList)){
            for(Bank bank : supportBankList){
                Channel channel = channelService.findChannelByBankCode(bank.getBankCode());
                bank.setLogo(channel.getLogo());
                bank.setLogoPc(channel.getLogoPc());
            }
        }
    }

    public List<Bank> selectSupportBankList() {
        return supportBankList;
    }

    @Override
    public String findBankNameByBankCode(String bankCode) {
        return bankMapper.selectBankNameByBankCode(bankCode);
    }
}
