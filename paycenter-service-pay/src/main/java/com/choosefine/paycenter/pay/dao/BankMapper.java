package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.Bank;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:31
 **/
public interface BankMapper extends BaseMapper<Bank>{
    @Select("SELECT bank_code,bank_name,status FROM paycenter_bank WHERE status=#{status}")
    public List<Bank> selectSupportBankList(@Param("status") Integer status);

    @Select("SELECT bank_name FROM paycenter_bank WHERE bank_code = #{bankCode}")
    public String selectBankNameByBankCode(@Param("bankCode") String bankCode);
}
