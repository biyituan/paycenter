package com.choosefine.paycenter.pay.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.choosefine.paycenter.account.dto.MainDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.account.model.AccountBill;
import com.choosefine.paycenter.account.service.AccountBillService;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.enums.*;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.api.RechargeService;
import com.choosefine.paycenter.pay.model.Channel;
import com.choosefine.paycenter.pay.model.Payment;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;
import com.choosefine.paycenter.pay.service.*;
import com.choosefine.paycenter.pay.utils.PayUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Timestamp;
import java.util.List;

/**
 * Comments：用于执行本地事物
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Service
public class PayLocalTransactionServiceImpl implements PayLocalTransactionService {
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private AccountBillService accountBillService;

    @Autowired
    private RechargeService rechargeService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private ChannelService channelService;

    @Autowired
    private PaymentToTradeOrderService paymentToTradeOrderService;

    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Autowired
    private com.choosefine.paycenter.account.service.ShiXiaoBaoTrxService shiXiaoBaoTrxService;


    //这里认为支付单与账单是关系比较紧密的，故放在一个事务
    @Transactional
    public void processPaySuccess(JSONObject payOrderMessage) {
        final String paySn = payOrderMessage.getString("sn");
        final BizzSys bizzSys = BizzSys.valueOf(payOrderMessage.getString("bizzSys"));

        final Payment payment = paymentService.findBizzSnAndBizzSysByPaySn(paySn);
        final String bizzSn = payment.getBizzSn();

        //如果是从账单发起的付款
        if (serialNumberUtils.isPayBizzSn(bizzSn)) {
            //判断从账单发起的该订单重复支付了一笔，如果是，则进行复制生成一进一出两笔账单
            PaymentToTradeOrder order = paymentToTradeOrderService.findByPaySn(paySn);
            List<PaymentToTradeOrder> paymentToOrderList = paymentToTradeOrderService.findByOrderSn(order.getBizzSys().name(), order.getOrderSn());

            if(CollectionUtils.isNotEmpty(paymentToOrderList)) {
                //说明针对某一业务系统的某一业务订单，有重复支付的情况发生
                if (paymentToOrderList.size() > 1) {
                    //处理补账单，若发现某一业务系统的某一业务订单，已经支付完成了，那么需要补账单
                    //hasPaidBefore表示之前是否已经支付过，若未支付过，还需要更改账单状态
                    boolean hasPaidBefore = processRepeatPay(payOrderMessage,paymentToOrderList);
                    if(!hasPaidBefore){
                        //设置账单与交易订单状态为成功
                        proccessDoAccountBillAndTradeOrderAsSuccess(paySn);
                    }
                }else{//针对某一业务系统的某一业务订单，没有重复支付的情况
                    //设置账单与交易订单状态为成功
                    proccessDoAccountBillAndTradeOrderAsSuccess(paySn);
                }
            }
        } else {//如果是普通转账、或是分包款、批量发工资等
            //同时更改账单(为完成)、交易订单状态（为已支付）
            JSONArray orders = payOrderMessage.getJSONArray("orders");
            if (null != orders && orders.size() > 0) {
                for (int i = 0; i < orders.size(); i++) {
                    JSONObject orderJSONObj = orders.getJSONObject(i);
                    String orderSn = orderJSONObj.getString("orderSn");
                    //TODO 有可能，用户先是通过账单付完款，那么需要针对每一个账单判断是否已经完成，若已经完成，需要补账单、补施小包流水
                    accountBillService.updateAccountBillSuccess(bizzSys, orderSn);
                    tradeOrderService.tradeOrderPaid(bizzSys.toString(), orderSn);
                }
            }
        }

        //执行更新支付单（支付流水）的支付状态为支付成功的本地事务
        paymentService.updatePaymentPaidSuccess(paySn);

        //账户余额处理
        PaymentDto paymentDto = PayUtils.getInstance().getPaymentDto(payOrderMessage);

        //主账户余额减少（只有余额来付款的时候，需要减少主账户余额）
        //若是余额支付的话需要减少主账户余额，增加对方账户余额,记录余额变更日志
        //TODO 若有其他需要减少主账户余额的支付方式，需要在里添加
        if ("SELF".equalsIgnoreCase(payOrderMessage.getString("channel")) && "BALANCE".equalsIgnoreCase(payOrderMessage.getString("payType"))) {
            accountService.decreaseMainAccountBalance(new MainDto(paymentDto.getAccountId(), paymentDto.getUserCode(), paymentDto.getAccountRealName(), paymentDto.getAmount()), paySn);
        }
        //对方账户余额增加
        accountService.increaseOppositeAccountBalance(paymentDto.getOrders(), paySn);
    }

    private void proccessDoAccountBillAndTradeOrderAsSuccess(String paySn) {
        PaymentToTradeOrder paymentToTradeOrder = paymentToTradeOrderService.findByPaySn(paySn);
        //更改账单状态(为完成)
        accountBillService.updateAccountBillSuccess(paymentToTradeOrder.getBizzSys(), paymentToTradeOrder.getOrderSn());
        //更改交易订单状态（为已支付）
        tradeOrderService.tradeOrderPaid(paymentToTradeOrder.getBizzSys().toString(), paymentToTradeOrder.getOrderSn());
    }

    /**
     * 处理重复支付，补账单
     * @param payOrderMessage
     * @param paymentToOrderList
     */
    private boolean processRepeatPay(JSONObject payOrderMessage, List<PaymentToTradeOrder> paymentToOrderList) {
        boolean hasPaidBefore = false;
        //遍历支付流水与（业务系统、业务订单关联关系），此关联关系表示多比支付流水对应同一个业务系统的同一个业务订单
        for (PaymentToTradeOrder paymentToTradeOrder : paymentToOrderList) {
            PayStatus status = paymentService.findStatusByPaySn(paymentToTradeOrder.getPaySn());
            //若找到其中有一笔支付流水已经完成了
            if (status.equals(PayStatus.PAID)) {
                log.info("从账单发起的该订单重复支付了一笔，orderSn={},bizzSys={}", paymentToTradeOrder.getOrderSn(), paymentToTradeOrder.getBizzSys().getName());
                //查出所有的账单，复制的时候只需取到第一笔，与第二笔。其中第一个比账单为发起方（付款方）的账单，第二笔账单为接收方（收款方）的账单
                List<AccountBill> billlist = accountBillService.findAccountBillListByBizzSysAndOrderSn(paymentToTradeOrder.getBizzSys(), paymentToTradeOrder.getOrderSn());
                if (CollectionUtils.isNotEmpty(billlist)) {
                    Timestamp nowTimestamp = new Timestamp(System.currentTimeMillis());
                    //复制一进一出两笔账单
                    copyAcccountBill(billlist, nowTimestamp);
                }
                //消息体设置重复支付标志
                payOrderMessage.put("isRepeat", "true");
                hasPaidBefore = true;
                break;  //补账单成功，跳出循环
            }
        }
        return hasPaidBefore;
    }

    /**
     * 复制账单（发起方，接收方各复制一份）
     * @param billlist
     * @param nowTimestamp
     */
    private void copyAcccountBill(List<AccountBill> billlist, Timestamp nowTimestamp) {
        AccountBill outBill = billlist.get(0);
        outBill.setCreatedAt(nowTimestamp);
        outBill.setUpdatedAt(null);
        outBill.setId(null);
        accountBillService.recordAccountBill(outBill);

        AccountBill inBill = billlist.get(1);
        inBill.setCreatedAt(nowTimestamp);
        inBill.setUpdatedAt(null);
        inBill.setId(null);
        accountBillService.recordAccountBill(inBill);
    }


    //这里认为支付单与账单是关系比较紧密的，故放在一个事务
    @Transactional
    public void processPayFailure(JSONObject payOrderMessage) {
        final String paySn = payOrderMessage.getString("sn");
        final BizzSys bizzSys = BizzSys.valueOf(payOrderMessage.getString("bizzSys"));
        //执行更新支付单的支付状态为支付失败的本地事务
        paymentService.updatePaymentPaidFailure(paySn);
        //同时更改账单状态
        JSONArray orders = payOrderMessage.getJSONArray("orders");
        if (null != orders && orders.size() > 0) {
            for (int i = 0; i < orders.size(); i++) {
                JSONObject orderJSONObj = orders.getJSONObject(i);
                String orderSn = orderJSONObj.getString("orderSn");
                accountBillService.updateAccountBillFailure(bizzSys, orderSn);
            }
        }
    }

    //这里认为充值单与账单是关系比较紧密的，故放在一个事务
    @Transactional
    public void processRechargeSuccess(JSONObject payOrderMessage) {
        final String rSn = payOrderMessage.getString("sn");
        final BizzSys bizzSys = BizzSys.valueOf(payOrderMessage.getString("bizzSys"));
        //执行更新支付单的支付状态为支付成功的本地事务
        rechargeService.rechargeComplted(rSn);
        //同时更改账单状态
        //accountBillService.updateAccountBillSuccess(bizzSys, rSn);
        //充值成功才记录账单
        AccountBill accountBill = getAccountBill(payOrderMessage);
        accountBillService.recordAccountBill(accountBill);
        //更新账单中充值使用的银行
        accountBillService.recordBankNameByBizzSysAndOrderSn(bizzSys, rSn, channelService.findChannelByChannelCode(payOrderMessage.getString("channel")).getBankName());
        //账户余额处理
        MainDto mainDto = new MainDto(payOrderMessage.getLong("accountId"), payOrderMessage.getString("userCode"), payOrderMessage.getString("realName"), payOrderMessage.getBigDecimal("amount"));
        //充值成功增加余额
        accountService.increaseMainAccountBalance(mainDto, rSn);
    }

    private AccountBill getAccountBill(JSONObject payOrderMessage) {
        AccountBill accountBill = new AccountBill();
        accountBill.setMainAccountId(payOrderMessage.getLong("accountId"));
        accountBill.setMainAccountUserCode(payOrderMessage.getString("userCode"));
        accountBill.setMainAccountMobile(payOrderMessage.getString("accountMobile"));
        accountBill.setMainAccountRealName(payOrderMessage.getString("realName"));
        accountBill.setAmount(payOrderMessage.getBigDecimal("amount"));
        Channel channel = channelService.findChannelByChannelCode(payOrderMessage.getString("channel"));
        accountBill.setImg(channel.getLogo());
        accountBill.setBizzSys(BizzSys.valueOf(payOrderMessage.getString("bizzSys")));
        accountBill.setTradeType(TradeType.RECHARGE);
        accountBill.setOperName(payOrderMessage.getString("operName"));
        accountBill.setStatus(AccountBillStatus.COMPLETE);
        accountBill.setBankName(channel.getBankName());
        accountBill.setTradeMemo("充值");
        accountBill.setFundFlow(FundFlow.IN);
        //充值的时候,账单的orderSn即为充值流水号
        accountBill.setOrderSn(payOrderMessage.getString("sn"));
        return accountBill;
    }

    //这里认为充值单与账单是关系比较紧密的，故放在一个事务
    @Transactional
    public void processRechargeFailure(JSONObject payOrderMessage) {
        final String rSn = payOrderMessage.getString("sn");
        final BizzSys bizzSys = BizzSys.valueOf(payOrderMessage.getString("bizzSys"));
        //执行更新充值单的支付状态为支付失败的本地事务
        rechargeService.rechargeFailure(rSn);
        //同时更改账单状态
        accountBillService.updateAccountBillFailure(bizzSys, rSn);
    }
}
