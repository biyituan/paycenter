package com.choosefine.paycenter.pay.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Deprecated
public enum CCB_COUNTER {
    B2C("000412817","正式B2C"),B2B("000433372","正式B2B"),B2C_TEST("002055383","测试B2C"),B2B_TEST("002055387","测试B2B");

    private String counterNo;
    private String name;

    private static final Map<String, CCB_COUNTER> lookup = new HashMap<String, CCB_COUNTER>();

    static {
        for (CCB_COUNTER s : EnumSet.allOf(CCB_COUNTER.class)) {
            lookup.put(s.getCounterNo(), s);
        }
    }

    CCB_COUNTER(String counterNo,String name){
        this.counterNo = counterNo;
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String getCounterNo(){
        return this.counterNo;
    }

    public static CCB_COUNTER getEnum(String counterNo){
        return lookup.get(counterNo);
    }
}
