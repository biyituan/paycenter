package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.dao.PaymentToTradeOrderMapper;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;
import com.choosefine.paycenter.pay.service.PaymentToTradeOrderService;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 20:42
 **/
@Slf4j
@Service
public class PaymentToTradeOrderServiceImpl implements PaymentToTradeOrderService {
    @Autowired
    private PaymentToTradeOrderMapper paymentToTradeOrderMapper;

    @Autowired
    private SerialNumberUtils serialNumberUtils;

    @Override
    public int recordPaymentToTradeOrder(PaymentDto paymentDto, String paySn) {
        if(CollectionUtils.isNotEmpty(paymentDto.getOrders())){
            List<PaymentToTradeOrder> paymentToTradeOrderList = Lists.newArrayList();
            for ( OrderDto orderDto: paymentDto.getOrders()){
                PaymentToTradeOrder paymentToTradeOrder = new PaymentToTradeOrder();
                paymentToTradeOrder.setBizzSys(paymentDto.getBizzSys());
                //转账的时候，业务流水号与业务订单号是相同的，故有以下处理方案
                if(serialNumberUtils.isTransferBizzSn(paymentDto.getBizzSn())){
                    paymentToTradeOrder.setOrderSn(paymentDto.getBizzSn());
                }else{
                    paymentToTradeOrder.setOrderSn(orderDto.getOrderSn());
                }
                paymentToTradeOrder.setPaySn(paySn);
                paymentToTradeOrderList.add(paymentToTradeOrder);
            }
            if(CollectionUtils.isNotEmpty(paymentToTradeOrderList)) {
                return paymentToTradeOrderMapper.batchInsertPaymentToTradeOrder(paymentToTradeOrderList);
            }
        }
        return 0;
    }

    @Override
    public int savePaymentToTradeOrder(PaymentToTradeOrder paymentToTradeOrder) {
        return paymentToTradeOrderMapper.insertSelective(paymentToTradeOrder);
    }

    @Override
    public List<String> selectOrderSnByPaySn(String paySn) {
        return paymentToTradeOrderMapper.selectOrderSnByPaySn(paySn);
    }

    @Override
    public String selectPaySnByBizzSysAndOrderSn(String bizzSys, String orderSn) {
        return paymentToTradeOrderMapper.selectPaySnByBizzSysAndOrderSn(bizzSys,orderSn);
    }

    @Override
    public PaymentToTradeOrder findByPaySn(String paySn) {
        return paymentToTradeOrderMapper.selectByPaySn(paySn);
    }

    @Override
    public List<PaymentToTradeOrder> findByOrderSn(String bizzSys,String orderSn){
        return paymentToTradeOrderMapper.findByOrderSn(bizzSys,orderSn);
    }
}
