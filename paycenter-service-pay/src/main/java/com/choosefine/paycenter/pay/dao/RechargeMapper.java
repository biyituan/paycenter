package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.account.model.Recharge;
import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.common.enums.RechargeStatus;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface RechargeMapper extends BaseMapper<Recharge> {
    @Select("SELECT * FROM paycenter_recharge WHERE r_sn = #{rSn}")
    public Recharge selectByRechargeSn(@Param("rSn") String rechargeSn);

    //注意：这里的finished_at表示某个状态的完成时间
    @Update("UPDATE paycenter_recharge SET status = #{status},finished_at = UNIX_TIMESTAMP()*1000 WHERE r_sn = #{rSn}")
    public int updateStatusByRechargeSn(@Param("rSn") String rechargeSn, @Param("status") RechargeStatus status);

    @Select("SELECT return_url FROM paycenter_recharge WHERE r_sn = #{rSn}")
    public String selectReturnUrlByRechargeSn(@Param("rSn") String rSn);

    @Select("SELECT status FROM paycenter_recharge WHERE r_sn = #{rSn}")
    String selectStatusByRsn(@Param("rSn") String rSn);
}
