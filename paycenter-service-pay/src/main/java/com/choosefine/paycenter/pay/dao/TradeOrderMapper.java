package com.choosefine.paycenter.pay.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.common.dao.SimpleSelectInExtendedLanguageDriver;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeOrderStatus;
import com.choosefine.paycenter.pay.model.TradeOrder;
import org.apache.ibatis.annotations.Lang;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:33
 **/
public interface TradeOrderMapper extends BaseMapper<TradeOrder>{
    @Select("SELECT * FROM paycenter_trade_order WHERE bizz_sys=#{bizzSys} AND order_sn=#{orderSn} AND status=#{status}")
    TradeOrder selectByBizzSysAndOrderSnAndStatus(@Param("bizzSys") String bizzSys, @Param("orderSn")String orderSn, @Param("status") String status);

    @Select("SELECT * FROM paycenter_trade_order WHERE bizz_sys = #{bizzSys} AND order_sn=#{orderSn}")
    TradeOrder selectByBizzSysAndOrderSn(@Param("bizzSys") BizzSys bizzSys ,@Param("orderSn")String orderSn);

    @Lang(SimpleSelectInExtendedLanguageDriver.class)
    @Select("SELECT * FROM paycenter_trade_order WHERE bizz_sys = #{bizzSys} AND order_sn IN (#{orderSns})")
    List<TradeOrder> selectByBizzSysAndOrderSns(@Param("bizzSys") BizzSys bizzSys, @Param("orderSns") List<String> orderSns);

    @Select("SELECT * FROM paycenter_trade_order WHERE bizz_sys = #{bizzSys} AND bizz_sn = #{bizzSn}")
    List<TradeOrder> selectByBizzSysAndBizzSn(@Param("bizzSys") BizzSys bizzSys, @Param("bizzSn") String bizzSn);

    @Update("UPDATE paycenter_trade_order SET status = #{tradeOrderStatus} WHERE bizz_sys = #{bizzSys} AND order_sn = #{orderSn}")
    int updateTradeOrderStatusByBizzSysAndOrderSn(@Param("bizzSys") String bizzSys, @Param("orderSn") String orderSn, @Param("tradeOrderStatus") TradeOrderStatus tradeOrderStatus);

    int batchInsertTradeOrder(@Param("tradeOrderList") List<TradeOrder> tradeOrderList);

}
