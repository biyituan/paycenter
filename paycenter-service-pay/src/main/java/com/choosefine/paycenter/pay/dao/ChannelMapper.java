package com.choosefine.paycenter.pay.dao;


import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.pay.model.Channel;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 支付渠道
 * Created by Dengyouyi on 2017/4/18.
 */
public interface ChannelMapper extends BaseMapper<Channel> {
    @Select("SELECT id,channel_code,bank_code,bank_name,logo,logo_pc,type,pay_type FROM paycenter_channel WHERE status =1 ORDER BY type ASC")
    List<Channel> selectAllChannelList();

    @Select("SELECT id,channel_code,bank_code,bank_name,logo,logo_pc,type,pay_type FROM paycenter_channel WHERE status =1 AND pay_type = #{payType} ORDER BY type ASC")
    List<Channel> selectChannelList(String payType);

    @Select("SELECT DISTINCT bank_name FROM paycenter_channel WHERE channel_code = #{channelCode}")
    String selectChannelNameByCode(@Param("channelCode") String channelCode);

    @Select("SELECT channel_code FROM paycenter_channel WHERE id = #{id}")
    String selectChannelCodeById(@Param("id")Long id);

    @Select("SELECT bank_code FROM paycenter_channel WHERE id = #{id}")
    String selectBankCodeById(@Param("id")Long id);

    @Select("SELECT DISTINCT channel_code FROM paycenter_channel WHERE bank_code=#{bankCode} AND status =1")
    String selectChannelListDis(@Param("bankCode") String bankCode);

    @Select("SELECT DISTINCT channel_code FROM paycenter_channel WHERE bank_code=#{bankCode}")
    String selectChannelCodeByBankCode(@Param("bankCode") String bankCode);

    @Select("SELECT DISTINCT channel_code,bank_name,bank_code,logo,logo_pc,status FROM paycenter_channel")
    List<Channel> selectAllDistinctChannelList();
}
