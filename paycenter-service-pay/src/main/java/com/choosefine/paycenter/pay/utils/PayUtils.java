package com.choosefine.paycenter.pay.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeType;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.dto.PayOrderMessageDto;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/4
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class PayUtils {
    private static PayUtils payUtils;

    private PayUtils(){}

    public static synchronized PayUtils getInstance(){
        return payUtils != null ? payUtils :(payUtils =  new PayUtils());
    }

    public PayOrderMessageDto getPayOrderMessageDto(PayBizzRequestDto payBizzRequestDto) {
        PayOrderMessageDto payOrderMessageDto = new PayOrderMessageDto();
        //业务订单
        payOrderMessageDto.setOrders(payBizzRequestDto.getOrders());
        payOrderMessageDto.setTradeType(payBizzRequestDto.getTradeType());
        //流水号
        payOrderMessageDto.setSn(payBizzRequestDto.getPaySn());
        payOrderMessageDto.setAmount(payBizzRequestDto.getAmount());
        payOrderMessageDto.setBizzSys(payBizzRequestDto.getBizzSys());
        payOrderMessageDto.setBizzSn(payBizzRequestDto.getBizzSn());
        payOrderMessageDto.setChannel(payBizzRequestDto.getChannel());
        payOrderMessageDto.setPayType(payBizzRequestDto.getPayType());
        payOrderMessageDto.setUserCode(payBizzRequestDto.getUserCode());
        payOrderMessageDto.setRealName(payBizzRequestDto.getAccountRealName());
        payOrderMessageDto.setAccountId(payBizzRequestDto.getAccountId());
        return payOrderMessageDto;
    }

    public PaymentDto getPaymentDto(JSONObject payOrderMessage) {
        String userCode = payOrderMessage.getString("userCode");
        PaymentDto paymentDto = new PaymentDto();
        paymentDto.setAccountId(payOrderMessage.getLong("accountId"));
        paymentDto.setUserCode(payOrderMessage.getString("userCode"));
        paymentDto.setAccountRealName(payOrderMessage.getString("realName"));
        paymentDto.setTradeType(payOrderMessage.getObject("tradeType", TradeType.class));
        paymentDto.setBizzSys(payOrderMessage.getObject("bizzSys", BizzSys.class));
        paymentDto.setBizzSn(payOrderMessage.getString("bizzSn"));
        paymentDto.setAmount(payOrderMessage.getBigDecimal("amount"));
        List<OrderDto> orderDtoList = Lists.newArrayList();
        JSONArray oppositeJSONArr = payOrderMessage.getJSONArray("orders");
        for(JSONObject ordersDtosJSONObj : oppositeJSONArr.toArray(new JSONObject[]{})){
              OrderDto ordersDto = new OrderDto();
              ordersDto.setAmount(ordersDtosJSONObj.getBigDecimal("amount"));
              ordersDto.setUserCode(ordersDtosJSONObj.getString("userCode"));
              ordersDto.setOrderSn(ordersDtosJSONObj.getString("orderSn"));
              orderDtoList.add(ordersDto);
        }
        paymentDto.setOrders(orderDtoList);
        return paymentDto;
    }
}
