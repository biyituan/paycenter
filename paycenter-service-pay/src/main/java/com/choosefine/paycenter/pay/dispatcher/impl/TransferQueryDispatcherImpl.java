package com.choosefine.paycenter.pay.dispatcher.impl;

import com.choosefine.paycenter.pay.dispatcher.TransferQueryDispatcher;
import com.choosefine.paycenter.pay.handler.TransferQueryHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:45
 **/
@Component
public class TransferQueryDispatcherImpl implements TransferQueryDispatcher,BeanFactoryAware{
    private BeanFactory beanFactory;
    @Override
    public TransferQueryHandler getTransferQueryHandler(String channel) {
        return beanFactory.getBean(channel.toUpperCase()+TRANSFER_QUERY_HANDLER_PREFIX,TransferQueryHandler.class);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory=beanFactory;
    }
}
