package com.choosefine.paycenter.pay.service.impl;

import com.choosefine.paycenter.account.dto.RechargeDto;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.Recharge;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.constants.ExceptionCodeConstants;
import com.choosefine.paycenter.common.constants.ExceptionMessageConstants;
import com.choosefine.paycenter.common.enums.RechargeStatus;
import com.choosefine.paycenter.common.exception.BusinessException;
import com.choosefine.paycenter.common.utils.MessageSourceUtils;
import com.choosefine.paycenter.common.utils.SerialNumberUtils;
import com.choosefine.paycenter.pay.api.RechargeService;
import com.choosefine.paycenter.pay.dao.RechargeMapper;
import com.choosefine.paycenter.pay.service.ChannelService;
import com.choosefine.paycenter.pay.vo.RechargeQueryVo;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Comments：充值服务
 * Author：Jay Chang
 * Create Date：2017/4/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Service
public class RechargeServiceImpl implements RechargeService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private RechargeMapper rechargeMapper;
    @Autowired
    private SerialNumberUtils serialNumberUtils;
    @Autowired
    private MessageSourceUtils messageSourceUtils;
    @Autowired
    private ChannelService channelService;

    public Recharge findByRechargeSn(String sn) {
        Recharge recharge =  rechargeMapper.selectByRechargeSn(sn);
        if(Objects.isNull(recharge)){
            throw new BusinessException(ExceptionCodeConstants.RECHARGE_NOT_FOUND,messageSourceUtils.getMessage(ExceptionMessageConstants.RECHARGE_NOT_FOUND));
        }
        return recharge;
    }

    @Override
    public void saveRecharge(RechargeDto rechargeDto,Account account) {
        Recharge recharge = new Recharge();
        try {
            BeanUtils.copyProperties(recharge,rechargeDto);
        } catch (Exception e){
            throw new RuntimeException(e);
        }
        String rSn = serialNumberUtils.getRechargeSn();
        recharge.setRSn(rSn);rechargeDto.setRSn(rSn);
        recharge.setAccountUserCode(rechargeDto.getUserCode());
        recharge.setAccountRealName(account.getRealName());
        recharge.setAccountId(account.getId());
        rechargeMapper.insertSelective(recharge);
    }

    @Override
    public int rechargeComplted(String rSn) {
        return rechargeMapper.updateStatusByRechargeSn(rSn, RechargeStatus.COMPLETE);
    }

    @Override
    public int rechargeFailure(String rSn) {
        return rechargeMapper.updateStatusByRechargeSn(rSn,RechargeStatus.FAILURE);
    }

    public String findReturnUrlByRechargeSn(final String rSn){
        return rechargeMapper.selectReturnUrlByRechargeSn(rSn);
    }

    public RechargeStatus findStatusByRechargeSn(String rSn) {
        Recharge recharge = findByRechargeSn(rSn);
        return recharge.getStatus();
    }

    /**
     * 充值状态查询
     * @param sn
     * @return
     */
    public RechargeQueryVo findRechargeQueryResultByRSn(String sn) {
        RechargeQueryVo rechargeQueryVo = new RechargeQueryVo();
        rechargeQueryVo.setRSn(sn);
        Recharge recharge = findByRechargeSn(sn);
        rechargeQueryVo.setAmount(recharge.getAmount());
        String channelName = channelService.findChannelByChannelCode(recharge.getChannel()).getBankName();
        rechargeQueryVo.setChannelName(channelName);
        rechargeQueryVo.setSuccess(RechargeStatus.COMPLETE.equals(recharge.getStatus()) ? true : false);
        return rechargeQueryVo;
    }
}
