package com.choosefine.paycenter.reconciliation.service.impl;

import com.choosefine.paycenter.reconciliation.dao.ChannelIncomeBillMapper;
import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;
import com.choosefine.paycenter.reconciliation.service.ChannelIncomeBillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Ye_Wenda
 * @Date 7/20/2017
 */
@Slf4j
@Service
public class ChannelIncomeServiceImpl implements ChannelIncomeBillService {

    @Autowired
    private ChannelIncomeBillMapper channelIncomeBillMapper;

    @Override
    public long save(ChannelIncomeBill channelIncomeBill) {
        return channelIncomeBillMapper.insertSelective(channelIncomeBill);
    }

    /**
     * paySn不重复
     * @param channelIncomeBills
     * @return
     */
    @Override
    public long saveAll(List<ChannelIncomeBill> channelIncomeBills) {
        for (ChannelIncomeBill bill : channelIncomeBills) {
            ChannelIncomeBill preBill = this.getBySn(bill.getPaySn());
            if (preBill == null) {
                channelIncomeBillMapper.insertSelective(bill);
            }
        }
        return 1;
    }

    @Override
    public long update(ChannelIncomeBill channelIncomeBill) {
        return channelIncomeBillMapper.updateByPrimaryKeySelective(channelIncomeBill);
    }

    @Override
    public ChannelIncomeBill get(Long id) {
        return channelIncomeBillMapper.selectByPrimaryKey(id);
    }

    @Override
    public ChannelIncomeBill getBySn(String sn) {
        return channelIncomeBillMapper.selectBySn(sn);
    }

    @Override
    public List<ChannelIncomeBill> getAll() {
        return channelIncomeBillMapper.selectAll();
    }
}
