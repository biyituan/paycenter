package com.choosefine.paycenter.reconciliation.handler;

import com.choosefine.paycenter.common.utils.DateUtil;
import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;
import com.choosefine.paycenter.reconciliation.service.ChannelIncomeBillService;
import com.choosefine.paycenter.reconciliation.service.ReconciliationService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
public class AbstractReconciliationHandler implements ReconciliationHandler {

    @Autowired
    private ChannelIncomeBillService channelIncomeBillService;

    protected ReconciliationService reconciliationService;

    /**
     * Get yesterday's account bill from Bank(or 3-party system).
     * 默认查询前一个工作日，如果传入日期，则查询传入的日期。
     * @param date
     */
    @Override
    public void prepareIncome(Date date) {
        Date requireDay;
        if (date == null) {
            requireDay = DateUtil.getLastWorkDay(date);
        } else {
            requireDay = date;
        }
        // 获取渠道进账数据并转换成通用格式
        List<ChannelIncomeBill> channelIncomeBills = reconciliationService.getDayBills(requireDay);
        // 保存渠道进账数据
        channelIncomeBillService.saveAll(channelIncomeBills);
    }

    @Override
    public void prepareWithdraw() {

    }
}
