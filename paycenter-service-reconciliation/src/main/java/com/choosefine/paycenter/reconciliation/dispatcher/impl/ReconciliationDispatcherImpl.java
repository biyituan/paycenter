package com.choosefine.paycenter.reconciliation.dispatcher.impl;

import com.choosefine.paycenter.reconciliation.dispatcher.ReconciliationDispatcher;
import com.choosefine.paycenter.reconciliation.handler.ReconciliationHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
@Component
public class ReconciliationDispatcherImpl implements ReconciliationDispatcher, BeanFactoryAware {
    private BeanFactory beanFactory;
    @Override
    public ReconciliationHandler getReconciliationHandler(String bankType) {
        return beanFactory.getBean(bankType + RECONCILIATION_HANDLER_SUFFIX, ReconciliationHandler.class);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
