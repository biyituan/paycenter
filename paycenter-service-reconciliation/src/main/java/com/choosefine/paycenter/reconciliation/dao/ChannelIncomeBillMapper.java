package com.choosefine.paycenter.reconciliation.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @Author Ye_Wenda
 * @Date 7/20/2017
 */
public interface ChannelIncomeBillMapper extends BaseMapper<ChannelIncomeBill> {

    @Select("SELECT * FROM paycenter_channel_income_bill WHERE pay_sn=#{sn}")
    ChannelIncomeBill selectBySn(@Param("sn") String sn);
}
