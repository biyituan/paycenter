package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.account.enums.AccountStatus;
import com.choosefine.paycenter.common.jackson.EnumSerializer;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.choosefine.paycenter.common.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 资金账户
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_account")
@ApiModel("资金账户")
public class Account extends BaseModel implements Serializable {
    /**租户编号*/
    @ApiModelProperty("租户编号")
    private String userCode;
    @ApiModelProperty("账户名（规则为xx+手机号码）")
    private String name;
    @ApiModelProperty("租户真实姓名或租户的公司名称")
    private String realName;
    /**角色id*/
    @ApiModelProperty("角色id")
    private Long roleId;
    /**角色名称*/
    @ApiModelProperty("角色名称")
    private String roleName;
    /**手机号码(若是个人租户手机号码可以从租户表中获取，若是公司租户需从登录表中获取)*/
    @ApiModelProperty("手机号码")
    private String mobile;
    /**支付密码*/
    @JsonIgnore
    private String payPass;
    /**支付密码盐*/
    @JsonIgnore
    private String salt;
    /**当前账户冻结的余额*/
    @ApiModelProperty("当前账户冻结的余额")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal freezeBalance;
    /**当前账户可用的余额*/
    @ApiModelProperty("当前账户可用的余额")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal availableBalance;
    /**当前账户余额*/
    @ApiModelProperty("当前账户余额")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal balance;
    /**账户状态 0为正常，1为冻结*/
    @ApiModelProperty("账户状态")
    @Enumerated(EnumType.STRING)
    //@JsonSerialize(using = EnumSerializer.class)
    private AccountStatus status;
    /**账户创建时间*/
    @ApiModelProperty("账户创建时间")
    private Timestamp createdAt;
    /**账户最后更新时间*/
    @ApiModelProperty("账户最后修改时间")
    private Timestamp updatedAt;
    /**银行卡数量*/
    @Transient
    @ApiModelProperty("银行卡数量")
    private Integer bankcardNum;
    /**是否已经设置资金密码*/
    @Transient
    private Boolean isSetPayPass = false;
}
