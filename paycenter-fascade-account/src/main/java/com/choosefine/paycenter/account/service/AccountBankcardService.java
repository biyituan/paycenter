package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.dto.AccountBankcardDto;
import com.choosefine.paycenter.account.dto.AccountBankcardSetUBankCodeAndNameDto;
import com.choosefine.paycenter.account.dto.AccountCompanyBankcardVerifyDto;
import com.choosefine.paycenter.account.model.AccountBankcard;
import com.choosefine.paycenter.account.model.BankcardBin;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Comments：银行卡管理服务
 * Author：DengYouyi
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */

public interface AccountBankcardService {
    public static final int COMPANY_BANK_ACCOUNT = 0;
    public static final int PERSON_BANK_ACCOUNT = 1;
    public static final String B2C  = "B2C";
    public static final String B2B  = "B2B";
    public static final int INPUT_VALID_AMOUNT_ERR_TIMES = 3;
    public static final String RETURN_CODE_SUCCESS = "000000";

    public static final int VALID_SUCCESS = 1;
    public static final int VALID_ING = 0;

    public static final String BANKCARD_NO_REGEX_STR = "\\d{10,}$";

    public static final Pattern BANKCARD_NO_REGEX = Pattern.compile(BANKCARD_NO_REGEX_STR);

    /**
     * 删除银行卡信息
     * @param accountId
     * @return
     */
    public int deleteAccountBankCard( Long accountId,Long bankcardId);

    /**
     *银行卡直接验证通过
     * @param accountId
     * @return
     */
    public int validAccountBankCard( Long accountId,Long bankcardId);

    /**
     * 获取账户银行卡列表信息
     * @param accountId
     * @return
     */
    public List<AccountBankcard> getBankCardList(Long accountId,Integer isPublic);

    /**
     * 获取能使用账户银行卡列表信息
     * @param accountId
     * @return
     */
    public List<AccountBankcard> getUsedBankCardList(Long accountId);

    /**
     * 获取资金账户银行卡详情
     * @param accountBankId
     * @return
     */
    public AccountBankcard findAccountBankById(Long accountBankId) ;

    /**
     * 更改默认银行卡
     *
     * @param accountId
     * @param accountBankcardId
     * @return
     */
    public int changeDefaultUseBankcard(Long accountId,Long accountBankcardId);

    /**
     * 设置联行号与支行名称
     * @param accountBankcardSetUBankCodeAndNameDto
     */
    public int setUnionBankCodeAndName(AccountBankcardSetUBankCodeAndNameDto accountBankcardSetUBankCodeAndNameDto);


    /**
     * 添加个人银行账户
     * @param accountBankcardDto
     */
    public long addPersonalBankCard(String userCode,AccountBankcardDto accountBankcardDto);

    /**
     * 添加企业银行账户
     * @param accountBankcardDto
     */
    public long addCompanyBankCard(String userCode,AccountBankcardDto accountBankcardDto);

    public void verifyCompanyBankcard(String userCode ,AccountCompanyBankcardVerifyDto accountCompanyBankcardVerifyDto);

    public BankcardBin verifyBankcardNo(String bankcardNo);

    public @interface AddPeronalAccountBankCard {
    }

    public @interface AddCompanyAccountBankCard {
    }
}
