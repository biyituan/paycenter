package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.account.enums.AccountStatus;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.dto.BaseDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Jay Chang on 2017/3/6.
 */
@Getter
@Setter
@ToString
@ApiModel("资金账号Dto")
public class AccountDto extends BaseDto implements Serializable{
    private Long id;
    /**租户编号*/
    @ApiModelProperty("租户编号")
    @NotBlank(groups = {AccountService.AccountAdd.class})
    private String userCode;
    /**账户真实姓名，公司租户为公司名称*/
    @ApiModelProperty("个人租户为真实姓名，公司租户为公司名称")
    @NotBlank(groups = {AccountService.AccountAdd.class})
    private String realName;
    /**角色id*/
    @ApiModelProperty("角色id")
    @NotNull(groups = {AccountService.AccountAdd.class})
    private Long roleId;
    /**角色名称*/
    @ApiModelProperty("角色名称")
    @NotBlank(groups = {AccountService.AccountAdd.class})
    private String roleName;
    /**手机号码*/
    @ApiModelProperty("手机号码")
    private String mobile;
    /**余额*/
    @ApiModelProperty("余额")
    private BigDecimal balance;
    /**银行卡数量*/
    @ApiModelProperty("银行卡数量")
    private Integer bankcardNum;
    /**账户状态*/
    @ApiModelProperty("账户状态")
    private AccountStatus status;
    /**创建时间*/
    @ApiModelProperty("创建时间")
    private Long createdAt;
    /**最后更新时间*/
    @ApiModelProperty("最后更新时间")
    private Long updatedAt;
    @NotBlank(groups = {AccountService.InitPayPass.class,AccountService.EditPayPass.class,AccountService.VerifyPayPass.class})
    @Pattern(groups = {AccountService.InitPayPass.class,AccountService.EditPayPass.class,AccountService.VerifyPayPass.class},regexp = "^\\d{6}$",message =  "支付密码必须为6位数字")
    @ApiModelProperty("支付密码")
    private String payPass;
    @NotBlank(groups = {AccountService.EditPayPass.class})
    @Pattern(groups = {AccountService.EditPayPass.class},regexp = "^\\d{6}$",message =  "旧支付密码必须为6位数字")
    @ApiModelProperty("旧支付密码")
    private String oldPayPass;
}
