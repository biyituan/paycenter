package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.dto.*;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.ShiXiaoBaoTrx;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;
import java.util.List;


/**
 * 资金账户服务
 */
public interface AccountService {

    public static final String API_USER_GET = "labour.user.get";

    public static final String API_USERINFO_GET = "labour.userinfo.get";

    public static final String API_USERROLE_GET = "labour.userrole.get";

    public static final String API_SEND_SMS_GET = "sms.sendSms.get";

    public static final String SMS_TEMPLATE = "清空支付密码";

    public static final String PRODUCT = "施小包";

    public static final String ACCOUNT_PAY_PASS_INPUT_ERR_PREFIX = "ACCT_PAY_PASS_INPUT_ERR_";

    public static final String ACCOUNT_LOCK_PREFIX = "ACCT_LOCK_";

    /** 账户余额锁标志*/
    public final static String UNIQUE_ACCOUNT_BALANCE = "UNIQUE_ACCOUNT_BALANCE_";

    /**
     * 查询账户的角色id
     * @param accountId
     * @return
     */
     Long findRoleIdById(final Long accountId);

    /**
     * 查询账户的角色
     * @param userCode
     * @return
     */
    Long findRoleIdByUserCode(final String userCode);

    /**
     * 根据租户编码查询账户id
     * @param userCode
     * @return
     */
     Long findAccountIdByUserCode(final String userCode);

    /**
     * 绑定手机号
     * @param userCode
     * @return
     */
     int bindMobile(String userCode,String mobile);


     void verifyPayPass(String userCode, String payPass);

    /**
     * 校验账户是否锁定或冻结
     * @param accountId
     */
     void checkAccountIsLockedOrBlocked(Long accountId);

    /**
     * 根据资金账号创建租户名称
     * @param accountName
     * @return
     */
    String findUserCodeByAccountName(String accountName);

    boolean checkAccountNameExists(String accountName);

    /**
     * 清空账户名及手机号码
     * @param accountClearNameAndMobileDto
     * @return
     */
    void clearAccountNameAndMobile(AccountClearNameAndMobileDto accountClearNameAndMobileDto);

    /**
     * 修改资金账号账户名与手机号
     * @param accountModifyNameAndMobileDto
     */
    void modifyAccountNameAndMobile(AccountModifyNameAndMobileDto accountModifyNameAndMobileDto);

    @interface AccountAdd{}
    /**
     * 新增资金账户
     *
     * @param accountDto 资金账户
     */
     Long saveAccount(final AccountDto accountDto);

    /**
     * 清空支付密码
     *
     * @param userCode 资金账户的userCode
     */
     int clearPayPass(final String userCode);


    /**
     * 查询资金账号详情
     * @param id
     * @return
     */
     Account findById(final Long id);

    /**
     * 查询资金账号详情
     * @param userCode
     * @return
     */
     Account findByUserCode(final String userCode);

    /**
     * 查询资金账号简单信息
     * @param userCode
     * @return
     */
    Account findSimpleByUserCode(final String userCode);

    /**
     * 查询资金账户总金额(即查询总账户金额)
     * @return
     */
     BigDecimal findTotalAmount();

    /**
     * 分页多条件查询资金账户列表
     * @param accountDto
     * @return
     */
     PageInfo<Account> findAllByPage(final AccountDto accountDto);

    /**
     * 冻结账户
     * @param userCode
     */
     int blockAccount(final String userCode);

    /**
     * 解冻资金账户
     * @param userCode
     * @return
     */
     int unBlockAccount(final String userCode);

    /**
     * 解冻资金账户
     * @param accountId
     * @return
     */
     int unBlockAccount(final Long accountId);

    /**
     * 查询资金账户余额
     * @param id
     * @return
     */
     BigDecimal findBalanceById(Long id);

    /**
     * 查询资金账户余额
     * @param userCode
     * @return
     */
     BigDecimal findBalanceByUserCode(String userCode);

    /**
     * 根据资金账号id校验某个资金账号是否存在
     * @param id
     */
     void checkAccountExist(final Long id);

    /**
     * 校验某个租户编码的账号是否存在
     * @param userCode
     */
     void checkAccountExist(final String userCode);

    /**
     * 校验资金账户是否存在且是否被冻结和锁定
     *
     * @param id
     */
     void checkAccountExistAndIsBlocked(final Long id);

    /**
     * 校验资金账户是否存在且是否被冻结和锁定
     *
     * @param account
     */
    void checkAccountExistAndIsBlocked(final Account account);

    /**
     * 校验交易主体资金账号余额是否充足
     */
     void checkBalanceIsEnough(Long mainAccountId,BigDecimal amount);

     void checkUserIsRight(Long mainAccountId,String userCode);
    /**
     * 未设置过支付密码的账户可以初始化支付密码
     * @param accountDto 资金账户Dto
     */
     int initPayPass(AccountDto accountDto);
     @interface InitPayPass{}

    /**
     * 修改支付密码
     *
     * @param accountDto
     */
     int editPayPass(AccountDto accountDto);
     @interface EditPayPass{}

    /**
     * 查询施小包流水
     * @param shiXiaoBaoTrxDto
     * @return
     */
     PageInfo<ShiXiaoBaoTrx> findShiXiaoBaoTrxHistoryByPage(ShiXiaoBaoTrxDto shiXiaoBaoTrxDto);

     @interface VerifyPayPass {
    }
    /**
     * 更新资金账户余额
     * @param accountId
     * @param balance
     * @return
     */
     int updateAccountBalance(Long accountId,BigDecimal balance);

    /**
     * 根据租户编码查询资金账户详情
     * @param userCode
     * @return
     */
     Account findAccountByUserCode(String userCode);

    /**
     * 用于充值
     * @param mainDto
     * @param sn
     */
     void increaseMainAccountBalance(MainDto mainDto,String sn);

    /**
     * 交易主体减少余额
     * @param mainDto
     */
     void decreaseMainAccountBalance(MainDto mainDto,String sn);

    /**
     * 交易对手增加余额
     * @param orderDto
     * @param sn
     */
     void increaseOppositeAccountBalance(List<OrderDto> orderDto, String sn);

    /**
     * 在提现完成之前更新账户的余额（正在处理提现的时候冻结提现的金额）
     */
     int updateAccountBalanceBeforWithDraw(AccountWdDto accountWdDto);
}
