package com.choosefine.paycenter.account.dto;

import lombok.*;

import java.math.BigDecimal;

/**
 * 交易主体
 * Created by yym on 2017/5/1.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MainDto {
    /**交易主体账号id*/
    private Long accountId;
    /**交易主体租户编码*/
    private String userCode;
    /**交易主体账号真实姓名或公司名称*/
    private String accountRealName;
    /**交易金额*/
    private BigDecimal amount;

    public MainDto(Long accountId,String userCode,BigDecimal amount){
        this.accountId = accountId;
        this.userCode = userCode;
        this.amount = amount;
    }
}
