package com.choosefine.paycenter.account.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

/**
 * Comments：账户名前缀enum
 * Author：Jay Chang
 * Create Date：2017/5/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum RoleType implements BaseEnum{
    WK(1L,"建筑工人"),CL(2L,"班组承包人"),SU(5L,"分包单位"),MG(3L,"项目承包人"),CC(4L,"建筑公司"),MS(6L,"材料供应商"),ES(7L,"设备供应商"),FS(8L,"设施供应商");
    //账户名格式：根据角色在登陆账号前加上2位
    //工人：WK+登陆账号
    //班组：CL+登陆账号
    //分包单位：SU+登陆账号
    //项目经理：MG+登陆账号
    //建筑公司：CC+登陆账号
    //材料供应商：MS+登陆账号
    //设备供应商：ES+登陆账号
    //设施供应商：FS+登陆账号
    private Long roleId;
    private String name;

    RoleType(Long roleId,String name){
        this.roleId = roleId;this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public Long getValue(){
        return roleId;
    }
}
