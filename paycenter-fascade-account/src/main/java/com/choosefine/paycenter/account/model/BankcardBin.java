package com.choosefine.paycenter.account.model;

import lombok.*;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-23 11:17
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BankcardBin {
    private String cardBin;
    private String bankName;
    private int bankId;
    private String cardName;
    private String cardType;
    private int binDigits;
    private int cardDigits;
}
