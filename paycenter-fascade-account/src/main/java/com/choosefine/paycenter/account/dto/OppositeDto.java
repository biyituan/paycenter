package com.choosefine.paycenter.account.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel("交易对方")
public class OppositeDto implements Serializable{
    @NotNull
    @ApiModelProperty("交易对方租户编码")
    private Long accountId;
    @NotNull
    @ApiModelProperty("交易对方真实姓名或公司名称")
    private String accountRealName;
    @NotNull
    @ApiModelProperty("交易对方租户编码")
    private String userCode;
    @NotNull
    @DecimalMin(value = "0.01")
    @ApiModelProperty("交易金额(单位：元)")
    private BigDecimal amount;
}
