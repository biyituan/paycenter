package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.common.dto.BaseDto;
import com.choosefine.paycenter.common.enums.FundFlowDirection;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 施小包流水
 * Created by Jay Chang on 2017/3/7.
 */
@Getter
@Setter
@ApiModel("施小包流水")
public class ShiXiaoBaoTrxDto extends BaseDto{
    @ApiModelProperty("起始时间")
    private Long startTime;
    @ApiModelProperty("截止时间")
    private Long endTime;
    @ApiModelProperty("资金来源/去向")
    private FundFlowDirection fundFlowDirection;
}
