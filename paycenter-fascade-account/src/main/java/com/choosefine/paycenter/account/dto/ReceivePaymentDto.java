package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.account.service.AccountBillService;
import com.choosefine.paycenter.common.dto.BaseDto;
import com.choosefine.paycenter.common.enums.FundFlow;
import com.choosefine.paycenter.common.enums.TradeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 收款明细实体
 * Created by DengYouyi on 2017/3/30.
 */
@Getter
@Setter
@ApiModel("收款明细")
public class ReceivePaymentDto extends BaseDto implements Serializable {
        @ApiModelProperty("收款人租户ID")
        @NotBlank(groups = {AccountBillService.findReceivedPayment.class})
        private String userCode;
        @ApiModelProperty("资金来源/去向")
        @NotNull(groups = {AccountBillService.findReceivedPayment.class})
        private FundFlow fundFlow;
        @ApiModelProperty("交易类型")
        @NotNull(groups = {AccountBillService.findReceivedPayment.class})
        private TradeType tradeType;
        @ApiModelProperty("交易号")
        private String tradeSn;
        @ApiModelProperty("交易时间")
        private Timestamp tradeTime;
        @ApiModelProperty("银行卡号")
        private String bankCard;
        @ApiModelProperty("金额")
        private BigDecimal amount;
}
