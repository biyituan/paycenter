package com.choosefine.paycenter.account.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Comments：设置联行号与支行名称
 * Author：Jay Chang
 * Create Date：2017/5/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class AccountBankcardSetUBankCodeAndNameDto implements Serializable{
    /**租户编码*/
    private String userCode;
    /**账户银行卡关联关系id*/
    @NotNull
    @ApiModelProperty("accountBankcardId")
    private Long accountBankcardId;
    /**联行号*/
    @NotBlank
    @ApiModelProperty("bankUnionCode")
    private String bankUnionCode;
    /**支行名称*/
    @NotBlank
    @ApiModelProperty("bankUnionName")
    private String bankUnionName;
}
