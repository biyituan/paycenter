package com.choosefine.paycenter.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.math.BigDecimal;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-06 16:46
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountWdDto {
    private Long accountId;
    private BigDecimal freezeBalance;
    private BigDecimal availableBalance;
    private BigDecimal balance;
    private String userCode;


}
