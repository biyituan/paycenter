package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.enums.AccountBillStatus;
import com.choosefine.paycenter.common.enums.TradeType;
import com.choosefine.paycenter.common.jackson.EnumSerializer;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.choosefine.paycenter.common.model.BaseModel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 账单详情
 * Created by dyy on 2017/7/5.
 */
@Setter
@Getter
@ToString
public class AccountBillDetail extends BaseModel implements Serializable{
    @ApiModelProperty("交易号")
    private String orderSn;

    @ApiModelProperty("交易类型")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private TradeType tradeType;

    @ApiModelProperty("交易金额（单位：元）")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;

    @ApiModelProperty("交易对象的用户真实名称")
    private String targetRealName;

    @ApiModelProperty("交易对象的账户名")
    private  String targetAccountName;

    @ApiModelProperty("交易时间")
    private Long createTime;

    @ApiModelProperty("完成时间")
    private Long finishTime;

    @ApiModelProperty("状态")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private AccountBillStatus status;

    /**银行名称*/
    @ApiModelProperty("银行名称")
    private String bankName;
    /**银行账户号码（银行卡号）*/
    @ApiModelProperty("银行账户号码（银行卡号）")
    private String bankcardNo;

}
