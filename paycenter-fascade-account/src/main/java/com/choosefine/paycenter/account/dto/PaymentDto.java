package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ApiModel("支付单")
public class PaymentDto implements Serializable {
    /**交易主体账号id*/
    @ApiModelProperty("交易主体账号的租户编码")
    private Long accountId;
    /**交易主体账号名称*/
    @ApiModelProperty("交易主体真实名称（个人为真实姓名，公司为真实公司名称）")
    private String accountRealName;
    /**交易主体账号手机号*/
    @ApiModelProperty("交易主体手机号码")
    private String accountMobile;
    /**交易主体账号userCode*/
    @ApiModelProperty("交易主体账号的租户编码")
    private String userCode;
    /**交易对方信息列表*/
    @ApiModelProperty("交易对方信息列表（含交易对方账号id,交易金额）")
    @NotNull
    private List<OrderDto> orders;
    /**交易类型*/
    @ApiModelProperty("交易类型")
    @NotNull
    private TradeType tradeType;
    /**操作员名称*/
    @ApiModelProperty("操作员名称")
    @NotBlank
    private String operName;
    /**业务系统标识*/
    @ApiModelProperty("业务系统标识[目前区分劳务LABOR,商城SHOP,支付本身SELF]")
    @NotNull
    private BizzSys bizzSys;
    /**业务系统业务流水号*/
    @ApiModelProperty("业务系统业务流水号")
    private String bizzSn;
    @ApiModelProperty("交易总金额(单位：元)")
    private BigDecimal amount;
}

