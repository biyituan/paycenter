package com.choosefine.paycenter.account.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * 余额变更日志Dto
 * Created by Jay Chang on 2017/5/8.
 */
@Getter
@Setter
@ToString
public class AccountBalanceLogDto {
    private Long accountId;
    private String accountUserCode;
    private String accountRealName;
    private BigDecimal oldFreezeBalance;
    private BigDecimal oldAvailableBalance;
    private BigDecimal oldBalance;
    private BigDecimal newFreezeBalance;
    private BigDecimal newAvailableBalance;
    private BigDecimal newBalance;
    /**余额变更说明*/
    private String changeDesc;
}
