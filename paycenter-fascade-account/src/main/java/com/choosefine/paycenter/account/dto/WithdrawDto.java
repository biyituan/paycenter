package com.choosefine.paycenter.account.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class WithdrawDto {
    /**提现流水号*/
    private String wSn;
    private String userCode;
    @NotBlank
    private String operName;
    @NotNull
    private Long accountBankId;
    @NotBlank
    @Pattern(regexp = "^\\d{6}$",message = "paycenter.recharge.paypass.format.error")
    private String payPass;
    @NotNull
    private BigDecimal amount;
}
