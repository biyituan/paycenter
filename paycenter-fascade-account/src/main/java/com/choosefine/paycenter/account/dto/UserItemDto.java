package com.choosefine.paycenter.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Comment 租户信息
 * Author：Jay Chang
 * Create Date：2017/6/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class UserItemDto implements Serializable{
    private String userCode;
    private Long roleId;
}
