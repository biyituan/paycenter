package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.common.enums.AccountBillStatus;
import com.choosefine.paycenter.common.dto.BaseDto;
import com.choosefine.paycenter.common.enums.FundFlow;
import com.choosefine.paycenter.common.enums.TradeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Comments：账单查询条件BEAN
 * Author：DengYouyi
 * Create Date：2017/3/7
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ApiModel("账单流水")
public class AccountBillDto extends BaseDto implements Serializable {

    /**交易主体账户id*/
    @ApiModelProperty("交易主体账户租户编码")
    private String mainAccountUserCode;

    private Long mainAccountId;
    @ApiModelProperty("账单状态")
    private AccountBillStatus status;
    @ApiModelProperty("交易类型")
    private TradeType tradeType;

    @ApiModelProperty("开始时间")
    private Long startTime;

    @ApiModelProperty("结束时间")
    private Long endTime;

    @ApiModelProperty("交易备注")
    private String tradeMemo;

    @ApiModelProperty("距离当前月份")
     private Integer beforeMonth;

    /**资金流向*/
    @ApiModelProperty("资金流向")
    private FundFlow fundFlow;

}
