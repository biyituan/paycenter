package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.ShiXiaoBaoTrx;
import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Comments：会影响我们账户总金额的都要记录下来(且明确是成功的交易)
 * Author：Jay Chang
 * Create Date：2017/3/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface ShiXiaoBaoTrxService {
    public int recordShixiaobaoTrx(ShiXiaoBaoTrx shiXiaoBaoTrx);

    public ShiXiaoBaoTrx selectByTradeSn(String tradeSn);
}
