package com.choosefine.paycenter.account.service;

import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface AccountBankCardVerifyService {

    /**
     * 给账户转一笔随机款
     * @param accountBankCardId
     */
    void transferRandomMoney(Long accountBankCardId);


    /**
     * 验证打款金额
     * @param accountBankCardId
     * @param amount
     * @return
     */
    boolean verifyAmount(Long accountBankCardId, BigDecimal amount);


}
