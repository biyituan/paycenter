package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.model.AccountBalanceLog;

/**
 * Comments：账户余额日志服务
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface AccountBalanceLogService {
    /**记录余额变更日志*/
    int recordAccountBalanceLog(AccountBalanceLog accountBalanceLog);
}
