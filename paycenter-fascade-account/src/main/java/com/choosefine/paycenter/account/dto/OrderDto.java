package com.choosefine.paycenter.account.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:24
 **/
@Getter
@Setter
@ToString
public class OrderDto implements Serializable{
    private String orderSn;
    private String tradeMemo;
    /**必传*/
    private String userCode;
    /**非必传*/
    @JSONField(serialize=false)
    private String accountRealName;
    /**非必传*/
    @JSONField(serialize=false)
    private String accountMobile;
    /**非必传*/
    @JSONField(serialize=false)
    private Long accountId;
    private BigDecimal amount;
}
