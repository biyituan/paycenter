package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.model.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 账户银行卡
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_account_bankcard")
@ApiModel("账户银行卡")
public class AccountBankcard extends BaseModel implements Serializable {
    /**银行名称*/
    @ApiModelProperty("银行名称")
    private String bankName;
    /**开户名称(个人为姓名，公司为公司名称)*/
    @ApiModelProperty("开户名称(个人为姓名，公司为公司名称)")
    private String bankAccountName;

    @ApiModelProperty("支付通道ID")
    private String channelId;
    /**银行卡号*/
    @ApiModelProperty("银行卡号")
    private String bankcardNo;
    /**卡号位数*/
    @ApiModelProperty("银行卡号位数")
    private Integer bankcardDigits;
    /**卡绑定手机号*/
    @ApiModelProperty("卡绑定手机号")
    private String bankcardMobile;
    /**卡绑定身份证*/
    @ApiModelProperty("卡绑定身份证")
    private String idCardNo;
    /**是否是默认*/
    @ApiModelProperty("是否是默认")
    private String isDefault;
    @ApiModelProperty("卡类型")
    private String cardType;
    /**新增时间*/
    @ApiModelProperty("新增时间")
    private Timestamp createdAt;
    @ApiModelProperty("企业卡验证状态")
    private Integer validStatus;
    @ApiModelProperty("打款验证金额")
    private BigDecimal validAmount;
    @ApiModelProperty("是否是企业银行卡")
    private Integer isPublic;
    @ApiModelProperty("银行支行联行号")
    private String bankUnionCode;
    @ApiModelProperty("银行卡支行名")
    private String bankUnionName;
    @ApiModelProperty("账户ID")
    private Long accountId;
    @ApiModelProperty("银行卡LOGO")
    private String logo;
    @ApiModelProperty("银行CODE")
    private String bankCode;
    //银行请求流水号
    private String requestSn;
    //验证次数
    private Integer validCount;
}
