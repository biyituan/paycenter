package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.enums.RechargeStatus;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 充值提现
 * Created by Jay Chang on 2017/3/4.
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_recharge")
public class Recharge extends BaseModel implements Serializable {
    /**充值提现流水号*/
    private String rSn;
    /**资金账号id*/
    private Long accountId;
    /**资金账户真实姓名或公司名称*/
    private String accountRealName;
    /**资金账户租户编码*/
    private String accountUserCode;
    /**充值或提现金额（单位：分）*/
    private BigDecimal amount;
    /**充值完成时间，时间戳*/
    private Long finishedAt;
    /**银行页面回调url*/
    private String returnUrl;
    /**支付通道*/
    private String channel;
    /**支付类型*/
    private String payType;
    /**充值状态（充值流水不应该有CLOSED的状态）*/
    @Enumerated(EnumType.STRING)
    private RechargeStatus status;
    /**操作员*/
    private String operName;
    /**创建时间*/
    private Timestamp createdTime;
    /**更新时间*/
    private Timestamp updatedTime;
}
