package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.dto.*;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.AccountBill;
import com.choosefine.paycenter.account.model.AccountBillDetail;
import com.choosefine.paycenter.account.vo.BizzOrderVo;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.FundFlow;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * Comments：账单信息管理
 * Author：DengYouyi
 * Create Date：2017/3/7
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface AccountBillService {
    /**
     *
     *  @param rechargeDto
     * @param paySn
     */
    public void recordAccountBill(RechargeDto rechargeDto, Account paySn);

    /**
     *
     *
     * @param paymentDto
     * @param paySn
     */
    public void recordAccountBill(PaymentDto paymentDto, String paySn);

    public void recordAccountBill(AccountBill accountBill);

    /**
     * 根据业务系统标识与业务方订单号（支付流水号，充值流水号，提现流水号）查询账单列表(交易对方的列表)
     * @param bizzSys
     * @param orderSn
     * @return
     */
    public List<AccountBill> findAccountBillListByBizzSysAndOrderSn(BizzSys bizzSys,String orderSn);

    /**
     * 根据交易流水号（支付流水号，充值流水号，提现流水号）查询账单列表(交易对方的列表)
     * @param orderSn
     * @param fundFlow
     * @return
     */
//    public List<AccountBill> findAccountBillListByBizzSysAndOrderSn(String orderSn,FundFlow fundFlow);

    /**
     * 更新账单状态为交易成功
     * @param bizzSys 可以是支付单流水号或充值提现流水号
     * @param orderSn
     * @return
     */
    public int updateAccountBillSuccess(BizzSys bizzSys,String orderSn);

    /**
     * 更新账单状态为交易失败
     * @param bizzSys 可以是支付单流水号或充值提现流水号
     * @param  orderSn
     * @return
     */
    public int updateAccountBillFailure(BizzSys bizzSys,String orderSn);

    public Map<String, String> findAllTradeTypeByRoleId(Long roleId);

    public BizzOrderVo findBizzOrderByAccountId(Long accountBillId);

    public AccountBill findAccountBillById(Long accountBillId);

    /**
     * 根据账单ID查询账单详情
     * @param accountBillId
     * @return
     */
    public AccountBillDetail findAccountBillDetailById(Long accountBillId);

    public List<AccountBill> findAccountBillListByBizzSysAndBizzSn(BizzSys bizzSys, String bizzSn);

    public @interface FindPageByAccountId{};
    /**
     * 根据租户账户ID查询账单信息
     * @param accountBillDto
     * @return
     */
    public PageInfo<AccountBill> findPageByAccountId(AccountBillDto accountBillDto);

    public @interface findReceivedPayment{};

    /**
     * 查询收款明细
     * @param rpaymentDto
     * @return
     */
    public PageInfo<ReceivePaymentDto> findReceivedPayment (ReceivePaymentDto rpaymentDto);


    /**
     * 关闭账单（同时关闭支付订单或充值单）
     * @param userCode
     * @param id
     * @return
     */
    public void closeAccountBill(String userCode,Long id);

    /**
     * 关闭账单（同时关闭支付订单或充值单）
     * @param bizzSys
     * @param orderSn
     */
    public void closeAccountBill(List<AccountBill> accountBillList);

    /**
     * 根据业务系统标识与订单号更新银行名称与订单号
     * @param bizzSys
     * @param orderSn
     */
    public void recordBankNameByBizzSysAndOrderSn(BizzSys bizzSys,String orderSn,String bankName);
}
