package com.choosefine.paycenter.account.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Comments：清空手机号与账户名
 * Author：Jay Chang
 * Create Date：2017/6/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class AccountClearNameAndMobileDto implements Serializable{
    private String mobile;
    private List<UserItemDto> userList;
}
