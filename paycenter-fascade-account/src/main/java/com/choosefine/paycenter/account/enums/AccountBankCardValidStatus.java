package com.choosefine.paycenter.account.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

/**
 * 企业卡验证状态
 * Created by dyy on 2017/5/9.
 */
public enum  AccountBankCardValidStatus implements BaseEnum {
    WAIT(0,"验证中"),COMPLETE(1,"验证成功"),FAILURE(2,"验证失败");
    private int id;
    private String name;

    AccountBankCardValidStatus(int id,String name){
        this.id=id;
        this.name = name;
    }

    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }
}