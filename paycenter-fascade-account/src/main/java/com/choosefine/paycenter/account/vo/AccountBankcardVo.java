package com.choosefine.paycenter.account.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class AccountBankcardVo implements Serializable{
    /**银行名称*/
    private String bank;
    /**开户名称(个人为姓名，公司为公司名称)*/
    private String bank_account_name;
    /**DEPOSIT(储蓄卡)，CREDIT_CARD(信用卡)，DEBIT_CARD(借记卡)*/
    private String bankcard_type_id;
    /**银行卡号*/
    private String bankcard_no;
    /**卡绑定手机号*/
    private String bankcard_mobile;
    /**卡绑定身份证*/
    private String id_card;
    /**支付类型*/
    private String pay_type;
    /**是否是默认*/
    private String is_default;
    /**新增时间*/
    private Timestamp created_at;
    /**删除时间*/
    private Long deleted_at;
}
