package com.choosefine.paycenter.account.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/16
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum BankcardTypeEnum implements BaseEnum {
    PUBLIC(0,"企业"),
    PERSONEL(1,"个人");
    private Integer id;
    private String name;

    BankcardTypeEnum(Integer id, String name){
	    this.id=id;
	    this.name = name;
    }

    public Integer getId() {
        return id;
    }
    public String getName(){
        return this.name;
    }

}