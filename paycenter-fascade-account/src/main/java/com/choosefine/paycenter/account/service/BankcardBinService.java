package com.choosefine.paycenter.account.service;

import com.choosefine.paycenter.account.model.BankcardBin;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-23 11:21
 **/
public interface BankcardBinService {
    BankcardBin findBankcardBinByCardNum(String bankcardNum);
}
