package com.choosefine.paycenter.account.dto;

import com.choosefine.paycenter.account.service.AccountBankcardService;
import com.choosefine.paycenter.common.dto.BaseDto;
import com.choosefine.paycenter.common.enums.PayType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Comments：银行卡
 * Author：DengYouyi
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
public class AccountBankcardDto extends BaseDto implements Serializable {
    /**账户名称*/
    @ApiModelProperty("账户id")
    private Long accountId;

    @NotBlank(groups = {AccountBankcardService.AddCompanyAccountBankCard.class})
    @ApiModelProperty("银行CODE")
    private String bankCode;

    @NotBlank(groups = {AccountBankcardService.AddCompanyAccountBankCard.class})
    /**开户银行*/
    @ApiModelProperty("银行名称")
    private String bankName;

    /**银行账户名称*/
    @NotBlank(groups = {AccountBankcardService.AddPeronalAccountBankCard.class,AccountBankcardService.AddCompanyAccountBankCard.class})
    @ApiModelProperty("银行账户名称")
    private String bankAccountName;

    /**银行卡号*/
    @NotBlank(groups = {AccountBankcardService.AddPeronalAccountBankCard.class,AccountBankcardService.AddCompanyAccountBankCard.class})
    //@Pattern(regexp = "^\\d{10,}$",groups = {AccountBankcardService.AddPeronalAccountBankCard.class,AccountBankcardService.AddCompanyAccountBankCard.class},message = "银行卡号格式不正确")
    @ApiModelProperty("银行卡号")
    private String bankcardNo;

    /**支付通道id*/
    @ApiModelProperty("支付通道id")
    private Long channelId;


    /**银行卡的手机号码*/
    @ApiModelProperty("银行卡绑定的手机号码")
    private String bankcardMobile;

    @ApiModelProperty("是否默认")
    private String isDefault;

    @ApiModelProperty("新增时间")
    private Long createdAt;

    @ApiModelProperty("删除时间")
    private Long deleteAt;

    @ApiModelProperty("验证状态")
    private Integer validStatus;

    @ApiModelProperty("验证金额")
    private BigDecimal validAmount;

    @ApiModelProperty("是否是企业银行卡")
    private Integer isPublic;

    @NotBlank(groups = {AccountBankcardService.AddCompanyAccountBankCard.class})
    @ApiModelProperty("银行支行联行号")
    private String bankUnionCode;

    @NotBlank(groups = {AccountBankcardService.AddCompanyAccountBankCard.class})
    @ApiModelProperty("银行卡支行名")
    private String bankUnionName;

    @ApiModelProperty("银行卡LOGO")
    private String logo;

    @ApiModelProperty("支付方式")
    private String payType;
}
