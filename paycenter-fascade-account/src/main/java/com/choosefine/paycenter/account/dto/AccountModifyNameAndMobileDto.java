package com.choosefine.paycenter.account.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Comments：更改多个租户对应的登录账号的手机号码后，这些租户对应的资金账号的账户名与手机号也需要同步更改
 * Author：Jay Chang
 * Create Date：2017/6/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class AccountModifyNameAndMobileDto implements Serializable {
    private String oldMobile;
    private String newMobile;
    private List<UserItemDto> userList;
}
