package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.enums.AccountBillStatus;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.FundFlow;
import com.choosefine.paycenter.common.enums.TradeType;
import com.choosefine.paycenter.common.jackson.EnumSerializer;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.choosefine.paycenter.common.model.BaseModel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 账户账单
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_account_bill")
public class AccountBill extends BaseModel implements Serializable {
    /**交易号,注意：这里的tradeSn暂时只是为了统一格式、与ShixiaobaoTrx的tradeSn不一样，施小包流水的tradeSn为充值、提现、支付流水号*/
    @ApiModelProperty("交易号")
    private String billTradeSn;
    /**业务系统标识*/
    @ApiModelProperty("业务系统标识")
    @Enumerated(EnumType.STRING)
    private BizzSys bizzSys;
    /**业务系统业务订单汇总流水号*/
    @ApiModelProperty("业务系统业务订单汇总流水号（）")
    @Enumerated(EnumType.STRING)
    private String bizzSn;
    /**业务系统业务订单号*/
    @ApiModelProperty("业务系统业务订单号")
    private String orderSn;
    /**交易主体账户id*/
    @ApiModelProperty("交易主体账户id")
    private Long mainAccountId;
    /**交易主体账号租户编码*/
    @ApiModelProperty("交易主体账户租户编码")
    private String mainAccountUserCode;
    /**交易主体账户名*/
    @ApiModelProperty("交易主体账户名")
    private String mainAccountRealName;
    /**交易主体账户手机号码*/
    @ApiModelProperty("交易主体账号手机号")
    private String mainAccountMobile;
    /**银行名称*/
    @ApiModelProperty("银行名称")
    private String bankName;
    /**银行账户号码（银行卡号）*/
    @ApiModelProperty("银行账户号码（银行卡号）")
    private String bankcardNo;
    /**银行卡号位数*/
    @ApiModelProperty("银行卡号位数")
    private String bankcardDigits;
    /**交易对象的账户id*/
    @ApiModelProperty("交易对象的账户id")
    private Long oppositeAccountId;
    /**交易对象的账户名*/
    @ApiModelProperty("交易对象的账户名")
    private String oppositeAccountRealName;
    /**交易对象的租户编号*/
    @ApiModelProperty("交易对象的租户编号")
    private String oppositeAccountUserCode;
    /**交易对象的租户编号*/
    @ApiModelProperty("交易对象的租户手机号码")
    private String oppositeAccountMobile;
    @ApiModelProperty("交易图片，账号头像，公司logo,银行logo等")
    private String img;
    /**操作员名称*/
    @ApiModelProperty("操作员名称")
    private String operName;
    /**交易类型*/
    @ApiModelProperty("交易类型")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private TradeType tradeType;
    /**交易金额（单位：分）*/
    @ApiModelProperty("交易金额（单位：分）")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    /**状态*/
    @ApiModelProperty("状态")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private AccountBillStatus status;
    /**创建时间*/
    @ApiModelProperty("创建时间")
    private Timestamp createdAt;
    /**最后更新时间*/
    @ApiModelProperty("最后更新时间")
    private Timestamp updatedAt;
    /**交易备注*/
    @ApiModelProperty("交易备注")
    private String tradeMemo;
    /**资金流向*/
    @ApiModelProperty("资金流向")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private FundFlow fundFlow;
}
