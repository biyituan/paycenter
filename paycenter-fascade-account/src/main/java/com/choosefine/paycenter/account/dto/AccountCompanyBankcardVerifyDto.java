package com.choosefine.paycenter.account.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Comments：账户公司类型银行账号验证dto(校验验证款是否正确)
 * Author：Jay Chang
 * Create Date：2017/5/13
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
public class AccountCompanyBankcardVerifyDto {
    /**资金账户的银行账号id*/
    private Long accountBankcardId;
    /**验证打款金额*/
    private BigDecimal validAmount;
}
