package com.choosefine.paycenter.account.model;

import com.choosefine.paycenter.common.jackson.EnumSerializer;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.choosefine.paycenter.common.model.BaseModel;
import com.choosefine.paycenter.common.enums.FundFlowDirection;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 施小包流水(从我们平台总账号角度看)
 * Created by Jay Chang on 2017/3/7.
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_shixiaobao_trx")
public class ShiXiaoBaoTrx extends BaseModel implements Serializable{
    /**交易流水号*/
    private String tradeSn;
    /**资金账号id*/
    private Long accountId;
    /**租户编号*/
    private String userCode;
    /**租户真实名称，公司名称*/
    private String userName;
    /**操作员账号名称*/
    private String operName;
    /**类型（充值、提现、转账）*/
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class )
    private FundFlowDirection type;
    /**银行卡名称（建行借记卡，建行储蓄卡，建行信用卡）*/
    private String bankcardName;
    /**银行卡号*/
    private String bankcardNo;
    /**金额*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    /**手续费*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal commission;
    /**备注信息*/
    private String memo;
    /**交易创建时间（取支付单、充值单、提现单创建时间）*/
    private Timestamp transactionAt;
    /**创建时间*/
    private Timestamp createdAt;
    /**最后修改时间*/
    private Timestamp updatedAt;
}
