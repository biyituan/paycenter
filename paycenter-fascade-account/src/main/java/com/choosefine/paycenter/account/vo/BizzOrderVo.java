package com.choosefine.paycenter.account.vo;

import com.choosefine.paycenter.common.enums.BizzSys;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：根据账单id查询业务系统订单的信息
 * Author：Jay Chang
 * Create Date：2017/5/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class BizzOrderVo {
    private String bizzSys;
    private String bizzSn;
}
