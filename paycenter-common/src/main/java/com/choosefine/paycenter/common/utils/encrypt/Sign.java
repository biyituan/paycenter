package com.choosefine.paycenter.common.utils.encrypt;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface Sign {
    String sign(String sourceStr);
}
