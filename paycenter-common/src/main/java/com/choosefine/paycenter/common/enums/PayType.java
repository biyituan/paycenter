package com.choosefine.paycenter.common.enums;

/**
 * Comments：支付类型
 * Author：Jay Chang
 * Create Date：2017/4/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayType {
        /**
         * 获取支付类型
         * @return
         */
        String getType();
}
