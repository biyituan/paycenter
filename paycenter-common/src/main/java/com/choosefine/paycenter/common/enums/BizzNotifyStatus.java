package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum BizzNotifyStatus  implements BaseEnum {
    FAILURE("失败"),SUCCESS("成功");
    private String name;

    BizzNotifyStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
