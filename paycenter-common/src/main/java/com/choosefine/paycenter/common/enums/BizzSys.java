package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum  BizzSys implements BaseEnum {
    LABOR("劳务"),SHOP("商城"),SELF("支付系统自身");
    private String name;

    BizzSys(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
