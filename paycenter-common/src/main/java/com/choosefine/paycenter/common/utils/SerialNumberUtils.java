package com.choosefine.paycenter.common.utils;

import com.choosefine.paycenter.common.constants.SNFlagConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 流水号生成工具
 * Created by Dengyouyi on 2017/4/7.
 */
@Slf4j
@Component
public class SerialNumberUtils {
    @Autowired
    private RedissonClient redissonClient;

    private static final String DEFAULT_PATTERN = "yyyyMMddHHmmss";

    private static final String BIZZ_SN_PATTERN = "yyyyMMdd";

    private static final String BANK_REQUEST_DEFAULT_PATTERN = "yyyyMMdd";

    private static final String CHARS = "0123456789";
    /**
     * 当等于此数时重置为初始值
     */
    private static final long OPERATION_CODE = 100000000L;
    /**
     * 账单交易号自增
     */
    private static final String UNIQUE_COUNTER_BILL_TRADE_SN = "UNIQUE_COUNTER_BILL_TRADE_SN";
    /**
     * 支付流水号自增
     */
    private static final String UNIQUE_COUNTER_PAY_SN_NAME = "UNIQUE_COUNTER_PAY_SN";

    /**
     * 充值流水号自增
     */
    private static final String UNIQUE_COUNTER_R_SN_NAME = "UNIQUE_COUNTER_R_SN";
    /**
     * 提现流水号自增
     */
    private static final String UNIQUE_COUNTER_W_SN_NAME = "UNIQUE_COUNTER_W_SN";

    /**
     * 生成普通转账流水号(相当于业务流水号,只不过这个是支付系统的业务流水号，等同于劳务、商城的业务流水号)
     */
    private static final String UNIQUE_COUNTER_NORMAL_TRANSFER_SN_NAME = "UNIQUE_COUNTER_NORMAL_TRANSFER_SN";

    /**
     * 生成账单发起的流水号（相当于业务流水号，从账单发起的支付，实际作用是给予选择支付渠道、支付方式时，使用）
     */
    private static final String UNIQUE_COUNTER_BILL_BIZZ_SN_NAME = "UNIQUE_COUNTER_BILL_BIZZ_SN";

    /**
     * 银行转账请求查询流水号
     */
    private static final String UNIQUE_COUNTER_BANK_REQUEST_SN_NAME = "UNIQUE_COUNTER_BANK_REQUEST_SN";

    private RLock billTradeSnLock;
    private RLock paySnLock;
    private RLock wSnLock;
    private RLock rSnLock;
    private RLock bankRSnLock;


    /**业务系统流水号（这里业务系统指支付系统自身）*/
    private RLock transferSnLock;

    /**从账单发起需要生成*/
    private RLock payFromBillbizzSnLock;

    /**
     * 获取账单交易号,标识位0
     *
     * @return String
     */
    public String getBillTradeSn() {
        RLock billTradeSnLock = redissonClient.getLock(UNIQUE_COUNTER_BILL_TRADE_SN+"_LOCK");
        try {
            billTradeSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow())
                    .append(SNFlagConstants.BILL_TRADE_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_BILL_TRADE_SN)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取账单交易号的自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(billTradeSnLock);
        }
    }

    /**
     * 获取交易流水号,标识位1
     *
     * @return String
     */
    public String getPaySn() {
        RLock paySnLock = redissonClient.getLock(UNIQUE_COUNTER_PAY_SN_NAME+"_LOCK");
        try {
            paySnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow())
                    .append(SNFlagConstants.PAY_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_PAY_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取支付单的自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
           doUnlock(paySnLock);
        }
    }

    /**
     * 获取充值流水号,标识位2
     *
     * @return String
     */
    public String getRechargeSn() {
        RLock rSnLock = redissonClient.getLock(UNIQUE_COUNTER_R_SN_NAME+"_LOCK");
        try {
            rSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow())
                    .append(SNFlagConstants.R_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_R_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取充值单的自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(rSnLock);
        }
    }

    /**
     *
     * 获取银行转账查询流水号
     *
     * @return String
     */
    public String getBankRequestSn(){
        RLock bankRSnLock = redissonClient.getLock(UNIQUE_COUNTER_BANK_REQUEST_SN_NAME+"_LOCK");
        try {
            bankRSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(DateFormatUtils.format(System.currentTimeMillis(), BANK_REQUEST_DEFAULT_PATTERN))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_BANK_REQUEST_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取银行转账查询流水号自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(bankRSnLock);
        }
    }

    /**
     * 获取提现流水号,标识位3
     *
     * @return String
     */
    public String getWithdrawSn(){
        RLock wSnLock = redissonClient.getLock(UNIQUE_COUNTER_W_SN_NAME+"_LOCK");
        try {
            wSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow())
                    .append(SNFlagConstants.W_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_W_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取提现单的自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(wSnLock);
        }
    }

    /**
     * 获取从账单发起支付的业务流水号,标识位4
     *
     * @return String
     */
    public String getPayBizzSn(){
        RLock payFromBillbizzSnLock = redissonClient.getLock(UNIQUE_COUNTER_BILL_BIZZ_SN_NAME+"_LOCK");
        try {
            payFromBillbizzSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow(BIZZ_SN_PATTERN))
                    .append(SNFlagConstants.P_BIZZ_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_BILL_BIZZ_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取从账单发起支付的业务流水号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(payFromBillbizzSnLock);
        }
    }
    /**
     * 获取普通转账流水号（会传给创建支付单接口的，作为业务系统流水号）
     *
     */
    public String getSelfNormalTransferSn(){
        RLock transferSnLock = redissonClient.getLock(UNIQUE_COUNTER_NORMAL_TRANSFER_SN_NAME+"_LOCK");
        try {
            transferSnLock.tryLock(5000L, TimeUnit.MILLISECONDS);
            StringBuilder number = new StringBuilder();
            number.append(getNow(BIZZ_SN_PATTERN))
                    .append(SNFlagConstants.TRANSFER_BIZZ_SN_FLAG)
                    .append(RandomStringUtils.random(3, CHARS))
                    .append(String.format("%08d",getRedisIncrease(UNIQUE_COUNTER_NORMAL_TRANSFER_SN_NAME)));
            return number.toString();
        } catch (Exception e) {
            log.error("获取普通转账流水号的自增序号(8位)redis锁失败", e);
            throw new RuntimeException(e);
        } finally {
            doUnlock(transferSnLock);
        }
    }

    private void doUnlock(RLock rLock){
        try {
            if(null != rLock) {
                rLock.unlock();
            }
        }catch (Exception e){
            log.error("unlock RLock exception ",e);
        }
    }


    /**
     * 获取redis8位递增值
     *
     * @return String
     */
    private synchronized long getRedisIncrease(String counterName) {
        RAtomicLong atomicLong = redissonClient.getAtomicLong(counterName);
        long value = atomicLong.get();
        atomicLong.set(value = (++value >= OPERATION_CODE ? 0 :value));
        return value;
    }

    public static String getNow() {
        return getNow(DEFAULT_PATTERN);
    }


    public static String getNow(String pattern) {
        long currentTimeMillis = System.currentTimeMillis();
        return DateFormatUtils.format(currentTimeMillis,pattern);
    }


    /**
     * 是否为支付流水号
     * @param sn
     * @return
     */
    public boolean isPaySn(String sn){
        if(sn.length() <= 14) {return false;}
        return SNFlagConstants.PAY_SN_FLAG.equals(sn.substring(14,15));
    }

    /**
     * 是否为充值流水号
     * @param sn
     * @return
     */
    public boolean isRechargeSn(String sn){
        if(sn.length() <= 14) {return false;}
        return SNFlagConstants.R_SN_FLAG.equals(sn.substring(14,15));
    }

    /**
     * 是否为提现流水号
     * @param sn
     * @return
     */
    public boolean isWithdrawSn(String sn){
        if(sn.length() <= 14) {return false;}
        return SNFlagConstants.W_SN_FLAG.equals(sn.substring(14,15));
    }

    /**
     * 是否为账单交易号
     * @param sn
     * @return
     */
    public boolean isBillTradeSn(String sn){
        if(sn.length() <= 14) {return false;}
        return SNFlagConstants.BILL_TRADE_SN_FLAG.equals(sn.substring(14,15));
    }

    /**
     * 是否为从账单支付的业务流水号
     * @param sn
     * @return
     */
    public boolean isPayBizzSn(String sn){
        if(sn.length() <= 8) {return false;}
        return SNFlagConstants.P_BIZZ_SN_FLAG.equals(sn.substring(8,9));
    }

    /**
     * 是否为转账的业务流水号
     * @param sn
     * @return
     */
    public boolean isTransferBizzSn(String sn){
        if(sn.length() <= 8) {return false;}
        return SNFlagConstants.TRANSFER_BIZZ_SN_FLAG.equals(sn.substring(8,9));
    }
}