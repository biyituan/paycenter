package com.choosefine.paycenter.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.security.InvalidParameterException;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/6/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class PaycenterStringUtils {
    private static PaycenterStringUtils paycenterStringUtils= null;
    private PaycenterStringUtils(){}

    public synchronized static PaycenterStringUtils getInstance(){
        return null == paycenterStringUtils ? (paycenterStringUtils = new PaycenterStringUtils()) : paycenterStringUtils;
    }

    public String hiddenByChar(String sourceStr,int start ,int end , char replacedByChar){
        if(StringUtils.isEmpty(sourceStr)){
            return null;
        }
        if(end <= start){
            throw new InvalidParameterException("end must bigger than start");
        }
        if(sourceStr.length() <= start || sourceStr.length() <= end){
            throw new InvalidParameterException("start or end must less than sourceStr's length");
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0 ; i < (end - start + 1); i ++){
            sb.append(replacedByChar);
        }
        return sourceStr.substring(0,start)+sb.toString()+sourceStr.substring(end+1);
    }
}
