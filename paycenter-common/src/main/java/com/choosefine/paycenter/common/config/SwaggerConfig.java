package com.choosefine.paycenter.common.config;

import com.choosefine.paycenter.common.yml.APIVersionYml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Jay Chang on 2017/3/6.
 */

@EnableSwagger2
@Configuration
public class SwaggerConfig{

    @Autowired
    private APIVersionYml apiVersionYml;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.choosefine"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("趋恒科技-支付系统 RESTful API在线查询")
                .description("更多企业介绍和产品相关文章请关注：")
                .termsOfServiceUrl("")
                .contact("")
                .version(apiVersionYml.getVersion())
                .build();
    }
}
