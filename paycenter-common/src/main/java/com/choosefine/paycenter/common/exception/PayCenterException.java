package com.choosefine.paycenter.common.exception;

/**
 * @Author Ye_Wenda
 * @Date 7/12/2017
 */
public class PayCenterException extends RuntimeException {

    public PayCenterException(String message) {
        super(message);
    }

    public PayCenterException(String message, Throwable cause) {
        super(message, cause);
    }
}
