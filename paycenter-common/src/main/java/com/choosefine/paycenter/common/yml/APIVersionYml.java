package com.choosefine.paycenter.common.yml;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by Jay Chang on 2017/3/6.
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "sys.api")
public class APIVersionYml {
    private String version;
}
