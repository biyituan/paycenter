/**
 * 
 * Author：liaozhanggen
 * Create Date：2017年3月23日
 * Version：v2.0
 */
package com.choosefine.paycenter.common.utils;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.annotation.Resource;
import java.util.Locale;

/**
 * MessageSourceAutoConfiguration
 * 国际化工具类 根据key得到value值 
 * 
 * Author：Dengyouyi Create Date：2017年4月6日 Version：v2.0
 */
@Component
public class MessageSourceUtils {

	@Resource
	public MessageSource messageSource;

	/**
	 * 对应messages配置的key
	 *
	 * @Author：Dengyouyi
	 * @Create Date：2017年3月23日 @param：....
	 * @return：String
	 */
	public String getMessage(String code) {
		return this.getMessage(code, new Object[] {});
	}

	public String getMessage(String code, String defaultMessage) {
		return this.getMessage(code, null, defaultMessage);
	}

	public String getMessage(String code, String defaultMessage, Locale locale) {
		return this.getMessage(code, null, defaultMessage, locale);
	}

	public String getMessage(String code, Locale locale) {
		return this.getMessage(code, null, "", locale);
	}

	public String getMessage(String code, Object[] args) {
		return this.getMessage(code, args, "");
	}

	public String getMessage(String code, Object[] args, Locale locale) {
		return this.getMessage(code, args, "", locale);
	}

	public String getMessage(String code, Object[] args, String defaultMessage) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.getMessage(code, args, defaultMessage, locale);
	}

	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		return messageSource.getMessage(code, args, defaultMessage, locale);
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		// 设置默认区域,
		slr.setDefaultLocale(Locale.CHINA);
//		slr.setCookieMaxAge(3600);//设置cookie有效期.
		return slr;
	}
	
//	/**
//	 * cookie区域解析器;
//	 * @return
//	 */
//	@Bean
//	public LocaleResolver localeResolver() {
//	       FixedLocaleResolver slr = newFixedLocaleResolver ();
//	    //设置默认区域,
//	    slr.setDefaultLocale(Locale.US);
//	    return slr;
//	}
}
