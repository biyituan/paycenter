package com.choosefine.paycenter.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * Created by Jay Chang on 2017/3/6.
 */
@Component
public class ConfigAssember {

    @Autowired
    @Qualifier("yml")
    private Properties yml;

    /**
     * 拼接RESTful URL
     * @param key 配置文件key
     * @return
     */
    public String getRestUrl(String key) {
        return yml.get(key).toString();
    }

    /**
     * 从Application中取值（所有格式转为字符串）
     * @param key 配置文件key
     * @return
     */
    public Object get(String key) {
        return yml.get(key);
    }
}
