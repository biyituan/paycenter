package com.choosefine.paycenter.common.utils;

import org.redisson.api.RLock;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/20
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class LockHelper {
    private static LockHelper instance;
    private LockHelper(){
    }

    public static synchronized LockHelper getInstance(){
        return null != instance ? instance : (instance = new LockHelper());
    }

    public void unLock(RLock rLock){
        if(null == rLock){
            return;
        }
        try{
            rLock.unlock();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
