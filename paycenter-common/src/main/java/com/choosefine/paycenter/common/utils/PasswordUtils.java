package com.choosefine.paycenter.common.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by Jay Chang on 2017/3/7.
 */
public class PasswordUtils {
    private static final String CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+=-/?<>,.;:'\"[]{}|\\";
    private static PasswordUtils passwordUtils= null;
    private PasswordUtils(){}

    public synchronized static PasswordUtils getInstance(){
        return null == passwordUtils ? (passwordUtils = new PasswordUtils()) : passwordUtils;
    }

    /**
     * 生成盐
     * @return
     */
    public static String createSalt(){
        return DigestUtils.md5Hex(RandomStringUtils.random(32,CHARS));
    }

    /**
     * 生成安全支付密码
     *
     * @param plaintextPayPass
     * @param salt
     * @return
     */
    public static String generateSecurityPayPass(String plaintextPayPass,String salt){
        return DigestUtils.sha256Hex(salt+DigestUtils.md5Hex(plaintextPayPass)+salt);
    }
}
