package com.choosefine.paycenter.common.utils.encrypt;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum SignUtils implements Sign{
    MD5{
        @Override
        public String sign(String sourceStr) {
            return DigestUtils.md5Hex(sourceStr);
        }
    },
    SHA1{
        @Override
        public String sign(String sourceStr) {
            return DigestUtils.sha1Hex(sourceStr);
        }
    },
    SHA256{
        @Override
        public String sign(String sourceStr) {
            return DigestUtils.sha256Hex(sourceStr);
        }
    }
}
