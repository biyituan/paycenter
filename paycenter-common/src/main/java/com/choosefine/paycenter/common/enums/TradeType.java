package com.choosefine.paycenter.common.enums;


import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Comments：交易类型
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum TradeType implements BaseEnum {
   TRANSFER("转账"),WITHDRAW("提现"),RECHARGE("充值"),PAY_AGENT("代付"),SALARY("工资发放"),SUBPACKAGE("分包款打款"),SHOPPING("商城付款"),REFUND("退款");

   private String desc;

   public String getName(){
      return this.desc;
   }

   public void setName(String desc){
      this.desc = desc;
   }

   TradeType(String desc){
      this.desc = desc;
   }

   private static final Map<String, String> lookup = new HashMap<String, String>();

   static {
      for (TradeType s : EnumSet.allOf(TradeType.class)) {
         lookup.put(s.name(),s.getName());
      }
   }

   public static Map getLookUp(){
      return lookup;
   }
}
