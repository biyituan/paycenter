package com.choosefine.paycenter.common.utils;

import java.io.UnsupportedEncodingException;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/6
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class ByteArrayStringUtils {

    private static ByteArrayStringUtils byteArrayStringUtils;


    private ByteArrayStringUtils(){}

    public static synchronized ByteArrayStringUtils getInstance(){
        return byteArrayStringUtils != null ? byteArrayStringUtils : (byteArrayStringUtils = new ByteArrayStringUtils());
    }

    public byte[]  string2ByteArray(String string,String charsetName){
        byte[] byteArr = null;
        try {
            byteArr = string.getBytes(charsetName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return byteArr;
    }

    public String  byteArray2String(byte[] byteArr,String charsetName){
        String string = null;
        try {
            string = new String(byteArr,charsetName);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return string;
    }
}
