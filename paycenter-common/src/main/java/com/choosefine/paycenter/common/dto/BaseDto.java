package com.choosefine.paycenter.common.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Jay Chang on 2017/3/7.
 */
@Getter
@Setter
public class BaseDto {
    public static final int DEFAULT_PAGE_NUM = 1;
    public static final int DEFAULT_PAGE_SIZE = 10;

    private int pageNum  = DEFAULT_PAGE_NUM;
    private int pageSize = DEFAULT_PAGE_SIZE;
}
