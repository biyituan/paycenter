package com.choosefine.paycenter.common.exception;

import com.choosefine.paycenter.common.dto.FieldValidError;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by jaychang on 2017/3/5.
 */
@Getter
@Setter
public class ParamValidException extends Exception {
    private final List<FieldValidError> fieldValidErrors;

    public ParamValidException(List<FieldValidError> fieldValidErrors){
        this.fieldValidErrors = fieldValidErrors;
    }
}
