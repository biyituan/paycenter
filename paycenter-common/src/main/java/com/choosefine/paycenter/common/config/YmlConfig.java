package com.choosefine.paycenter.common.config;

import com.choosefine.paycenter.common.yml.APIVersionYml;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.util.Properties;

/**
 * Created by Jay Chang on 2017/3/6.
 */
@Slf4j
@Configuration
@EnableConfigurationProperties({ APIVersionYml.class})
public class YmlConfig {
    @Bean(name = "yml")
    public Properties yml() {
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
        Properties resultProperties = null;
        try {
            log.info("load yml config file from classpath:config/**/*.yml");
            yaml.setResources(resolver.getResources("classpath:config/**/*.yml"));
            resultProperties = yaml.getObject();
        } catch (Exception e) {
            log.error("cannot read yml config file from classpath:config/**/*.yml please check", e);
            resultProperties = null;
        }
        return resultProperties;
    }
}
