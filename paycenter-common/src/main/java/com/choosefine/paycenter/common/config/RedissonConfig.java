package com.choosefine.paycenter.common.config;

import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Configuration
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    @Value("${spring.redis.database}")
    private int database;

    @Value("${spring.redis.password}")
    private String password;

    private int DEFAULT_TIME_OUT = 5000;

    @Bean
    public RedissonClient createRedissonClient() {
        Config config = new Config();
        SingleServerConfig singleServerConfig = config.useSingleServer();
        singleServerConfig.setAddress("redis://"+host+":"+port);
        singleServerConfig.setTimeout(DEFAULT_TIME_OUT);
        if(StringUtils.isNotBlank(password)){
            singleServerConfig.setPassword(password);
        }
        singleServerConfig.setDatabase(database);
        return Redisson.create(config);
    }
}
