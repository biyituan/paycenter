package com.choosefine.paycenter.common.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Comments：账单状态
 * Author：Jay Chang
 * Create Date：2017/3/22
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum  AccountBillStatus implements BaseEnum {
    WAIT("等待付款"),CLOSED("交易关闭"),COMPLETE("交易成功"),FAILURE("交易失败"),PROCESSING("处理中");

    private String name;

    AccountBillStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    private static final Map<String, String> lookup = new HashMap<String, String>();

    static {
        for (AccountBillStatus s : EnumSet.allOf(AccountBillStatus.class)) {
            lookup.put(s.name(),s.getName());
        }
    }

    public static Map getLookUp(){
        return lookup;
    }
}
