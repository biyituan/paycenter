package com.choosefine.paycenter.common.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @desc
 * @author 戚羿辰
 * @Date 2017/01/06
 */
public class HttpUtil {

	private static final String[] IP_HEADERS_TO_TRY = { "X-Forwarded-For", "Proxy-Client-IP",
			"WL-Proxy-Client-IP", "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED",
			"HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP", "HTTP_FORWARDED_FOR", "HTTP_FORWARDED",
			"HTTP_VIA", "REMOTE_ADDR", "X-Real-IP" };

	/**
	 * 获取远程客户端 IP
	 */
	public static String getIPFromClient(HttpServletRequest request) {
		for (String header : IP_HEADERS_TO_TRY) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip.split(",")[0];
			}
		}
		return request.getRemoteAddr();
	}
}
