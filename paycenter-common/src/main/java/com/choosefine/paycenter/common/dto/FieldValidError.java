package com.choosefine.paycenter.common.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by yym on 2017/3/5.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FieldValidError implements Serializable{
    private String name;
    private String message;
}
