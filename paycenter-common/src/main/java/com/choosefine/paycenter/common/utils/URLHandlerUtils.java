package com.choosefine.paycenter.common.utils;

import org.springframework.web.util.DefaultUriTemplateHandler;

import java.util.Map;

public class URLHandlerUtils {
	
	/**
	 * 组装路径
	 *
	 * @Author：liaozhanggen
	 * @Create Date：2017年2月22日
	 * @param：	rootURL 协议路径  eg: http://localhost:8080/shixiaobao
	 * 			pathURL 访问的映射路径 eg: xx/xxx
	 * @return：StringBuffer
	 */
	public static String getUrl(String rootURL, String pathURL) {
		return getUrlBuffer(rootURL,pathURL).toString();
	}
	
	public static StringBuffer getUrlBuffer(String rootURL, String pathURL) {
		StringBuffer sb = new StringBuffer();
		sb.append(rootURL).append(pathURL);
		return sb;
	}

	public static String DefaultUriTemplateHandler(String uri,
												   Map<String, Object> params) {
		DefaultUriTemplateHandler uriTemplateHandler = new DefaultUriTemplateHandler();
		String url = uriTemplateHandler.expand(uri, params) + "";
		return url;
	}
}
