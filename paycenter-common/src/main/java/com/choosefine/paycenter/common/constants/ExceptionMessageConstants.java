package com.choosefine.paycenter.common.constants;

/**
 * 异常消息Message常量定义
 * 规则：1.常量注释写明对应消息资源的中文 2.常量命名规则：从资源文件第二个单词开始，全大写，“.”用“_”代替
 * Created by Dengyouyi on 2017/4/6.
 */
public class ExceptionMessageConstants {

    /**------------------------------账户----------------------------------------------------**/

    public final static String ACCOUNT_CHECK_ALREADY_LOCKED = "paycenter.account.check.alreadylocked";
    /**
     *  账户[{0}]已被冻结
     *  {0}: accountName
     */
    public final static String ACCOUNT_CHECK_ALREADYBLOCK = "paycenter.account.check.alreadyblock";

    /**
     *  支付密码输入不正确!
     */
    public final static String ACCOUNT_CHECK_PAYPASSERROR =  "paycenter.account.check.paypasserror";

    /**
     *  资金账号已经存在!
     */
    public final static String ACCOUNT_CHECK_ALREADYEXIST =  "paycenter.account.check.alreadyexist";

    /**
     *  资金账号不存在!
     */
    public final static String ACCOUNT_CHECK_NOTEXIST = "paycenter.account.check.notexist";

    /**
     *  账户[{0}]已被锁定
     *  {0}: accountName
     */
    public final static String ACCOUNT_CHECK_ALREADYLOCKED = "paycenter.account.check.alreadylocked";

    /**
     *  账户的支付密码未设置!
     */
    public final static String ACCOUNT_CHECK_PAYPASSNOTSET = "paycenter.account.check.paypassnotset";

    /**
     *  原支付密码输入不正确!
     */
    public final static String ACCOUNT_CHECK_OLDPAYPASSERROR =  "paycenter.account.check.oldpaypasserror";

    /**
     *  支付密码不能为空!
     */
    public final static String ACCOUNT_CHECK_PAYPASSNOTEMPTY =  "paycenter.account.check.paypassnotempty";

    /**
     * 支付密码必须是6位数字!
     */
    public final static String ACCOUNT_CHECK_PAYPASSLENGHTERROR = "paycenter.account.check.paypasslengtherror";

    /**
     * 资金账号名对应的资金账号不存在
     */
    public static final String ACCOUNT_CHECK_ACCOUNT_NAME_NOT_EXISTS = "paycenter.account.check.accountname.not.exists";

    /**
     * 清除资金账号的账户名与手机号码时候，根据租户编码、角色id、手机号码找不到资金账号
     */
    public static final String ACCOUNT_CLEAR_NAME_AND_MOBILE_CANNOT_FOUND = "paycenter.account.clear.name.and.mobile.cannot.found";

    /**
     * 更换资金账号的账户名与手机号码时候，根据租户编码、角色id、手机号码找不到资金账号
     */
    public static final String ACCOUNT_MODIFY_NAME_AND_MOBILE_CANNOT_FOUND = "paycenter.account.modify.name.and.mobile.cannot.found";

    /**------------------------------账单----------------------------------------------------**/
    /**
     *  该账号无账单!
     */
    public final static String ACCOUNTBILL_ACCOUNT_NOBILL = "paycenter.accountbill.account.nobill";

    public final static String ACCOUNTBILL_NOT_EXISTS = "paycenter.accountbill.not.exists";

    public static final String ACCOUNTBILL_CAN_NOT_CLOSE_NOT_BELONG_YOU = "paycenter.accountbill.cannot.close.not.belong.you";


    public static final String ACCOUNTBILL_PAYMENT_NOT_EXISTS = "paycenter.accountbill.payment.not.exists" ;



    /**------------------------------支付----------------------------------------------------**/
    /**
     *   账户余额不足!
     */
    public final static String  PAYTYPE_BALANCE_NOTENOUGH = "paycenter.paytype.balance.notenough";

    /**
     *  输入支付密码错误过多账户已被锁定，请点击忘记密码进行找回或10分钟00秒后重试!
     */
    public final static String  PAY_CHECK_PAYPASSTIMEERROR = "paycenter.pay.check.paypasstimeserror";

    /**
     *   付款方式还未确认!
     */
    public final static String  PAYTYPE_CHECK_NOTCHOOSE = "paycenter.paytype.check.notchoose";

    /**
     *   业务系统[{0}]的业务流水[{1}]的支付单已经存在，无需重复创建!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static String  PAYMENT_BIZZSN_ALREADYEXISTS = "paycenter.payment.bizzsn.alreadyexists";

    /**
     *   业务系统[{0}]产生的业务流水号[{1}]对应的支付单已经支付完成了，无需重复支付!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static String  PAYMENT_BIZZSN_ALREADYPAID = "paycenter.payment.bizzsn.alreadypaid";

    /**
     *   业务系统[{0}]，业务流水号[{1}]的支付单不存在!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static String  PAYMENT_BIZZSN_NOSUCHPAYMENT = "paycenter.payment.bizzsn.nosuchpayment";

    /**
     *   支付单不是由租户编码为[{0}]的资金账户创建的!
     *  {0}: userCode
     */
    public final static String  PAYMENT_ACCOUNT_ISNOTCREATEDBYCURRENTACCOUNT = "paycenter.payment.account.isnotcreatedbycurrentaccount";

    /**
     * 自己不能给自己进行支付
     */
    public static final String PAYMENT_CANNOT_PAY_TO_SELF = "paycenter.payment.cannot.pay.to.self";

    /**
     * 转账收款人只能是工人角色的账户
     */
    public static final String PAYMENT_RECEIVER_NOT_MATCH = "paycenter.payment.receiver.not.match";

    /**
     * 交易必须由原先的交易发起方发起付款
     */
    public static final String PAYMENT_TRADE_MUST_INITIATE_BY_INITIATOR="paycenter.payment.trade.must.initiate.by.inititator";

    /**
     *   支付单当前的付款方式不允许修改!
     */
    public final static String  PAY_PAYTYPE_NOTALLOWEDMODIFY = "paycenter.pay.paytype.notallowedmodify";

    /**
     *   交易主账号[{0}]的角色必须是建筑公司!
     *  {0}: accountId
     */
    public final static String  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTCONSTRUCTIONCOMPANY = "paycenter.pay.account.trademainaccountrolemustconstructioncompany";

    /**
     *   交易主账号[{0}]的角色必须是分包承包人!
     *  {0}: accountId
     */
    public final static String  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTTEAMUNDERTAKE = "paycenter.pay.account.trademainaccountrolemustteamundertake";

    /**
     *   交易对方账号[{0}]的角色必须是分包单位！
     *  {0}: accountId
     */
    public final static String  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTTEAMCOMPANY = "paycenter.pay.account.tradeoppositerolemustteamcompany";

    /**
     *   交易对方账号[{0}]的角色必须是工人！
     *  {0}: accountId
     */
    public final static String PAY_ACCOUNT_TRADEOPPOSITEROLEMUSTWORKER = "paycenter.pay.account.tradeoppositerolemustworker";

    /**
     *   业务订单至少要有1个！
     */
    public final static String PAY_ACCOUNT_TRADEORDER_CANNOT_EMPTY =  "paycenter.pay.account.tradeorder.cannotempty";

    /**
     *   支付单[{0}]不存在或已失效!
     *  {0}: bizzsn
     */
    public final static String  PAYMENT_NOTFOUND = "paycenter.pay.payment.notfound";

    /**
     * 当前支付类型必须设置returnUrl
     */
    public final static String PAYMENT_CURRENT_PAY_TYPE_MUST_SET_RETURN_URL = "payment.currentpaytype.mustset.returnul";

    /**
     * 当前支付类型的支付金额必须大于10元
     */
    public final static String PAYMENT_CURRENT_PAY_TYPE_MUST_BIGGER_THAN = "payment.current.paytype.amount.must.biggerthan";


    public final static String PAYMENT_CURRENT_ACCOUNT_CANNOT_CREATE_THIS_TRADE_TYPE="payment.current.acount.cannot.create.this.tradetype";

    public final static String PAYMENT_HAS_ORDER_IS_PAID="payment.has.order.is.paid";


    /**
     * 交易总金额与订单总金额不匹配
     */
    public static final String PAYMENT_TRADE_AMOUNT_NOT_MATCH = "payment.trade.amount.not.match";

    /**
     * 交易订单已经关闭
     */
    public static final String PAYMENT_TRADEORDER_IS_ALREADY_CLOSED = "paycenter.payment.tradeorder.already.closed";

    /**
     * 不是有效的转账业务流水号
     */
    public static final String PAYMENT_NOT_VALID_TRANSFER_BIZZ_SN = "paycenter.payment.not.valid.transfer.bizzSn";

    /**
     * 转账收款人只能有1个
     */
    public static final java.lang.String PAYMENT_RECEIVER_ONLY_ONE = "paycenter.payment.receiver.only.one";



    public static final java.lang.String PAYMENT_BIZZ_ORDER_AMOUNT_CANNOT_BE_ZERO = "paycenter.payment.bizzOrder.amount.cannot.be.zero";

    /**------------------------------提现----------------------------------------------------**/
    public final static String WITHDRAW_IS_ALREADY_EXIT = "paycenter.withdraw.is.already.exit";
    public final static String WITHDRAW_ACCOUNT_IS_NOT_MATCH = "paycenter.withdraw.account.is.not.match";

    /**------------------------------充值----------------------------------------------------**/
    public final static String RECHARGE_NOT_FOUND = "paycenter.recharge.not.found";


    /**-----------------------------银行卡----------------------------------------------------**/
    public static final String ACCOUNT_BANKCARD_ALREAD_EXISTS = "paycenter.account.bankcard.already.exists";
    public static final String ACCOUNT_BANKCARD_VALID_AMOUNT_INPUT_ERR = "paycenter.account.bankcard.valid.input.err";
    public static final String ACCOUNT_BANKCARD_ADD_ERR = "paycenter.account.bankcard.add.err";
    public static final String ACCOUNT_BANKCARD_NOT_CORRECT = "paycenter.account.bankcard.not.corrent";
    public static final String ACCOUNT_BANKCARD_AREADY_VALID = "paycenter.account.bankcard.already.valid";
    public static final String ACCOUNT_BANKCARD_NOT_EXISTS = "paycenter.account.bankcard.not.exists";
    public static final String ACCOUNT_BANKCARD_CANNOT_VERIFY_NOT_BELONG_YOU = "paycenter.account.bankcard.cannot.verify.not.belong.you";
    public static final String ACCOUNT_BANKCARD_IS_PERSON = "paycenter.account.bankcard.is.person";
    public static final String ACCOUNT_BANKCARD_ADD_ERR_WRONG_TO_MUCH = "paycenter.account.bankcard.add.err.wrong.to.much";


    public static final String ACCOUNT_BANKCARD_IS_NOT_VALID = "paycenter.account.bankcard.isnot.valid";
    public static final String ACCOUNT_BANKCARD_NOT_SUPPORT_CREDIT = "paycenter.account.bankcard.not.support.credit";
    public static final String ACCOUNT_BANKCARD_NOT_SUPPORT_OTHER_BANKCARD = "paycenter.account.bankcard.not.support.other.bankcard";
    public static final String ACCOUNT_BANKCARD_NOT_CORRECT_PUBLIC = "paycenter.account.bankcard.not.public.account";
    public static final String ACCOUNT_BANKCARD_FORMAT_NOT_CORRECT = "paycenter.account.bankcard.format.not.correct";
}
