package com.choosefine.paycenter.common.constants;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface IdempotentConstants {
    final static String PROCESSING = "PROCESSING";
    final static String COMPLETED = "COMPLETED";
}
