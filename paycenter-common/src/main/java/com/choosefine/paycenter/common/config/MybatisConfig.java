package com.choosefine.paycenter.common.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by Jay Chang on 2017/3/4.
 */
@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = {"com.choosefine.paycenter.account.dao","com.choosefine.paycenter.pay.dao","com.choosefine.paycenter.notify.dao","com.choosefine.paycenter.reconciliation.dao","com.choosefine.paycenter.settlement.dao"})
public class MybatisConfig {
}
