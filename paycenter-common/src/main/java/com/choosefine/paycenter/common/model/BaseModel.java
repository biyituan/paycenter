package com.choosefine.paycenter.common.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
public class BaseModel {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
