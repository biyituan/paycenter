package com.choosefine.paycenter.common.jackson;

import com.choosefine.paycenter.common.enums.BaseEnum;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class EnumSerializer extends JsonSerializer {
    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeString(((BaseEnum)o).getName());
    }
}
