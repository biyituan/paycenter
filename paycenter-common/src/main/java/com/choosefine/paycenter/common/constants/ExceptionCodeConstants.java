package com.choosefine.paycenter.common.constants;

/**
 * 异常编码code定义
 * Created by Dengyouyi on 2017/4/6.
 */
public class ExceptionCodeConstants {
    /**------------------------------账户--------------1001-1999--------------------------------------**/
    /**
     *  账户[{0}]已被冻结
     *  {0}: accountName
     */
    public final static int ACCOUNT_CHECK_ALREADYBLOCK = 1001;

    /**
     *  支付密码输入不正确!
     */
    public final static int ACCOUNT_CHECK_PAYPASSERROR =  1002;

    /**
     *  资金账号已经存在
     */
    public final static int ACCOUNT_CHECK_ALREADYEXIST =  1003;

    /**
     *  资金账号不存在!
     */
    public final static int ACCOUNT_CHECK_NOTEXIST = 1004;

    /**
     *  账户[{0}]已被锁定
     *  {0}: accountName
     */
    public final static int ACCOUNT_CHECK_ALREADYLOCKED = 1005;

    /**
     *  账户的支付密码未设置!
     */
    public final static int ACCOUNT_CHECK_PAYPASSNOTSET = 1006;

    /**
     *  原支付密码输入不正确!
     */
    public final static int ACCOUNT_CHECK_OLDPAYPASSERROR =  1007;

    /**
     *  支付密码不能为空!
     */
    public final static int ACCOUNT_CHECK_PAYPASSNOTEMPTY =  1008;

    /**
     * 支付密码必须是6位数字!
     */
    public final static int ACCOUNT_CHECK_PAYPASSLENGHTERROR = 1009;

    /**
     * 账户已经被锁定
     */
    public final static int ACCOUNT_CHECK_ALREADY_LOCKED = 1010;

    /**
     * 账号名对应的资金账号不存在
     */
    public static final int ACCOUNT_CHECK_ACCOUNT_NAME_NOT_EXISTS =1011 ;

    /**
     * 清除资金账号的账户名与手机号码时发现 ，通过租户编码，角色id,手机号码查不到该资金账号
     */
    public static final int ACCOUNT_CLEAR_NAME_AND_MOBILE_CANNOT_FOUND = 1012;

    /**
     * 修改资金账号的账户名与手机号码时发现 ，通过租户编码，角色id,手机号码查不到该资金账号
     */
    public static final int ACCOUNT_MODIFY_NAME_AND_MOBILE_CANNOT_FOUND = 1013;


    /**------------------------------账单---------------2001-2999-------------------------------------**/

    /**
     *  该账号无账单!
     */
    public final static int ACCOUNTBILL_ACCOUNT_NOBILL = 2001;

    /**
     * 账单不存在
     */
    public final static int ACCOUNTBILL_NOT_EXISTS = 2002;

    /**
     * 账单对应的支付单不存在
     */
    public static final int ACCOUNTBILL_PAYMENT_NOT_EXISTS = 2003;

    /**
     * 不能关闭不属于你的账单
     */
    public static final int ACCOUNTBILL_CAN_NOT_CLOSE_NOT_BELONG_YOU = 2003;



    /**------------------------------支付----------------3001-3999------------------------------------**/

    /**
     *   所选支付方式余额不足!
     */
    public final static int  PAYTYPE_BALANCE_NOTENOUGH = 3001;

    /**
     *   输入支付密码错误过多账户已被锁定，请点击忘记密码进行找回或10分钟后重试!
     */
    public final static int  PAY_CHECK_PAYPASSTIMEERROR = 3002;

    /**
     *   付款方式还未确认!
     */
    public final static int  PAYTYPE_CHECK_NOTCHOOSE = 3003;

    /**
     *   业务系统[{0}]的业务流水[{1}]的支付单已经存在，无需重复创建!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static int  PAYMENT_BIZZSN_ALREADYEXISTS = 3004;

    /**
     *   业务系统[{0}]产生的业务流水号[{1}]对应的支付单已经支付完成了，无需重复支付!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static int  PAYMENT_BIZZSN_ALREADYPAID = 3005;

    /**
     *   业务系统[{0}]，业务流水号[{1}]的支付单不存在!
     *  {0}: bizzSys.name(),{1}:bizzSn
     */
    public final static int  PAYMENT_BIZZSN_NOSUCHPAYMENT = 3006;

    /**
     *   支付单不是由租户编码为[{0}]的资金账户创建的!
     *  {0}: userCode
     */
    public final static int  PAYMENT_ACCOUNT_ISNOTCREATEDBYCURRENTACCOUNT = 3007;

    /**
     * 自己不能给自己支付
     */
    public static final int PAYMENT_CANNOT_PAY_TO_SELF = 3008;

    /**
     * 收款人角色不符合要求
     */
    public static final int PAYMENT_RECEIVER_NOT_MATCH = 3009;

    /**
     * 交易总金额与订单总金额不匹配
     */
    public static final int PAYMENT_TRADE_AMOUNT_NOT_MATCH = 3010;

    /**
     *   交易主账号[{0}]的角色必须是建筑公司
     *  {0}: accountId
     */
    public final static int  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTCONSTRUCTIONCOMPANY = 3009;

    /**
     *   交易主账号[{0}]的角色必须是分包承包人!
     *  {0}: accountId
     */
    public final static int  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTTEAMUNDERTAKE = 3010;

    /**
     *   交易对方账号[{0}]的角色必须是分包单位！
     *  {0}: accountId
     */
    public final static int  PAY_ACCOUNT_TRADEMAINACCOUNTROLEMUSTTEAMCOMPANY = 3011;

    /**
     *   交易对方账号[{0}]的角色必须是工人！
     *  {0}: accountId
     */
    public final static int PAY_ACCOUNT_TRADEOPPOSITEROLEMUSTWORKER = 3012;

    /**
     *   业务订单至少要有1个！
     */
    public final static int PAY_ACCOUNT_TRADEORDER_CANNOT_EMPTY =  3013;

    /**
     *   支付单[{0}]不存在或已失效!
     *  {0}: bizzsn
     */
    public final static int  PAYMENT_NOTFOUND = 3014;


    /**
     * 当前支付类型必须设置returnUrl(银行支付成功后的页面回调url)
     */
    public final static int PAYMENT_CURRENT_PAYTYPE_MUST_SET_RETURN_URL = 3015;


    /**
     * 当前资金账号的这个角色不能创建交易类型为xxx的支付订单
     */
    public static final int PAYMENT_CURRENT_ACCOUNT_CANNOT_CREATE_THIS_TRADE_TYPE = 3016;

    /**
     * 不是有效的转账业务流水号
     */
    public static final int PAYMENT_NOT_VALID_TRANSFER_BIZZ_SN = 3017;

    /**
     *   支付单当前的付款方式不允许修改!
     */
    public final static int  PAY_PAYTYPE_NOTALLOWEDMODIFY = 3017;

    /**
     *  建行B2B支付，金额必须大于10元
     */
    public static final int PAYMENT_CURRENT_PAY_TYPE_MUST_BIGGER_THAN = 3018;

    /**
     * 支付单中有订单已经支付完成
     */
    public static final int PAYMENT_HAS_ORDER_IS_PAID = 3019;


    /**
     * 交易必须由交易发起方发起
     */
    public static final int PAYMENT_TRADE_MUST_INITIATE_BY_INITIATOR = 3020;


    /**
     * 业务订单已经关闭了
     */
    public static final int PAYMENT_TRADEORDER_IS_ALREADY_CLOSED = 3021;


    /**
     * 收款人只能有1个
     */
    public static final int PAYMENT_RECEIVER_ONLY_ONE = 3022;

    /**
     * 业务订单金额不能为0
     */
    public static final int PAYMENT_BIZZ_ORDER_AMOUNT_CANNOT_BE_ZERO = 3023;


    /**------------------------------提现-----------------4001-4999-----------------------------------**/
    public final static int WITHDRAW_IS_EXIT = 4001;
    public final static int WITHDRAW_ACCOUNT_IS_NOT_MATCH=4002;

    /**------------------------------充值------------------5001-5999----------------------------------**/
    public final static int RECHARGE_NOT_FOUND = 5001;


    /**------------------------------银行卡------------------6001-6999---------------------------------**/
    public static final int ACCOUNT_BANKCARD_AREADY_EXISTS = 6001;

    public static final int ACCOUNT_BANKCARD_VALID_AMOUNT_INPUT_ERR = 6002;
    /**银行卡添加失败*/
    public static final int ACCOUNT_BANKCARD_ADD_ERR = 6003 ;
    public static final int ACCOUNT_BANKCARD_AREADY_VALID = 6004;
    public static final int ACCOUNT_BANKCARD_NOT_CORRECT = 6005;
    public static final int ACCOUNT_BANKCARD_NOT_EXISTS = 6006;
    public static final int ACCOUNT_BANKCARD_CANNOT_VERIFY_NOT_BELONG_YOU = 6007;
    public static final int ACCOUNT_BANKCARD_IS_PERSON = 6008;
    public static final int ACCOUNT_BANKCARD_ADD_ERR_WRONG_TO_MUCH = 6009 ;


    public static final int ACCOUNT_BANKCARD_IS_NOT_VALID = 6010;
    public static final int ACCOUNT_BANKCARD_NOT_SUPPORT_CREDIT = 6011;
    public static final int ACCOUNT_BANKCARD_NOT_SUPPORT_OTHER_BANKCARD = 6012;
    public static final int ACCOUNT_BANKCARD_NOT_CORRECT_PUBLIC = 6013;
    public static final int ACCOUNT_BANKCARD_FORMAT_NOT_CORRECT = 6014;
}
