package com.choosefine.paycenter.common.constants;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface SNFlagConstants {
    /**************************26位流水号****************************************/
    /**
     * 账单交易号
     */
    public static final String BILL_TRADE_SN_FLAG = "0";
    /**
     * 支付流水标志位
     */
    public static final String PAY_SN_FLAG = "1";
    /**
     * 充值流水标志位
     */
    public static final String R_SN_FLAG = "2";
    /**
     * 提现流水标志位
     */
    public static final String W_SN_FLAG = "3";
    /**************************26位流水号****************************************/


    /**************************20位流水号****************************************/
    /**
     * 账单发起支付业务流水号标志位
     */
    public static final String P_BIZZ_SN_FLAG="1";

    /**
     * 转账流水标志位
     */
    public static final String TRANSFER_BIZZ_SN_FLAG = "2";
    /**************************20位流水号****************************************/

}
