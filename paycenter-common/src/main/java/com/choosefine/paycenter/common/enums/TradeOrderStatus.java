package com.choosefine.paycenter.common.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Comments：交易订单状态
 * Author：Jay Chang
 * Create Date：2017/6/13
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum TradeOrderStatus implements BaseEnum {
    WAIT_PAY("待支付"),PAID("已支付"),FAILURE("付款失败"),CLOSED("已关闭");

    private String name;

    TradeOrderStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    private static final Map<String, TradeOrderStatus> lookup = new HashMap<String, TradeOrderStatus>();

    static {
        for (TradeOrderStatus s : EnumSet.allOf(TradeOrderStatus.class)) {
            lookup.put(s.getName(), s);
        }
    }

    public static TradeOrderStatus lookup(int value) {
        return lookup.get(value);
    }

    public static Map getLookUp(){
        return lookup;
    }
}
