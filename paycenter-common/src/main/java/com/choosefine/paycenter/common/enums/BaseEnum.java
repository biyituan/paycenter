package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface BaseEnum  {
    String getName();
}
