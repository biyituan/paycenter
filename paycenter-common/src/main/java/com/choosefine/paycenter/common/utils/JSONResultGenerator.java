package com.choosefine.paycenter.common.utils;

import com.choosefine.paycenter.common.dto.FieldValidError;
import com.choosefine.paycenter.common.dto.JSONResult;
import com.choosefine.paycenter.common.exception.BusinessException;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Jay Chang on 2017/3/6.
 */
public class JSONResultGenerator {
    private static final String SUCCESS_MESSAGE = "success";


    private JSONResultGenerator(){}

    public static JSONResultGenerator getInstance(){
        return new JSONResultGenerator();
    }

    /**
     * 操作处理成功
     * @param
     * @return
     */
    public static JSONResult buildOperSuccessResult(){
        return new JSONResult(HttpStatus.OK.value(),SUCCESS_MESSAGE);
    }

    /**
     * 操作处理成功
     * @param
     * @return
     */
    public static JSONResult buildCreatedSuccessResult(Long id){
        return new JSONResult(HttpStatus.OK.value(),SUCCESS_MESSAGE,new ResourceCreatedSuccessReturnData(id));
    }

    /**
     * 操作处理成功
     * @param
     * @return
     */
    public static JSONResult buildOperSuccessResult(int data){
        return new JSONResult(HttpStatus.OK.value(),SUCCESS_MESSAGE,data);
    }


    /**
     * 操作处理成功
     * @param
     * @return
     */
    public static JSONResult buildOperSuccessResult(Object data){
        return new JSONResult(HttpStatus.OK.value(),SUCCESS_MESSAGE,data);
    }

    public static JSONResult buildOperSuccessMessage(String message){
        return new JSONResult(HttpStatus.OK.value(),message);
    }
    /**
     * 处理业务异常
     * @param e
     * @return
     */
    public static JSONResult buildBusinessError(BusinessException e){
        return new JSONResult(HttpStatus.BAD_REQUEST.value(),e.getMessage());
    }

    public static JSONResult buildBusinessError(String message){
        return new JSONResult(HttpStatus.BAD_REQUEST.value(),message);
    }
    /**
     * 不支持该请求方法
     * @return
     */
    public static JSONResult buildMethodNotSupportError(HttpServletRequest req){
        return new JSONResult(HttpStatus.METHOD_NOT_ALLOWED.value(),"URL为["+req.getRequestURL() +"]的请求不支持"+req.getMethod()+"方法");
    }

    /**
     * 构造字段校验不通过返回结果
     * @param fieldValidError
     * @return
     */
    public static JSONResult buildValidError(List<FieldValidError> fieldValidError){
        return new JSONResult<>(HttpStatus.BAD_REQUEST.value(),"请求参数校验不通过",fieldValidError);
    }

    /**
     * 构造无法处理的Exception的返回结果
     * @param e
     * @return
     */
    public static JSONResult buildError(Throwable e){
        return new JSONResult(HttpStatus.INTERNAL_SERVER_ERROR.value(),"Internal server error: reason ["+e.getMessage()+"]",null);
    }

    /**
     * 构造查询成功后的返回结果
     * @param obj
     * @return
     */
    public static JSONResult buildQuerySuccessResult(Object obj){
        return new JSONResult(HttpStatus.OK.value(),SUCCESS_MESSAGE,obj);
    }

    /**
     * 请求的JSON格式有误
     * @return
     */
    public static JSONResult buildHttpMessageNotReadableError() {
        return new JSONResult(HttpStatus.BAD_REQUEST.value(),"请求不可读(可能原因：1.没有Request Body 2.Request Body格式有误)");
    }
}

class ResourceCreatedSuccessReturnData implements Serializable{
    private Long id;

    public ResourceCreatedSuccessReturnData(Long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
