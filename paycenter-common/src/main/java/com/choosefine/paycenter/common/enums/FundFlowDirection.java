package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/22
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum FundFlowDirection implements BaseEnum {
    TRANSFER("转账"),RECHARGE("充值"),WITHDRAW("提现");

    private String name;

    FundFlowDirection(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
