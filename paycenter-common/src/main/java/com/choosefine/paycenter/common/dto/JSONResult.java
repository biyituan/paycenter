package com.choosefine.paycenter.common.dto;

import lombok.*;

import java.io.Serializable;

/**
 * Created by Jay Chang on 2017/3/4.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JSONResult<T extends Object> implements Serializable{
    private Integer status;
    private String message;
    private T data;

    public JSONResult(Integer status ,String message){
        this.status = status;
        this.message = message;
    }
}
