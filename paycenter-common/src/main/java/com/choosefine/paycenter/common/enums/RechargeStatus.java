package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum RechargeStatus implements BaseEnum {
    WAITING("等待"),COMPLETE("完成"),FAILURE("失败");

    private String name;

    RechargeStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

}
