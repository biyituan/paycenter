package com.choosefine.paycenter.common.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum FundFlow implements BaseEnum {
    IN("资金流入"),OUT("资金流出");

    private String name;

    FundFlow(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
