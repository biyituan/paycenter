package com.choosefine.paycenter.common.enums;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 11:27
 **/
public enum WithdrawStatus implements BaseEnum{
    WAITING("系统处理中"),PROCESSING("银行处理中"),COMPLETE("完成"),FAILURE("失败");

    private String name;
    WithdrawStatus(String name){this.name=name;}
    @Override
    public String getName() {
        return this.name;
    }
}
