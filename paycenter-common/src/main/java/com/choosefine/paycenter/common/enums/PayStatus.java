package com.choosefine.paycenter.common.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 支付状态
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum PayStatus implements BaseEnum {
    WAIT_PAY("待支付"),PAID("已支付"),FAILURE("付款失败");

    private String name;

    PayStatus(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    private static final Map<String, PayStatus> lookup = new HashMap<String, PayStatus>();

    static {
        for (PayStatus s : EnumSet.allOf(PayStatus.class)) {
            lookup.put(s.getName(), s);
        }
    }

    public static PayStatus lookup(int value) {
        return lookup.get(value);
    }

    public static Map getLookUp(){
        return lookup;
    }
}
