package com.choosefine.paycenter.channel.ccb.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：建行B2B网银支付
 * Author：Jay Chang
 * Create Date：2017/3/31
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBB2BPayReqParams {
    /**商户号 【必需】*/
    private String MERCHANTID = "";
    /**商户柜台代码 【必需】*/
    private String POSID = "";
    /**分行代码 【必需】*/
    private String BRANCHID = "";
    /**商户订单号 【必需】*/
    private String ORDERID = "";
    /**付款金额 【必需】*/
    private String PAYMENT = "";
    /**币种  固定为01－人民币 【必需】*/
    private String CURCODE = "";
    /**交易码，固定为690401 【必需】*/
    private String TXCODE = "";
    /**备注1 【非必需】*/
    private String REMARK1  = "";
    /**备注2 【非必需】*/
    private String REMARK2 = "";
    /**超时时间，可验证订单处理截至时间。格式：yyyyMMddHHMISS 【非必需】*/
    private String TIMEOUT = "";
}


