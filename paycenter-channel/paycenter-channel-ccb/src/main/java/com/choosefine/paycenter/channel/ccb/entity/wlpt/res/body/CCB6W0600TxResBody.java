package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:42
 **/
@Getter
@Setter
@ToString
public class CCB6W0600TxResBody {
    private String CREDIT_NO; //凭证号
    private String DEAL_RESULT; //交易处理结果 返回状态为9时，稍后再调用6W0600查询处理结果
    private String MESSAGE; //错误原因
    private String REM1; //备注1
    private String REM2; //备注2  　
    private String CST_PAY_NO; //客户方流水号 　

}
