package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W0100TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Comments：NO.2 （6W0100）余额查询交易 Response
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@XStreamAlias("TX")
public class CCB6W0100TxRes extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB6W0100TxResBody txBody;
}

