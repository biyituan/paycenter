package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.pay.handler.AbstractTransferQueryHandler;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:48
 **/
@Getter
@Component("CCB_TRANSFER_QUERY_HANDLER")
public class CCBTransferQueryHandler extends AbstractTransferQueryHandler{
    @Autowired
    @Qualifier("ccbTransferQueryService")
    private TransferQueryService ccbTransferQueryService;

    @PostConstruct
    public void init() {
        this.transferQueryService=ccbTransferQueryService;
    }


    @Override
    public TransferQueryService getTransferService() {
        return ccbTransferQueryService;
    }
}
