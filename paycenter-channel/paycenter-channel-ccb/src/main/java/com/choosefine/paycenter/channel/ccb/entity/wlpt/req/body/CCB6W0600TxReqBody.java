package com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:30
 **/
@Getter
@Setter
@ToString
public class CCB6W0600TxReqBody {
    private String REQUEST_SN1; //原请求序列号,原转账交易请求序列号
    private String CREDIT_NO1=""; //原转账凭证号 varChar(12) T 原转账交易应答报文返回的凭证号
    private String CST_PAY_NO1=""; //原转账交易客户方流水号
}
