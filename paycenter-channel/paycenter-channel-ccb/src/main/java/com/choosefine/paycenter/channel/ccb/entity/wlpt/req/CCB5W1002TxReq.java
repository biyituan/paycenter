package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB5W1002TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W0100TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：（5W1002）商户支付流水查询 Request
 * Author：Jay Chang
 * Create Date：2017/6/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB5W1002TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB5W1002TxReqBody txBody;

    public CCB5W1002TxReq(){
        setTX_CODE("5W1002");
    }
}
