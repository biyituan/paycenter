package com.choosefine.paycenter.channel.ccb.api.impl;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.CCB6W0600TxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W0600TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.CCB6W0600TxRes;
import com.choosefine.paycenter.channel.ccb.utils.CCBWlptUtils;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import com.choosefine.paycenter.pay.vo.TransferQueryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:17
 **/
@Service("ccbTransferQueryService")
public class CCBTransferQueryServiceImpl implements TransferQueryService{
    @Value("${bank.ccb.wlptUrl}")
    private String ccbWlptUrl;
    @Autowired
    private CCBPayConfigStorage ccbPayConfigStorage;
    @Override
    public CCB6W0600TxRes sendTransferQuery(TransferQueryVo transferQueryVo) {
        CCB6W0600TxReqBody ccb6W0600TxReqBody =new CCB6W0600TxReqBody();
        ccb6W0600TxReqBody.setREQUEST_SN1(transferQueryVo.getOldRequestSn());
        CCB6W0600TxReq ccb6W0600TxReq = new CCB6W0600TxReq();
        ccb6W0600TxReq.setREQUEST_SN(transferQueryVo.getRequestSn());
        ccb6W0600TxReq.setCcb6W0600TxReqBody(ccb6W0600TxReqBody);
        ccb6W0600TxReq.setCUST_ID(ccbPayConfigStorage.getWlpt().getString("CUST_ID"));
        ccb6W0600TxReq.setUSER_ID(ccbPayConfigStorage.getWlpt().getString("USER_ID"));
        ccb6W0600TxReq.setPASSWORD(ccbPayConfigStorage.getWlpt().getString("PASSWORD"));
        CCB6W0600TxRes ccb6W0600TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlptUrl, ccb6W0600TxReq, CCB6W0600TxRes.class, ccbPayConfigStorage.getCa());
        return ccb6W0600TxRes;
    }

    @Override
    public String getResult(TransferQueryVo transferQueryVo) {
        CCB6W0600TxReqBody ccb6W0600TxReqBody =new CCB6W0600TxReqBody();
        ccb6W0600TxReqBody.setREQUEST_SN1(transferQueryVo.getOldRequestSn());
        CCB6W0600TxReq ccb6W0600TxReq = new CCB6W0600TxReq();
        ccb6W0600TxReq.setREQUEST_SN(transferQueryVo.getRequestSn());
        ccb6W0600TxReq.setCcb6W0600TxReqBody(ccb6W0600TxReqBody);
        ccb6W0600TxReq.setCUST_ID(ccbPayConfigStorage.getWlpt().getString("CUST_ID"));
        ccb6W0600TxReq.setUSER_ID(ccbPayConfigStorage.getWlpt().getString("USER_ID"));
        ccb6W0600TxReq.setPASSWORD(ccbPayConfigStorage.getWlpt().getString("PASSWORD"));
        CCB6W0600TxRes ccb6W0600TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlptUrl, ccb6W0600TxReq, CCB6W0600TxRes.class, ccbPayConfigStorage.getCa());
        String result=null;
        if(ccb6W0600TxRes.getRETURN_CODE().equals("000000")){
            if(2<=Integer.valueOf(ccb6W0600TxRes.getCcb6W0600TxResBody().getDEAL_RESULT())
                    &&4>=Integer.valueOf(ccb6W0600TxRes.getCcb6W0600TxResBody().getDEAL_RESULT())){
                return "success";
            }
            if(ccb6W0600TxRes.getCcb6W0600TxResBody().getDEAL_RESULT().equals("9")){
                return "processing";
            }
            result =ccb6W0600TxRes.getCcb6W0600TxResBody().getMESSAGE();
            return result;
        }
        //查询失败
        result=ccb6W0600TxRes.getRETURN_MSG();
        return result;
    }
}
