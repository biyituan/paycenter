package com.choosefine.paycenter.channel.ccb.utils;

import com.alibaba.fastjson.JSONObject;
import com.choosefine.paycenter.common.exception.PayCenterException;
import com.choosefine.paycenter.common.utils.XmlUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.security.KeyStore;

/**
 * Comments：建行外联平台接口操作工具类
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Getter
@Setter
public class CCBWlptUtils {

    private static CCBWlptUtils instance;

    private CCBWlptUtils(){
    }

    public synchronized static CCBWlptUtils getInstance(){
        return null == instance ? (instance = new CCBWlptUtils()):instance;
    }

    /**
     * 建行外联平台 post请求
     * @param reqObj
     * @param resClazz
     * @return
     */
    public <T> T postForCCBWlpt(String wlptUrl,Object reqObj,Class<T> resClazz){
        String requestXml = XmlUtils.getInstance().toXml(reqObj);
        String responseXml = postForCCBWlpt(wlptUrl,requestXml);
        return (T)XmlUtils.getInstance().fromXml(responseXml,resClazz);
    }

    /**
     * 建行外联平台 post请求
     * @param reqObj
     * @param resClazz
     * @return
     */
    public <T> T postForCCBWlpt(String wlptUrl, Object reqObj, Class<T> resClazz, JSONObject caConfig){
        String requestXml = XmlUtils.getInstance().toXml(reqObj);
        String responseXml = postForCCBWlpt(wlptUrl, requestXml, caConfig);
        return (T)XmlUtils.getInstance().fromXml(responseXml,resClazz);
    }

    /**
     * 建行外联平台 post请求
     * @param wlptUrl
     * @param requestXml
     * @return
     */
    public String postForCCBWlpt(String wlptUrl, String requestXml) {
        return postForCCBWlpt(wlptUrl,requestXml,"GBK");
    }

    /**
     * 建行外联平台 post请求
     * @param wlptUrl
     * @param requestXml
     * @return
     */
    public String postForCCBWlpt(String wlptUrl, String requestXml, JSONObject caConfig) {
        return postForCCBWlpt(wlptUrl, requestXml, "GBK", caConfig);
    }

    /**
     * 原生Http请求方式
     * @param urlStr
     * @param requestXml
     * @param encoding
     * @return
     */
    public String postForCCBWlpt(String urlStr,String requestXml,String encoding){
        if(urlStr.indexOf("https") >= 0){
            return useHttps(urlStr,requestXml,encoding);
        }else{
            return useHttp(urlStr,requestXml,encoding);
        }
    }

    /**
     * 原生Http请求方式 - 双向HTTPS验证
     * @param urlStr
     * @param requestXml
     * @param encoding
     * @param caConfig
     * @return
     */
    public String postForCCBWlpt(String urlStr, String requestXml, String encoding, JSONObject caConfig) {
        return useHttpsWithClientAuth(urlStr, requestXml, encoding, caConfig);
    }

    private String useHttp(String urlStr,String requestXml,String encoding){
        HttpURLConnection httpConn = null;
        OutputStreamWriter osw = null;
        BufferedReader responseReader = null;
        try {
            //建立连接
            URL url = new URL(urlStr);
            httpConn = (HttpURLConnection) url.openConnection();
            //设置参数
            httpConn.setDoOutput(true);   //需要输出
            httpConn.setDoInput(true);   //需要输入
            httpConn.setUseCaches(false);  //不允许缓存
            httpConn.setRequestMethod("POST");   //设置POST方式连接
            //设置请求属性
            httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConn.setRequestProperty("Connection", "close");
            httpConn.setRequestProperty("Charset", encoding);
            //连接,也可以不用明文connect，使用下面的httpConn.getOutputStream()会自动connect
            log.info(requestXml.replaceAll("<PASSWORD>\\w+</PASSWORD>", "<PASSWORD>******</PASSWORD>"));
            httpConn.connect();
            //建立输入流，向指向的URL传入参数
            osw = new OutputStreamWriter(httpConn.getOutputStream(),encoding);
            osw.write("requestXml="+requestXml);
            osw.flush();
            //获得响应状态
            int resultCode = httpConn.getResponseCode();
            if (HttpURLConnection.HTTP_OK == resultCode) {
                StringBuffer sb = new StringBuffer();
                String readLine = new String();
                responseReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), encoding));
                while ((readLine = responseReader.readLine()) != null) {
                    sb.append(readLine).append("\n");
                }
                return sb.toString();
            }
            return null;
        }catch (Exception e){
            log.error("请求建行外联平台客户端失败,{}",e.getMessage());
            throw new RuntimeException("request wlpt client error,maybe it havn't started",e);
        }finally {
            closeQuietly(responseReader);
            closeQuietly(osw);
            if(null != httpConn) {
                httpConn.disconnect();
            }
        }
    }

    private String useHttps(String urlStr,String requestXml,String encoding){
        HttpsURLConnection httpConn = null;
        OutputStreamWriter osw = null;
        BufferedReader responseReader = null;
        try {
            //建立连接
            URL url = new URL(urlStr);
            httpConn = (HttpsURLConnection) url.openConnection();
            //设置参数
            httpConn.setDoOutput(true);   //需要输出
            httpConn.setDoInput(true);   //需要输入
            httpConn.setUseCaches(false);  //不允许缓存
            httpConn.setRequestMethod("POST");   //设置POST方式连接
            //设置请求属性
            httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConn.setRequestProperty("Connection", "close");
            httpConn.setRequestProperty("Charset", encoding);
            //连接,也可以不用明文connect，使用下面的httpConn.getOutputStream()会自动connect
            log.info(requestXml);
            httpConn.connect();
            //建立输入流，向指向的URL传入参数
            osw = new OutputStreamWriter(httpConn.getOutputStream(),encoding);
            osw.write("requestXml="+requestXml);
            osw.flush();
            //获得响应状态
            int resultCode = httpConn.getResponseCode();
            if (HttpURLConnection.HTTP_OK == resultCode) {
                StringBuffer sb = new StringBuffer();
                String readLine = new String();
                responseReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream(), encoding));
                while ((readLine = responseReader.readLine()) != null) {
                    sb.append(readLine).append("\n");
                }
                return sb.toString();
            }
            return null;
        }catch (Exception e){
            log.error("请求建行外联平台客户端失败,{}",e.getMessage());
            throw new RuntimeException("request wlpt client error,maybe it havn't started",e);
        }finally {
            closeQuietly(responseReader);
            closeQuietly(osw);
            if(null != httpConn) {
                httpConn.disconnect();
            }
        }
    }

    private String useHttpsWithClientAuth(String urlStr, String requestXml, String encoding, JSONObject caConfig) {
        HttpsURLConnection httpConn = null;
        OutputStreamWriter osw = null;
        BufferedReader responseReader = null;
        try {
            // 初始化密钥库
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            KeyStore keyStore = getKeyStore(caConfig.getString("KEY_FILE_CLIENT"), caConfig.getString("KEY_PWD_CLIENT"), "PKCS12");
            keyManagerFactory.init(keyStore, caConfig.getString("KEY_PWD_CLIENT").toCharArray());

            // 初始化信任库
            TrustManagerFactory trustManagerFactory = TrustManagerFactory
                    .getInstance("SunX509");
            KeyStore trustkeyStore = getKeyStore(caConfig.getString("KEY_FILE_TRUST"), caConfig.getString("KEY_PWD_TRUST"), "JKS");
            trustManagerFactory.init(trustkeyStore);

            // 初始化SSL上下文
            SSLContext ctx = SSLContext.getInstance("SSL");
            ctx.init(keyManagerFactory.getKeyManagers(), trustManagerFactory
                    .getTrustManagers(), null);
            SSLSocketFactory sf = ctx.getSocketFactory();

            HttpsURLConnection.setDefaultSSLSocketFactory(sf);
            URL urlObj = new URL(urlStr);
            httpConn = (HttpsURLConnection) urlObj.openConnection();
            //设置参数
            httpConn.setDoOutput(true);   //需要输出
            httpConn.setDoInput(true);   //需要输入
            httpConn.setUseCaches(false);  //不允许缓存
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpConn.setRequestProperty("Connection", "close");
            httpConn.setRequestProperty("Charset", encoding);
            //建立输入流，向指向的URL传入参数
            osw = new OutputStreamWriter(httpConn.getOutputStream(), encoding);
            osw.write("requestXml=" + requestXml);
            osw.flush();

            String response = readResponseBody(httpConn.getInputStream(), encoding);
            return response;
        } catch (Exception e) {
            String hint = "请求建行外联平台客户端失败！";
            log.error(hint, e);
            throw new PayCenterException(hint, e);
        }
    }

    private KeyStore getKeyStore(String keyStorePath, String password, String type) {
        try {
            InputStream is = CCBWlptUtils.class.getClassLoader().getResourceAsStream(keyStorePath);
            KeyStore ks = KeyStore.getInstance(type);
            ks.load(is, password.toCharArray());
            is.close();
            return ks;
        } catch (Exception e) {
            String hint = "证书文件 - " + keyStorePath + " 地址或密码有误！";
            log.error(hint, e.getMessage());
            throw new PayCenterException(hint, e);
        }
    }

    private String readResponseBody(InputStream inputStream, String encode) throws IOException {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, encode));
            StringBuffer sb = new StringBuffer();
            String buff = null;
            while((buff = br.readLine()) != null){
                sb.append(buff+"\n");
            }
            return sb.toString();
        } finally {
            inputStream.close();
        }
    }

    /**
     * 使用socket方式调用接口(存在不建议用)
     * @param host
     * @param port
     * @param requestXml
     * @param encoding
     * @return
     */
    public String sendSocketRequest(String host,int port,String requestXml,String encoding){
        OutputStreamWriter osw = null;
        BufferedReader br = null;
        Socket socket = null;
        try {
            socket = new Socket(host,port);
            socket.setKeepAlive(false);
            osw = new OutputStreamWriter(socket.getOutputStream(),encoding);
            osw.write(requestXml);
            osw.flush();
            InputStream ins = socket.getInputStream();
            br = new BufferedReader(new InputStreamReader(ins, encoding));
            String s = null;
            StringBuilder sb = new StringBuilder();
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            return sb.toString();
        }catch (Exception e){
            throw new RuntimeException(e);
        }finally {
            closeQuietly(osw);
            closeQuietly(br);
            closeQuietly(socket);
        }
    }

    public String sendSocketRequest(String host, int port, String requestXml) {
        return sendSocketRequest(host,port,requestXml,"GBK");
    }

    private void closeQuietly(Socket socket) {
        try {
            if(null != socket) {
                socket.close();
            }
        } catch (IOException e) {
        }
    }

    private void closeQuietly(OutputStreamWriter osw){
        if(null!= osw){
            try {
                osw.close();
            } catch (IOException e) {
            }
        }
    }

    private void closeQuietly(OutputStream outputStream) {
        try {
            if(null != outputStream) {
                outputStream.close();
            }
        } catch (IOException e) {
        }
    }

    private void closeQuietly(InputStream inputStream) {
        try {
            if(null != inputStream) {
                inputStream.close();
            }
        } catch (IOException e) {
        }
    }

    private void closeQuietly(BufferedReader br) {
        try {
            if(null != br) {
                br.close();
            }
        } catch (IOException e) {
        }
    }
}
