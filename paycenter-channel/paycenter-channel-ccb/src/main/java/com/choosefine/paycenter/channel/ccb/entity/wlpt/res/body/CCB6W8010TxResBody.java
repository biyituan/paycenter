package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import lombok.Getter;
import lombok.Setter;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
public class CCB6W8010TxResBody{
   /** 8 CREDIT_NO 凭证号 Char(12) F 　*/
   private String CREDIT_NO;
   /** 9 DEAL_TYPE 处理方式 Char(1) F 调用6W0600查询处理结果*/
   private String DEAL_TYPE;
   /** 10 VALIDATE_CODE 验证码 varChar(30) T 新加，交易成功时返回*/
   private String VALIDATE_CODE;
   /** 11  INDIVIDUAL_NAME1 自定义输出名称1 varChar(99) T 分行自定义输出名称1　*/
   private String INDIVIDUAL_NAME1;
   /** 12  INDIVIDUAL1 自定义输出内容1 varChar(99) T*/
   private String INDIVIDUAL1;
   /** 13 INDIVIDUAL_NAME2 自定义输出名称2 varChar(99) T 分行自定义输出名称2 */
   private String INDIVIDUAL_NAME2;
   /** 14  INDIVIDUAL2 自定义输出内容2 varChar(99) T 　*/
   private String INDIVIDUAL2;
   /** 15 REM1 备注1 varChar(32) T 　*/
   private String REM1;
   /** 16 REM2 备注2 varChar(32) T 　*/
   private String REM2;
   /** 17 CST_PAY_NO 客户方流水号 varChar(60) T 　*/
   private String CST_PAY_NO;
}
