package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W8020TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.6 （6W8020）跨行单笔转账（大小额） Request
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W8020TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB6W8020TxReqBody txBody;
    /**18 SIGN_INFO 签名信息 varChar(254) T*/
    private String SIGN_INFO = "";
    /**19 SIGNCERT 签名CA信息 varChar(254) T 客户采用socket连接时，建行客户端自动添加*/
    private String SIGNCERT = "";
    public CCB6W8020TxReq(){
        setTX_CODE("6W8020");
    }
}
