package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/6/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBTradeItem {
    /** 11	TRAN_DATE	交易日期	varChar(32)	T*/
    private String TRAN_DATE = "";
    /** 12	ACC_DATE	记账日期	varChar(32)	T*/
    private String ACC_DATE = "";
    /** 13	ORDER	订单号	varChar(30)	T*/
    private String ORDER = "";
    /** 14	ACCOUNT	付款方账号	varChar(30)	T*/
    private String ACCOUNT = "";
    /** 15	PAYMENT_MONEY	支付金额	Decimal(16,2)	T*/
    private String PAYMENT_MONEY = "";
    /** 16	REFUND_MONEY	退款金额	Decimal(16,2)	T*/
    private String REFUND_MONEY = "";
    /** 17	POS_ID	柜台号	varChar(9)	T*/
    private String POS_ID = "";
    /** 18	REM1	备注1	varChar(32)	T*/
    private String REM1 = "";
    /** 19	REM2	备注2 	varChar(32)	T*/
    private String REM2 = "";
    /** 20	ORDER_STATUS	订单状态	Char(1)	T	0:失败,1:成功,2:待银行确认,3:已部分退款,4:已全额退款,5:待银行确认*/
    private String ORDER_STATUS = "";

}
