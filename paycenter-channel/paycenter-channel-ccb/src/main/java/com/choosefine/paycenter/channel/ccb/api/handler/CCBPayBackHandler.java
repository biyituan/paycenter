package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.channel.ccb.api.impl.CCBPayType;
import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractPayBackHandler;
import com.choosefine.paycenter.pay.dto.PayBackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * Comments：建行支付回调handler
 * Author：Jay Chang
 * Create Date：2017/4/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component("CCB_PAY_BACK_HANDLER")
public class CCBPayBackHandler extends AbstractPayBackHandler {

    @Autowired
    private PayService ccbPayService;

    private final static String SUCCESS = "Y";

    /**B2B支付成功标志*/
    private final static String B2B_SUCCESS = "2";
    /**B2B支付失败标志*/
    private final static String B2B_FAILURE = "5";

    @PostConstruct
    public void init(){
        setPayService(ccbPayService);
    }

    @Override
    protected String getSign(Map<String, String> params,String payType) {
        if(CCBPayType.B2B_NB.getType().equalsIgnoreCase(payType)){
            return params.get("SIGNSTRING");
        }
        return params.get("SIGN");
    }

    /**
     *  支付成功标志参数为Y 则表示支付成功
     * @param params
     * @return
     */
    @Override
    protected boolean isPaySuccess(Map<String, String> params,String payType) {
        if(CCBPayType.B2B_NB.getType().equalsIgnoreCase(payType)){
            return B2B_SUCCESS.equals(params.get("STATUS"));
        }
        return SUCCESS.equals(params.get("SUCCESS"));
    }

    /**
     * 建行并不会返回错误信息，故返回空字符串
     * @param payBackDto
     * @return
     */
    @Override
    public String getPayErrorMsg(PayBackDto payBackDto) {
        return "";
    }
}
