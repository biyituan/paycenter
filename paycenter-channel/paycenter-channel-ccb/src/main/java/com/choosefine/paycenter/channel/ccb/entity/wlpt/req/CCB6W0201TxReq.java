package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W0201TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.1 （6W0201）超级网银联行号查询 Request
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W0201TxReq extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB6W0201TxReqBody txBody;

    public CCB6W0201TxReq(){
        setTX_CODE("6W0201");
    }
}

