package com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB6W1101TxReqBody{
    /** 7 ACC_NO 账号 varChar(32) F 账号*/
    private String ACC_NO = "";
    /** 8 ACC_NAME 账户名称 varChar(100) F 账户名称*/
    private String ACC_NAME = "";


}
