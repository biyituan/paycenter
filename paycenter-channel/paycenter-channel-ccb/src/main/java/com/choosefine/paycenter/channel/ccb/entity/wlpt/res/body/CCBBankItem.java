package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBBankItem {
    /** 7 BANK_CODE 银行联行号 Char(12) F*/
    private String BANK_CODE;
    /** 8 BANK_NAME 银行名称 varchar(60) F*/
    private String BANK_NAME;

}
