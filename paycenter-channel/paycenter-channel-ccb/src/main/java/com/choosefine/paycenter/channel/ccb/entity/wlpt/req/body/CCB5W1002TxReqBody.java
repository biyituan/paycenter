package com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.6 （6W8020）跨行单笔转账（大小额） Body
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB5W1002TxReqBody {
    /** 7	START    	起始日期	varChar(8)	T*/
    private String START = "";
    /** 8	STARTHOUR	开始小时	varChar(2)	T*/
    private String STARTHOUR = "";
    /** 9	STARTMIN 	开始分钟	varChar(2)	T*/
    private String STARTMIN = "";
    /** 10	END      	截止日期	varChar(8)	T*/
    private String END = "";
    /** 11	ENDHOUR  	结束小时	varChar(2)	T*/
    private String ENDHOUR = "";
    /** 12	ENDMIN   	结束分钟	varChar(2)	T*/
    private String ENDMIN = "";
    /** 13	KIND     	流水类型	Char(1)	F	0:未结流水,1:已结流水*/
    private String KIND = "";
    /** 14	ORDER    	订单号  	varChar(30)	F	按订单号查询时，时间段不起作用*/
    private String ORDER = "";
    /** 15	ACCOUNT  	结算账户号	varChar(30)	T	暂不用*/
    private String ACCOUNT = "";
    /** 16	DEXCEL   	文件类型	Char(1)	F	默认为“1”，1:不压缩,2.压缩成zip文件*/
    private String DEXCEL = "";
    /** 17	MONEY    	金额    	Decimal(16,2)	T	不支持以金额查询*/
    private String MONEY = "";
    /** 18	NORDERBY 	排序    	Char(1)	F	1:交易日期,2:订单号*/
    private String NORDERBY = "";
    /** 19	PAGE     	当前页次	Int	F*/
    private String PAGE = "";
    /** 20	POS_CODE 	柜台号  	varChar(9)	T*/
    private String POS_CODE = "";
    /** 21	STATUS   	流水状态	Char(1)	F	0:交易失败,1:交易成功,2:待银行确认(针对未结流水查询);3:全部*/
    private String STATUS = "";

}
