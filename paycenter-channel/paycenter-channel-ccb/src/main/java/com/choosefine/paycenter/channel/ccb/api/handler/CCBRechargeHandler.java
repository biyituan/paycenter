package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractRechargeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component("CCB_RECHARGE_HANDLER")
public class CCBRechargeHandler extends AbstractRechargeHandler {
    @Autowired
    @Qualifier("ccbPayService")
    private PayService ccbPayService;

    @PostConstruct
    public void init() {
        this.payService = ccbPayService;
    }
}
