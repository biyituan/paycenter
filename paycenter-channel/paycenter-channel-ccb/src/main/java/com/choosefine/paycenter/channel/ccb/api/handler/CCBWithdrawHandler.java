package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.settlement.dto.WithdrawDTO;
import com.choosefine.paycenter.settlement.handler.AbstractWithdrawHandler;
import com.choosefine.paycenter.settlement.service.WithdrawBankService;
import com.choosefine.paycenter.settlement.vo.WithDrawVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 15:55
 **/
@Component("CCB_WITHDRAW_HANDLER")
public class CCBWithdrawHandler extends AbstractWithdrawHandler {
    @Autowired
    @Qualifier("ccbWithdrawService")
    private WithdrawBankService ccbWithdrawBankService;

    @PostConstruct
    public void init() {
       this.withdrawBankService=ccbWithdrawBankService;
    }
}
