package com.choosefine.paycenter.channel.ccb.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：建行B2C支付回调通知
 * Author：Jay Chang
 * Create Date：2017/4/1
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBB2CNotifyParams {
    /**商户柜台代码(从商户传送的信息中获得)*/
    private String POSID = "";
    /**分行代码(从商户传送的信息中获得)*/
    private String BRANCHID  = "";
    /**订单号(从商户传送的信息中获得)*/
    private String ORDERID = "";
    /**付款金额(从商户传送的信息中获得)*/
    private String PAYMENT = "";
    /**币种(从商户传送的信息中获得)*/
    private String CURCODE = "";
    /**备注一(从商户传送的信息中获得)*/
    private String REMARK1 = "";
    /**备注二(从商户传送的信息中获得)*/
    private String REMARK2 = "";
    /**账户类型(仅服务器通知中有此字段返回且参与验签，页面通知无此字段返回且不参与验签。)*/
    private String ACC_TYPE = "";
    /**成功标识(成功－Y，失败－N)*/
    private String SUCCESS = "";
    /**接口类型(1- 防钓鱼接口)*/
    private String TYPE = "";
    /**Referer信息(跳转到网银之前的系统 Referer，因为长度不可控，目前截取前100位)*/
    private String REFERER = "";
    /**客户端IP(客户在网银系统中的IP)*/
    private String CLIENTIP = "";
    /**系统记账日期(商户主管在商户后台设置返回记账日期时返回该字且参与验签，字段格式为YYYYMMDD（如20100907），未设置时无此字段返回且不参与验签)*/
    private String ACCDATE = "";
    /**支付账户信息*/
    private String USRMSG = "";
    /**分期期数(从商户传送的信息中获得，当分期期数为空或无此字段上送时，无此字段返回且不参与验签，否则有此字段返回且参与验签)*/
    private String INSTALLNUM = "";
    /**错误信息(该值默认返回为空，商户无需处理，仅需参与验签即可。当有分期期数返回时，则有ERRMSG字段返回且参与验签，否则无此字段返回且不参与验签)*/
    private String ERRMSG = "";
    /**客户加密信息(业务人员在ECTIP后台设置客户信息加密返回的开关且该字段参与验签，否则无此字段返回且不参与验签，格式如下：“证件号密文|手机号密文”。该字段不可解密。)*/
    private String USRINFO = "";
    /**数字签名(签名字段)*/
    private String SIGN = "";
}
