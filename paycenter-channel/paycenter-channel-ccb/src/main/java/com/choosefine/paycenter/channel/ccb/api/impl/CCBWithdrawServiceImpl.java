package com.choosefine.paycenter.channel.ccb.api.impl;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.CCB6W8010TxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.CCB6W8020TxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W8010TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W8020TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.CCB6W8010TxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.CCB6W8020TxRes;
import com.choosefine.paycenter.channel.ccb.utils.CCBWlptUtils;
import com.choosefine.paycenter.settlement.service.WithdrawBankService;
import com.choosefine.paycenter.settlement.vo.WithDrawVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 16:01
 **/
@Service("ccbWithdrawService")
public class CCBWithdrawServiceImpl implements WithdrawBankService {
    private final double bigAmount = 50000.00;
    @Autowired
    private RedisTemplate redisTemplate;

    @Value("${bank.ccb.wlptUrl}")
    private String ccbWlptUrl;

    @Autowired
    private CCBPayConfigStorage ccbPayConfigStorage;

    private final static String CCB_BANK_CODE = "105";

    private final static String CCB_BANK_NAME = "中国建设银行";

    @Override
    public String sendWithdrawRequest(WithDrawVo withDrawVo) {
        String returnCode = null;
        String returnMsg = null;
        try {
            CCB6W8020TxReq ccb6W8020TxReq = new CCB6W8020TxReq();
            ccb6W8020TxReq.setCUST_ID(ccbPayConfigStorage.getWlpt().getString("CUST_ID"));
            ccb6W8020TxReq.setUSER_ID(ccbPayConfigStorage.getWlpt().getString("USER_ID"));
            ccb6W8020TxReq.setPASSWORD(ccbPayConfigStorage.getWlpt().getString("PASSWORD"));

            CCB6W8010TxReq ccb6W8010TxReq = new CCB6W8010TxReq();
            ccb6W8010TxReq.setCUST_ID(ccbPayConfigStorage.getWlpt().getString("CUST_ID"));
            ccb6W8010TxReq.setUSER_ID(ccbPayConfigStorage.getWlpt().getString("USER_ID"));
            ccb6W8010TxReq.setPASSWORD(ccbPayConfigStorage.getWlpt().getString("PASSWORD"));

            //TODO 这里的bankCode就是指行号(需与channelCode区分)
            if (!withDrawVo.getWithdrawBankCode().equals(CCB_BANK_CODE)) {
                CCB6W8020TxReqBody ccb6W8020TxReqBody = new CCB6W8020TxReqBody();
                ccb6W8020TxReqBody.setAMOUNT(withDrawVo.getAmount());
                ccb6W8020TxReqBody.setCST_PAY_NO(withDrawVo.getWithDrawSn());
                ccb6W8020TxReqBody.setPAY_ACCNO(ccbPayConfigStorage.getWlpt().getString("PAY_ACCNO"));
                ccb6W8020TxReqBody.setRECV_ACC_NAME(withDrawVo.getWithDrawRealName());
                ccb6W8020TxReqBody.setRECV_ACCNO(withDrawVo.getWithDrawBankCardNum());
                ccb6W8020TxReqBody.setRECV_OPENACC_DEPT(withDrawVo.getBankTypeName());
                ccb6W8020TxReqBody.setRECV_UBANKNO(withDrawVo.getBankUnion());
                ccb6W8020TxReq.setTxBody(ccb6W8020TxReqBody);
                ccb6W8020TxReq.setREQUEST_SN(withDrawVo.getRequestSn());
                CCB6W8020TxRes ccb6W8020TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlptUrl,ccb6W8020TxReq, CCB6W8020TxRes.class, ccbPayConfigStorage.getCa());
                returnCode = ccb6W8020TxRes.getRETURN_CODE();
                returnMsg = ccb6W8020TxRes.getRETURN_MSG();
            } else {
                CCB6W8010TxReqBody ccb6W8010TxReqBody = new CCB6W8010TxReqBody();
                ccb6W8010TxReqBody.setAMOUNT(withDrawVo.getAmount());
                ccb6W8010TxReqBody.setCHK_RECVNAME("1");
                ccb6W8010TxReqBody.setCST_PAY_NO(withDrawVo.getWithDrawSn());
                ccb6W8010TxReqBody.setPAY_ACCNO(ccbPayConfigStorage.getWlpt().getString("PAY_ACCNO"));
                ccb6W8010TxReqBody.setRECV_ACC_NAME(withDrawVo.getWithDrawRealName());
                ccb6W8010TxReqBody.setRECV_ACCNO(withDrawVo.getWithDrawBankCardNum());
                String openAcctDept = StringUtils.isNotBlank(withDrawVo.getBankTypeName())?withDrawVo.getBankTypeName():CCB_BANK_NAME;
                ccb6W8010TxReqBody.setRECV_OPENACC_DEPT(openAcctDept);
                ccb6W8010TxReq.setTxBody(ccb6W8010TxReqBody);
                ccb6W8010TxReq.setREQUEST_SN(withDrawVo.getRequestSn());
//                CCB6W8010TxRes ccb6W8010TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlptUrl,ccb6W8010TxReq, CCB6W8010TxRes.class);
                CCB6W8010TxRes ccb6W8010TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlptUrl, ccb6W8010TxReq, CCB6W8010TxRes.class, ccbPayConfigStorage.getCa());
                returnCode = ccb6W8010TxRes.getRETURN_CODE();
                returnMsg = ccb6W8010TxRes.getRETURN_MSG();
            }
            if (returnCode.equals("000000")) {
                return "success";//成功返回success
            }
            return returnMsg;//失败返回银行给出的错误信息
        } catch (Exception e) {
            // TODO: 2017/5/5 更新该笔提现状态为失败或者删除这比提现记录
            e.printStackTrace();
        }
        return null;
    }
}
