package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W8010TxResBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W8020TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Comments：NO.6 （6W8020）跨行单笔转账（大小额） Response
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@XStreamAlias("TX")
public class CCB6W8020TxRes extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB6W8020TxResBody txBody;
}

