package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W0201TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.1 （6W0201）超级网银联行号查询 Response
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W0201TxRes extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB6W0201TxResBody txBody;
}

