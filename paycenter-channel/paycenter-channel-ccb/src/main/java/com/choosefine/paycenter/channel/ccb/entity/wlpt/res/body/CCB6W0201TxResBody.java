package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Comments：NO.1 （6W0201）超级网银联行号查询  Response Body
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB6W0201TxResBody{
    @XStreamImplicit(itemFieldName="list")
    private List<CCBBankItem> bankItemList;
}
