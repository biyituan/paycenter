package com.choosefine.paycenter.channel.ccb.entity.CCBConfig;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dyy on 2017/6/26.
 *
 *
 * <p>Add CA certificate configuration.</p>
 * @Author Ye, Wenda
 * @Date 20170712
 */
@Component
public class CCBConfigParams {
    /**建行B2C网银支付配置key*/
    @Value("${CCB.B2C_NB.POSID}")
    private  String B2C_NB_POSID;
    @Value("${CCB.B2C_NB.PROINFO}")
    private  String B2C_NB_PROINFO;
    @Value("${CCB.B2C_NB.PUB}")
    private String B2C_NB_PUB;
    @Value("${CCB.B2C_NB.TXCODE}")
    private String B2C_NB_TXCODE;
    @Value("${CCB.B2C_NB.TYPE}")
    private String B2C_NB_TYPE;
    @Value("${CCB.B2C_NB.PUBLIC_KEY}")
    private String B2C_NB_PUBLIC_KEY;

    /**建行外联平台配置key*/
    @Value("${CCB.WLPT.CUST_ID}")
    private String WLPT_CUST_ID;
    @Value("${CCB.WLPT.PAY_ACCNO}")
    private String WLPT_PAY_ACCNO;
    @Value("${CCB.WLPT.USER_ID}")
    private String WLPT_USER_ID;
    @Value("${CCB.WLPT.PASSWORD}")
    private String WLPT_PASSWORD;

    /**建行外联平台2配置key(主要用于支付流水查询，对账单文件下载，与上述提现用的外联客户端，不是同一个)*/
    @Value("${CCB.WLPT2.CUST_ID}")
    private String WLPT2_CUST_ID;
    @Value("${CCB.WLPT2.USER_ID}")
    private String WLPT2_USER_ID;
    @Value("${CCB.WLPT2.PASSWORD}")
    private String WLPT2_PASSWORD;


    /**建行B2C APP支付配置key*/
    @Value("${CCB.B2C_APP.POSID}")
    private  String B2C_APP_POSID;
    @Value("${CCB.B2C_APP.PROINFO}")
    private String B2C_APP_PROINFO;
    @Value("${CCB.B2C_APP.PUB}")
    private String B2C_APP_PUB;
    @Value("${CCB.B2C_APP.TXCODE}")
    private String B2C_APP_TXCODE;
    @Value("${CCB.B2C_APP.TYPE}")
    private String B2C_APP_TYPE;
    @Value("${CCB.B2C_APP.PUBLIC_KEY}")
    private String B2C_APP_PUBLIC_KEY;
    @Value("${CCB.B2C_APP.THIRDAPPINFO}")
    private String B2C_APP_THIRDAPPINFO;


    /**建行公共配置key*/
    @Value("${CCB.COMMON.MERCHANT_ID}")
    private String COMMON_MERCHANT_ID;
    @Value("${CCB.COMMON.BRANCHID}")
    private String COMMON_BRANCHID;
    @Value("${CCB.COMMON.CURCODE}")
    private String COMMON_CURCODE;

    /**建行B2B网银支付配置key*/
    @Value("${CCB.B2B_NB.POSID}")
    private String B2B_NB_POSID;
    @Value("${CCB.B2B_NB.TXCODE}")
    private String B2B_NB_TXCODE;
    @Value("${CCB.B2B_NB.PUBLIC_KEY}")
    private String B2B_NB_PUBLIC_KEY;

    /** HTTPS双向认证，CA证书相关配置 */
    @Value("${ca.keyFileClient}")
    private String KEY_FILE_CLIENT;
    @Value("${ca.keyFileTrust}")
    private String KEY_FILE_TRUST;
    @Value("${ca.keyPwdClient}")
    private String KEY_PWD_CLIENT;
    @Value("${ca.keyPwdTrust}")
    private String KEY_PWD_TRUST;

    /**建行B2C网银支付配置key*/
    public JSONObject getB2C_NB(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("POSID",B2C_NB_POSID);
        map.put("PROINFO",B2C_NB_PROINFO);
        map.put("PUB",B2C_NB_PUB);
        map.put("TXCODE",B2C_NB_TXCODE);
        map.put("TYPE",B2C_NB_TYPE);
        map.put("PUBLIC_KEY",B2C_NB_PUBLIC_KEY);
        return new JSONObject(map);
    }

    /**建行外联平台配置key*/
    public JSONObject getWLPT(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("CUST_ID",WLPT_CUST_ID);
        map.put("PAY_ACCNO",WLPT_PAY_ACCNO);
        map.put("USER_ID",WLPT_USER_ID);
        map.put("PASSWORD",WLPT_PASSWORD);
        return new JSONObject(map);
    }

    /**建行B2C APP支付配置key*/
    public JSONObject getB2C_APP(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("POSID",B2C_APP_POSID);
        map.put("PROINFO",B2C_APP_PROINFO);
        map.put("PUB",B2C_APP_PUB);
        map.put("TXCODE",B2C_APP_TXCODE);
        map.put("TYPE",B2C_APP_TYPE);
        map.put("PUBLIC_KEY",B2C_APP_PUBLIC_KEY);
        map.put("THIRDAPPINFO",B2C_APP_THIRDAPPINFO);
        return new JSONObject(map);
    }

    /**建行公共配置key*/
    public JSONObject getCOMMON(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("MERCHANT_ID",COMMON_MERCHANT_ID);
        map.put("BRANCHID",COMMON_BRANCHID);
        map.put("CURCODE",COMMON_CURCODE);
        return new JSONObject(map);
    }

    /**建行B2B网银支付配置key*/
    public JSONObject getB2B_NB(){
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("POSID",B2B_NB_POSID);
        map.put("TXCODE",B2B_NB_TXCODE);
        map.put("PUBLIC_KEY",B2B_NB_PUBLIC_KEY);
        return new JSONObject(map);
    }

    /**建行外联客户端（与提现所用的非同一个客户端，这个外联客户端主要用于支付流水查询，及对账单文件下载）*/
    public JSONObject getWLPT2() {
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("CUST_ID",WLPT2_CUST_ID);
        map.put("USER_ID",WLPT2_USER_ID);
        map.put("PASSWORD",WLPT2_PASSWORD);
        return new JSONObject(map);
    }

    /** HTTPS双向认证，CA证书相关配置 */
    public JSONObject getCa() {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("KEY_FILE_CLIENT", KEY_FILE_CLIENT);
        map.put("KEY_FILE_TRUST", KEY_FILE_TRUST);;
        map.put("KEY_PWD_CLIENT", KEY_PWD_CLIENT);
        map.put("KEY_PWD_TRUST", KEY_PWD_TRUST);
        return new JSONObject(map);
    }

}
