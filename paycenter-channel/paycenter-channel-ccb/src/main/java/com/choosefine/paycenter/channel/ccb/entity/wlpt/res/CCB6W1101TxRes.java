package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W1101TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.2 （6W1101）账号、户名是否匹配校验交易
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@ToString
@XStreamAlias("TX")
@Getter
@Setter
public class CCB6W1101TxRes extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB6W1101TxResBody txBody;
}
