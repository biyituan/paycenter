package com.choosefine.paycenter.channel.ccb.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBB2BNotifyParams {
    /**商户柜台代码*/
    private String MPOSID;
    /**订单号*/
    private String ORDER_NUMBER;
    /**付款客户号*/
    private String CUST_ID;
    /**付款账号*/
    private String ACC_NO;
    /**付款账户名称*/
    private String ACC_NAME;
    /**付款金额*/
    private String AMOUNT;
    /**支付结果*/
    private String STATUS;
    /**备注一*/
    private String REMARK1;
    /**备注二*/
    private String REMARK2;
    /**付款方式*/
    private String TRAN_FLAG;
    /**交易时间*/
    private String TRAN_TIME;
    /**分行名称*/
    private String BRANCH_NAME;
    /**数字签名加密串*/
    private String SIGNSTRING;
    /**最后一级复核员是否审核通过*/
    private String CHECKOK;
}
