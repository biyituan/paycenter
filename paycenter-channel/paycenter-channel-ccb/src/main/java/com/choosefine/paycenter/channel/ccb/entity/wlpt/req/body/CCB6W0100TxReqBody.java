package com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：NO.2 （6W0100）余额查询交易 Body
 * Author：Jay Chang
 * Create Date：2017/4/20
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB6W0100TxReqBody {
    /**  7 ACC_NO 账号 varChar(32) F 查询账号*/
    private String ACC_NO = "";
}
