package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.reconciliation.handler.AbstractReconciliationHandler;
import com.choosefine.paycenter.reconciliation.service.ReconciliationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
@Component("CCB_RECONCILIATION_HANDLER")
public class CCBReconciliationHandler extends AbstractReconciliationHandler {
    @Autowired
    @Qualifier("ccbReconciliationService")
    private ReconciliationService ccbReconciliationService;

    @PostConstruct
    public void init() {
        this.reconciliationService = ccbReconciliationService;
    }
}
