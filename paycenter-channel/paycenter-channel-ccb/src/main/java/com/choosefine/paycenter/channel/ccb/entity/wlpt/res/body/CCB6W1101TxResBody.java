package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB6W1101TxResBody{
    /** 7 IS_RIGHT 账号户名是否匹配标志 Char(1) F 0-匹配 1-不匹配*/
    private String IS_RIGHT;
}
