package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB5W1002TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * Comments：（5W1002）商户支付流水查询 Response
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@XStreamAlias("TX")
public class CCB5W1002TxRes extends CCBTxRes {
    @XStreamAlias("TX_INFO")
    private CCB5W1002TxResBody txBody;
}

