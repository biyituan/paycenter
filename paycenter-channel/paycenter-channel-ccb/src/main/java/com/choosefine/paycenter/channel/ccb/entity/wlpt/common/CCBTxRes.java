package com.choosefine.paycenter.channel.ccb.entity.wlpt.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：建行外联平台交易
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBTxRes {
    /** 1 REQUEST_SN 请求序列号 varChar(16) F 同请求报文中的序列号*/
    private String REQUEST_SN;
    /** 2 CUST_ID 客户号 varChar(21) F 同请求报文中的客户号*/
    private String CUST_ID;
    /** 3 TX_CODE 交易码 Char(6) F 同请求报文中的交易码*/
    private String TX_CODE;
    /** 4 RETURN_CODE 响应码 varChar(12) F 交易响应码*/
    private String RETURN_CODE;
    /** 5 RETURN_MSG 响应信息 varChar(600) T 交易响应信息*/
    private String RETURN_MSG;
    /** 6 LANGUAGE 语言 Char(2) F CN，同请求报文*/
    private String LANGUAGE;
}
