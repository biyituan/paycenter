package com.choosefine.paycenter.channel.ccb.entity.wlpt.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：建行外联平台交易
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBTxReq {
    //TODO 将这些配置要么放配置文件，要么放mogndo或redis
    /** 1 REQUEST_SN 请求序列号 varChar(16) F 只可以使用数字*/
    private String REQUEST_SN;
    /** 2 CUST_ID 客户号 varChar(21) F 字符型char，网银客户号*/
    private String CUST_ID = "";
    /** 3 USER_ID 操作员号 varChar(6) F 20051210后必须使用*/
    private String USER_ID = "";
    /** 4 PASSWORD 密码 varChar(32) F 操作员密码*/
    private String PASSWORD = "";
    /** 5 TX_CODE 交易码 varChar(6) F 交易请求码*/
    private String TX_CODE="";
    /** 6 LANGUAGE 语言 varChar(2) F CN*/
    private String LANGUAGE = "CN";
}
