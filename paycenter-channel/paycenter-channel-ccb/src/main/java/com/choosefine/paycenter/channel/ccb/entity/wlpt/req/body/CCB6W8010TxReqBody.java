package com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/15
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB6W8010TxReqBody{
    /** 7 PAY_ACCNO 转出账户号 varChar(32) F 转出账户号为授权账户，转入账户号为签约账户时，为主动收款功能*/
    private String PAY_ACCNO = "";
    /** 8 RECV_ACCNO 转入账户号 varChar(32) F 　*/
    private String RECV_ACCNO = "";
    /** 9 RECV_ACC_NAME 转入账户名称 varChar(100) F 　*/
    private String RECV_ACC_NAME = "";
    /** 10 CHK_RECVNAME 收款账户户名校验 varChar(1) T 行内转账收款账户户名校验 1:校验 0：不校验*/
    private String CHK_RECVNAME = "";
    /** 11 RECV_OPENACC_DEPT 转入账户开户机构名称 varChar(100) F 　*/
    private String RECV_OPENACC_DEPT = "";
    /** 12 AMOUNT 金额 Decimal(16,2) F 　*/
    private String AMOUNT = "";
    /** 13 CUR_TYPE 币种 Char(2) F 01:人民币　该接口仅支持人民币转账*/
    private String CUR_TYPE="01";
    /** 14 USEOF 用途 varChar(100) F 　*/
    private String USEOF="提现";
    /** 15 CST_PAY_NO 客户方流水号 varChar(60) T 　*/
    private String CST_PAY_NO = "";
    /** 16 REM1 备注1 varChar(32) T 备注1*/
    private String REM1 = "";
    /** 17 REM2 备注2 varChar(32) T 备注2*/
    private String REM2 = "";
}
