package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

/**
 * Comments：NO.2 （6W0100）余额查询交易 Response Body
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class CCB6W0100TxResBody {
    /**7 ACC_NO 账号 varChar(32) F 同请求报文中的账号*/
    private String ACC_NO;
    /**8 BALANCE 余额 Decimal(16,2) T 　*/
    private String BALANCE;
    /**9 BALANCE1 可用余额 Decimal(16,2) T 　*/
    private String BALANCE1;
    /**10 INTEREST 账户利息 Decimal(16,2) T 　*/
    private String INTEREST;
    /**11 INTEREST_RATE 账户利率 Decimal(6,4) T 　*/
    private String INTEREST_RATE;
    /**12 ACC_STATUS 账户状态 Char(20) F 　*/
    private String ACC_STATUS;
    /**13 RESV_NAME1 自定义名称1 varChar(99) T 分行自定义输出名称1*/
    private String RESV_NAME1;
    /**14 RESV1 自定义内容1 varChar(99) T 分行自定义输出内容1*/
    private String RESV1;
    /**15 RESV_NAME2 自定义名称2 varChar(99) T 分行自定义输出名称2*/
    private String RESV_NAME2;
    /**16 RESV2 自定义内容2 varChar(99) T 分行自定义输出内容2*/
    private String RESV2;
    /**17 REM1 备注1 varChar(32) T 　*/
    private String REM1;
    /**18 REM2 备注2 varChar(32) T 　*/
    private String REM2;


}
