package com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * Comments：（5W1002）商户支付流水查询  Response Body
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCB5W1002TxResBody {
    /** 8	CUR_PAGE	当前页次	Int	T*/
    private String CUR_PAGE = "";
    /** 9	PAGE_COUNT	总页次	Int	T*/
    private String PAGE_COUNT = "";
    /** 10	NOTICE	提示	varChar(200)	T	提示信息*/
    private String NOTICE = "";
    /** List（多条信息）*/
    @XStreamImplicit(itemFieldName="LIST")
    private List<CCBTradeItem> LIST;
}
