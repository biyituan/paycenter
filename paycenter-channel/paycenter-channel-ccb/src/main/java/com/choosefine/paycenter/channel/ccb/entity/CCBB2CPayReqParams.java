package com.choosefine.paycenter.channel.ccb.entity;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 * 建行B2C网银支付请求参数
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/31
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class CCBB2CPayReqParams{
    /**商户代码  【必须】*/
    private String MERCHANTID = "";
    /**柜台代码 【必须】*/
    private String POSID = "";
    /**分行代码 【必须】*/
    private String BRANCHID = "";
    /**商户订单号 【必须】*/
    private String ORDERID = "";
    /**付款金额 【必须】*/
    private String PAYMENT = "";
    /**币种(01人民币，目前只支持人民币支付) 【必须】*/
    private String CURCODE = "";
    /**交易码 【必须】*/
    private String TXCODE = "";
    /**备注1 【非必须】*/
    private String REMARK1 = "";
    /**备注2 【非必须】*/
    private String REMARK2 = "";
    /**0- 非钓鱼接口,1- 防钓鱼接口 【必须】*/
    private String TYPE = "1";
    /**公钥后30位 【必须】*/
    private String PUB = "";
    /**网关类型 【非必须】*/
    private String GATEWAY = "";
    /**客户端IP（客户在商户系统中的IP）【非必须】*/
    private String CLIENTIP = "";
    /**客户注册信息（客户在商户系统中注册的信息，中文需使用escape编码）【非必需】*/
    private String REGINFO = "";
    /**商品信息 【非必需】*/
    private String PROINFO = "";
    /**商户URL 商户送空值即可，银行从后台读取商户设置的一级域名，如www.ccb.com则设为： “ccb.com”，最多允许设置三个不同的域名，格式为：****.com| ****.com.cn|****.net） 【非必需】*/
    private String REFERER = "";
    /**
     分期期数 信用卡支付分期期数，
     一般为3、6、12等，必须为大于1的整数，当分期期数为空或无该字段上送时，则视为普通的网上支付。
     当分期期数为空或无该字段上送时，该字段不参与MAC校验，否则参与MAC校验 【非必需】
     */
    private String INSTALLNUM;
    /**订单超时时间 格式：yyyyMMddHHmmss 如：20120214143005
     银行系统时间> TIMEOUT时拒绝交易，若送空值则不判断超时 【非必需】
     */
    private String TIMEOUT;
    /**
     商户客户端的intent-filter/schema，格式如下：
     comccbpay+商户代码(即MERCHANTID字段值)+商户自定义的标示app的字符串
     商户自定义的标示app的字符串，只能为字母或数字。
     示例：
     comccbpay105320148140002alipay
     当该字段有值时参与MAC校验，否则不参与MAC校验 【非必需】*/
    private String THIRDAPPINFO;
}
