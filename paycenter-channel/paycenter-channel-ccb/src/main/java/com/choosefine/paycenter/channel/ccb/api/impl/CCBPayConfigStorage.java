package com.choosefine.paycenter.channel.ccb.api.impl;

import com.alibaba.fastjson.JSONObject;
import com.choosefine.paycenter.channel.ccb.entity.CCBConfig.CCBConfigParams;
import com.choosefine.paycenter.common.enums.PayType;
import com.choosefine.paycenter.pay.api.AbstractPayConfigStorage;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 *
 * <p>Add CA certificate configuration.</p>
 * @Author Ye, Wenda
 * @Date 20170712
 */
@Getter
@Component
public class CCBPayConfigStorage extends AbstractPayConfigStorage {

    @Autowired
    CCBConfigParams ccbConfigParams;

    /**公共配置*/
    private JSONObject common;
    /**b2c网银支付配置*/
    private JSONObject b2cNB;
    /**b2b网银支付配置*/
    private JSONObject b2bNB;
    /**b2c app支付配置*/
    private JSONObject b2cApp;
    /**外联平台配置*/
    private JSONObject wlpt;
    /**外联平台2配置(主要用于支付流水查询，因为建行的支付流水查询，需要用到另外一个外联客户端)*/
    private JSONObject wlpt2;
    /** CA certificate configuration.*/
    private JSONObject ca;


    @PostConstruct
    public void init(){
        common = ccbConfigParams.getCOMMON();
        //初始化商户id
        setMerchantId(common.getString(MERCHANT_ID));
        b2cNB = ccbConfigParams.getB2C_NB();
        b2cApp = ccbConfigParams.getB2C_APP();
        b2bNB = ccbConfigParams.getB2B_NB();
        wlpt = ccbConfigParams.getWLPT();
        wlpt2 = ccbConfigParams.getWLPT2();
        ca = ccbConfigParams.getCa();
        setPayType("CCB");
        setSignType("MD5");
    }

    /**
     * 如果存在不同的交易方式需要不同的公钥，那么使用此方法
     * @param payType
     * @return
     */
    public String getPublicKey(PayType payType) {
        if(CCBPayType.B2C_NB.getType().equals(payType.getType())){
            return b2cNB.getString(PUBLIC_KEY);
        }else if(CCBPayType.B2C_APP.getType().equals(payType.getType())){
            return b2cApp.getString(PUBLIC_KEY);
        }else{
            return b2bNB.getString(PUBLIC_KEY);
        }
    }

    /**
     * 此方法不支持
     * @return
     */
    public String getPublicKey() {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getValue(PayType payType,String key) {
        if(CCBPayType.B2C_NB.getType().equals(payType.getType())){
            return b2cNB.getString(key);
        }else if(CCBPayType.B2C_APP.getType().equals(payType.getType())){
            return b2cApp.getString(key);
        }else{
            return b2bNB.getString(key);
        }
    }
}
