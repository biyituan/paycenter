package com.choosefine.paycenter.channel.ccb.api.handler;

import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractPayAgencyPayHandler;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.dto.PayOrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import java.lang.reflect.InvocationTargetException;
/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Component("CCB_PAY_HANDLER")
public class CCBPayHandler extends AbstractPayAgencyPayHandler {
    @Autowired
    @Qualifier("ccbPayService")
    private PayService ccbPayService;

    @Override
    protected String getGotoUrl(PayBizzRequestDto payBizzRequest) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        PayOrderDto payOrder = new PayOrderDto();
        payOrder.setOrderId(payBizzRequest.getPaySn());
        payOrder.setAmount(payBizzRequest.getAmount());
        return ccbPayService.buildRequest(payOrder,payBizzRequest.getPayType());
    }

    @Override
    public PayService getPayService(){
        return this.ccbPayService;
    }
}