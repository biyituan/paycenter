package com.choosefine.paycenter.channel.ccb.entity.wlpt.res;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB6W0600TxResBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:45
 **/
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W0600TxRes extends CCBTxRes{
    @XStreamAlias("TX_INFO")
    private CCB6W0600TxResBody ccb6W0600TxResBody;
}
