package com.choosefine.paycenter.channel.ccb.api.impl;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.CCB5W1002TxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB5W1002TxReqBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.CCB5W1002TxRes;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCB5W1002TxResBody;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.res.body.CCBTradeItem;
import com.choosefine.paycenter.channel.ccb.utils.CCBWlptUtils;
import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;
import com.choosefine.paycenter.reconciliation.service.ReconciliationService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
@Slf4j
@Service("ccbReconciliationService")
public class CCBReconciliationServiceImpl implements ReconciliationService {

    public static final String CHANNEL_CODE = "CCB";

    @Value("${bank.ccb.wlpt2Url}")
    private String ccbWlpt2Url;

    @Autowired
    private CCBPayConfigStorage ccbPayConfigStorage;

    /**
     * 查询工作日进账流水
     * @param day
     * @return
     */
    @Override
    public List<ChannelIncomeBill> getDayBills(Date day) {
        List<ChannelIncomeBill> channelIncomeBills = new ArrayList<>();
        // 拼接查询参数
        CCB5W1002TxReq ccb5W1002TxReq = this.generateRequest(day);
        // 从CCB获取数据
        CCB5W1002TxRes ccb5W1002TxRes = this.getOriginBillDataFromCCB(ccb5W1002TxReq);
        this.convertAndAdd(ccb5W1002TxRes, channelIncomeBills);
        if (!"000000".equals(ccb5W1002TxRes.getRETURN_CODE())) {
            log.warn("查询CCB进账流水失败：" + ccb5W1002TxRes.getRETURN_MSG());
        }
        // 建行接口没有pageSize，所以多页结果需要额外次数的查询
        CCB5W1002TxResBody CCBTxBody = ccb5W1002TxRes.getTxBody();
        if (CCBTxBody != null) {
            int pageCount = Integer.parseInt(CCBTxBody.getPAGE_COUNT());
            if (pageCount > 1) {
                for (int page = 2; page <= pageCount; page++) {
                    ccb5W1002TxReq.getTxBody().setPAGE(page + "");
                    ccb5W1002TxRes = this.getOriginBillDataFromCCB(ccb5W1002TxReq);
                    this.convertAndAdd(ccb5W1002TxRes, channelIncomeBills);
                }
            }
        }
        return channelIncomeBills;
    }

    /**
     * 调用建行接口查询日进账流水
     * @param ccb5W1002TxReq
     * @return
     */
    private CCB5W1002TxRes getOriginBillDataFromCCB(CCB5W1002TxReq ccb5W1002TxReq) {
        CCB5W1002TxRes ccb5W1002TxRes;
        try {
            ccb5W1002TxRes = CCBWlptUtils.getInstance().postForCCBWlpt(ccbWlpt2Url, ccb5W1002TxReq, CCB5W1002TxRes.class);
        } catch (Exception e){
            log.error("建行查询支付流水失败！", e);
            throw new RuntimeException("建行查询支付流水失败！", e);
        }
        // 000000表示响应成功，0250E0200001表示流水记录不存在
//        String returnCode = ccb5W1002TxRes.getRETURN_CODE();
        return ccb5W1002TxRes;
    }

    /**
     * 拼接查询request
     * @param day
     * @return
     */
    private CCB5W1002TxReq generateRequest(Date day) {
        CCB5W1002TxReq ccb5W1002TxReq = new CCB5W1002TxReq();
        ccb5W1002TxReq.setREQUEST_SN(System.currentTimeMillis() + "");
        ccb5W1002TxReq.setUSER_ID(ccbPayConfigStorage.getWlpt2().getString("USER_ID"));
        ccb5W1002TxReq.setCUST_ID(ccbPayConfigStorage.getWlpt2().getString("CUST_ID"));
        ccb5W1002TxReq.setPASSWORD(ccbPayConfigStorage.getWlpt2().getString("PASSWORD"));
        CCB5W1002TxReqBody ccb5W1002TxReqBody = new CCB5W1002TxReqBody();
        // 流水类型  0:未结流水,1:已结流水
        ccb5W1002TxReqBody.setKIND("1");
        // 排序 1:交易日期,2:订单号
        ccb5W1002TxReqBody.setNORDERBY("2");
        // 页码 从1开始计数
        ccb5W1002TxReqBody.setPAGE("1");
        // 流水状态 0:交易失败,1:交易成功,2:待银行确认(针对未结流水查询);3:全部
        ccb5W1002TxReqBody.setSTATUS("3");
        // 时间 - 为查询日的00:00 - 23:59
        DateTime requireDay = new DateTime(day);
        DateTime startTime = new DateTime(requireDay.getYear(), requireDay.getMonthOfYear(), requireDay.getDayOfMonth(), 0, 0, 0);
        String start = DateFormatUtils.format(startTime.toDate(), "yyyyMMddHHmmss");
        DateTime endTime = new DateTime(requireDay.getYear(), requireDay.getMonthOfYear(), requireDay.getDayOfMonth(), 23, 59, 59);
        String end = DateFormatUtils.format(endTime.toDate(), "yyyyMMddHHmmss");
        ccb5W1002TxReqBody.setSTART(start.substring(0, 8));
        ccb5W1002TxReqBody.setSTARTHOUR(start.substring(8, 10));
        ccb5W1002TxReqBody.setSTARTMIN(start.substring(10, 12));
        ccb5W1002TxReqBody.setEND(end.substring(0, 8));
        ccb5W1002TxReqBody.setENDHOUR(end.substring(8, 10));
        ccb5W1002TxReqBody.setENDMIN(end.substring(10, 12));
        ccb5W1002TxReq.setTxBody(ccb5W1002TxReqBody);
        return ccb5W1002TxReq;
    }

    /**
     * Convert response items and add them into result list.
     * @param CCBResponse
     * @param channelIncomeBills
     */
    public void convertAndAdd(CCB5W1002TxRes CCBResponse, List<ChannelIncomeBill> channelIncomeBills) {
        if (CCBResponse.getTxBody() != null) {
            CCB5W1002TxResBody CCBTxBody = CCBResponse.getTxBody();
            List<CCBTradeItem> items = CCBTxBody.getLIST();
            if (!CollectionUtils.isEmpty(items)) {
                for (CCBTradeItem item : items) {
                    ChannelIncomeBill channelIncomeBill = this.convert(item);
                    channelIncomeBills.add(channelIncomeBill);
                }
            }
        }
    }

    /**
     * Convert CCB item to Channel Income Bill.
     * @param item
     * @return
     */
    public ChannelIncomeBill convert(CCBTradeItem item) {
        ChannelIncomeBill bill = new ChannelIncomeBill();
        bill.setAccount(item.getACCOUNT());
        bill.setChannelCode(CHANNEL_CODE);
        bill.setComment(item.getREFUND_MONEY() + ChannelIncomeBill.COMMENT_SEPARATOR + item.getREM1()
                + ChannelIncomeBill.COMMENT_SEPARATOR + item.getREM2());
        bill.setPaySn(item.getORDER());
        if (item.getORDER_STATUS() != null) {
            bill.setStatus(Integer.parseInt(item.getORDER_STATUS()));
        }
        if (item.getPAYMENT_MONEY() != null) {
            bill.setAmount(new BigDecimal(Double.parseDouble(item.getPAYMENT_MONEY())));
        }
        if (item.getACC_DATE() != null) {
            try {
                bill.setAccountAt(DateUtils.parseDate(item.getACC_DATE(), "yyyy-MM-dd"));
            } catch (ParseException e) {
                log.error("CCB income bill - [" + item.getORDER() + "] account date parse error : [" + item.getACC_DATE() + "]", e);
            }
        }
        if (item.getTRAN_DATE() != null) {
            try {
                bill.setTransactionAt(DateUtils.parseDate(item.getTRAN_DATE(), "yyyy-MM-dd HH:mm:ss"));
            } catch (ParseException e) {
                log.error("CCB income bill - [" + item.getORDER() + "] transaction date parse error : [" + item.getTRAN_DATE() + "]", e);
            }
        }
        return bill;
    }

}
