package com.choosefine.paycenter.channel.ccb.api.impl;

import com.choosefine.paycenter.common.enums.PayType;
/**
 * Comments：建行交易类型枚举
 * Author：Jay Chang
 * Create Date：2017/4/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum CCBPayType implements PayType {
    //建行B2C网银支付
    B2C_NB("建行B2C网银支付"),
    //建行B2B网银支付
    B2B_NB("建行B2B网银支付"),
    //建行B2C手机支付
    B2C_APP("建行B2C手机支付");

    private String desc;

    private CCBPayType(String desc) {
        this.desc = desc;
    }

    public String getType(){
        return this.name();
    }
}
