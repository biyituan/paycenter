package com.choosefine.paycenter.channel.ccb.entity.wlpt.req;

import com.choosefine.paycenter.channel.ccb.entity.wlpt.common.CCBTxReq;
import com.choosefine.paycenter.channel.ccb.entity.wlpt.req.body.CCB6W0600TxReqBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:33
 **/
@Getter
@Setter
@ToString
@XStreamAlias("TX")
public class CCB6W0600TxReq extends CCBTxReq {
    @XStreamAlias("TX_INFO")
    private CCB6W0600TxReqBody ccb6W0600TxReqBody;

    public CCB6W0600TxReq(){
        setTX_CODE("6W0600");
    }
}
