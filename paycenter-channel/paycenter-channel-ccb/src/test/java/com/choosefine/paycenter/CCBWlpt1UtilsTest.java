package com.choosefine.paycenter;

import com.choosefine.paycenter.channel.ccb.utils.CCBWlptUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author Ye_Wenda
 * @Date 7/26/2017
 */
public class CCBWlpt1UtilsTest {
    @Autowired
    private CCBWlptUtils ccbWlptUtils  = CCBWlptUtils.getInstance();

    /** 转账接口 ********************************************************************************************/

    @Test
    public void test6W1901() {
        // 转账单据流水
        String requestXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TX><REQUEST_SN>" + System.currentTimeMillis() + "</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W1901</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACC_NO>33050161772700000354</ACC_NO><START_DATE>20170625</START_DATE><END_DATE>20170628</END_DATE><CREDIT_NO /><CST_PAY_NO1 /><DJTYPE /><PAGE /><REM1 /><REM2 /></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForCCBWlpt("https://apipay.sxiaobao.com", requestXml);
        System.out.println(responseXml);
    }


    /** 查询接口 ********************************************************************************************/

    @Test
    public void postForCCB6W0100() {
        //查询余额
        String requestXml = "<?xml version=\"1.0\" encoding=\"GB2312\" standalone=\"yes\" ?><TX><REQUEST_SN>" + System.currentTimeMillis() + "</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6W0100</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACC_NO>33050161772700000354</ACC_NO></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForCCBWlpt("https://apipay.sxiaobao.com", requestXml);
        System.out.println(responseXml);
    }

    @Test
    public void test6WY101(){
        //NO.1 （6WY101）一点接入活期账户明细查询
        String requestXml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\" ?><TX><REQUEST_SN>1501066815208</REQUEST_SN><CUST_ID>ZJ33000009170851001</CUST_ID><USER_ID>WLPT01</USER_ID><PASSWORD>781220</PASSWORD><TX_CODE>6WY101</TX_CODE><LANGUAGE>CN</LANGUAGE><TX_INFO><ACCNO1>2017051000000034</ACCNO1><STARTDATE>20170513</STARTDATE><ENDDATE>20170513</ENDDATE><BARGAIN_FLAG></BARGAIN_FLAG><CHECK_ACC_NO></CHECK_ACC_NO><CHECK_ACC_NAME></CHECK_ACC_NAME><REMARK></REMARK><LOW_AMT></LOW_AMT><HIGH_AMT></HIGH_AMT><PAGE></PAGE><POSTSTR></POSTSTR><TOTAL_RECORD></TOTAL_RECORD><DET_NO/></TX_INFO></TX>";
        String responseXml = ccbWlptUtils.postForCCBWlpt("https://apipay.sxiaobao.com", requestXml);
        System.out.println(responseXml);
    }
}
