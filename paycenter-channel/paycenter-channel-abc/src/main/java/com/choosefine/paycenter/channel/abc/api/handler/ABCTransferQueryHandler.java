package com.choosefine.paycenter.channel.abc.api.handler;

import com.choosefine.paycenter.pay.handler.AbstractTransferQueryHandler;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 20:29
 **/
@Getter
@Component("ABC_TRANSFER_QUERY_HANDLER")
public class ABCTransferQueryHandler extends AbstractTransferQueryHandler{
    @Autowired
    @Qualifier("abcTransferQueryService")
    private TransferQueryService abcTransferQueryService;

    @PostConstruct
    public void init() {
        this.transferQueryService=abcTransferQueryService;
    }

    @Override
    public TransferQueryService getTransferService() {
        return abcTransferQueryService;
    }
}
