package com.choosefine.paycenter.channel.abc.api.impl;

import com.abc.pay.client.Base64;
import com.abc.pay.client.JSON;
import com.abc.pay.client.ebus.QueryOrderRequest;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import com.choosefine.paycenter.pay.vo.TransferQueryVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-25 11:21
 **/
@Slf4j
@Service("abcTransferQueryService")
public class ABCQueryOderServiceImpl implements TransferQueryService {
    /**
     * 农业银行的订单查询包括支付，退款等等
     *
     * @param payTypeID
     * @param queryTpye
     * @param OrderNo
     * @return
     */
    public Map queryOder(String payTypeID, String queryTpye, String OrderNo) {
        Map<String, Object> resultMap = new HashMap();
        if (queryTpye.equals("0")) {
            queryTpye = "false";
        } else if (queryTpye.equals("1")) {
            queryTpye = "true";
        }
        QueryOrderRequest tQueryRequest = new QueryOrderRequest();
        tQueryRequest.queryRequest.put("PayTypeID", payTypeID);    //设定交易类型
        tQueryRequest.queryRequest.put("OrderNo", OrderNo);    //设定订单编号 （必要信息）
        tQueryRequest.queryRequest.put("QueryDetail", queryTpye);//设定查询方式
        //如果需要专线地址，调用此方法：
        //tQueryRequest.setConnectionFlag(true);
        JSON json = tQueryRequest.postRequest();
        //JSON json = tQueryRequest.extendPostRequest(1);

        String ReturnCode = json.GetKeyValue("ReturnCode");
        String ErrorMessage = json.GetKeyValue("ErrorMessage");

        if (ReturnCode.equals("0000")) {
            log.info("ReturnCode   = [" + ReturnCode + "]");
            log.info("ErrorMessage = [" + ErrorMessage + "]");
            //4、获取结果信息
            String orderInfo = json.GetKeyValue("Order");
            if (orderInfo.length() < 1) {
                log.info("查询结果为空");
            } else {
                //1、还原经过base64编码的信息
                Base64 tBase64 = new Base64();
                String orderDetail = new String(tBase64.decode(orderInfo));
                json.setJsonString(orderDetail);
                log.info("订单明细" + orderDetail);

                if (queryTpye.equals("0")) {
                    resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                    resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                    resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                    resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                    resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                    resultMap.put("Status", json.GetKeyValue("Status"));
                    return resultMap;
                } else {
                    LinkedHashMap hashMap = new LinkedHashMap();
                    if (payTypeID.equals("ImmediatePay") || payTypeID.equals("PreAuthPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("OrderDesc", json.GetKeyValue("OrderDesc"));
                        resultMap.put("OrderURL", json.GetKeyValue("OrderURL"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));
                        //5、商品明细
                        hashMap = json.GetArrayValue("OrderItems");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("DividedPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("InstallmentCode", json.GetKeyValue("InstallmentCode"));
                        resultMap.put("InstallmentNum", json.GetKeyValue("InstallmentNum"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));

                        hashMap = json.GetArrayValue("OrderItems");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        hashMap.clear();
                        hashMap = json.GetArrayValue("Distribution");
                        if (hashMap.size() == 0) {
                            log.info("分账账户信息为空");
                            resultMap.put("Distribution", null);
                        } else {
                            log.info("分账账户信息明细为:");
                            resultMap.put("Distribution", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("Refund")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("RefundAmount", json.GetKeyValue("RefundAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("MerRefundAccountNo", json.GetKeyValue("MerRefundAccountNo"));
                        resultMap.put("MerRefundAccountName", json.GetKeyValue("MerRefundAccountName"));
                        return resultMap;
                    } else if (payTypeID.equals("AgentPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("InstallmentCode", json.GetKeyValue("InstallmentCode"));
                        resultMap.put("InstallmentNum", json.GetKeyValue("InstallmentNum"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));
                        hashMap = json.GetArrayValue("OrderItem");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        //4、获取分账账户信息
                        hashMap.clear();
                        hashMap = json.GetArrayValue("Distribution");
                        if (hashMap.size() == 0) {
                            log.info("分账账户信息为空");
                            resultMap.put("Distribution", null);
                        } else {
                            log.info("分账账户信息明细为:");
                            resultMap.put("Distribution", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("PreAuthed") || payTypeID.equals("PreAuthCancel")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        return resultMap;
                    }
                }
            }
        } else {
            //6、商户订单查询失败
            resultMap.put("ReturnCode", ReturnCode);
            resultMap.put("ErrorMessage", ErrorMessage);
            return resultMap;
        }
        return null;
    }


    @Override
    public Map sendTransferQuery(TransferQueryVo transferQueryVo) {
        Map<String, Object> resultMap = new HashMap();
        String queryTpye = "false";
//        if (queryTpye.equals("0")) {
//            queryTpye = "false";
//        } else if (queryTpye.equals("1")) {
//            queryTpye = "true";
//        }
        String payTypeID = transferQueryVo.getPayType();
        String OrderNo = transferQueryVo.getOldRequestSn();
        QueryOrderRequest tQueryRequest = new QueryOrderRequest();
        tQueryRequest.queryRequest.put("PayTypeID", payTypeID);    //设定交易类型
        tQueryRequest.queryRequest.put("OrderNo", OrderNo);    //设定订单编号 （必要信息）
        tQueryRequest.queryRequest.put("QueryDetail", queryTpye);//设定查询方式
        //如果需要专线地址，调用此方法：
        //tQueryRequest.setConnectionFlag(true);
        JSON json = tQueryRequest.postRequest();
        //JSON json = tQueryRequest.extendPostRequest(1);

        String ReturnCode = json.GetKeyValue("ReturnCode");
        String ErrorMessage = json.GetKeyValue("ErrorMessage");

        if (ReturnCode.equals("0000")) {
            log.info("ReturnCode   = [" + ReturnCode + "]");
            log.info("ErrorMessage = [" + ErrorMessage + "]");
            //4、获取结果信息
            String orderInfo = json.GetKeyValue("Order");
            if (orderInfo.length() < 1) {
                log.info("查询结果为空");
            } else {
                //1、还原经过base64编码的信息
                Base64 tBase64 = new Base64();
                String orderDetail = new String(tBase64.decode(orderInfo));
                json.setJsonString(orderDetail);
                log.info("订单明细" + orderDetail);

                if (queryTpye.equals("0")) {
                    resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                    resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                    resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                    resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                    resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                    resultMap.put("Status", json.GetKeyValue("Status"));
                    return resultMap;
                } else {
                    LinkedHashMap hashMap = new LinkedHashMap();
                    if (payTypeID.equals("ImmediatePay") || payTypeID.equals("PreAuthPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("OrderDesc", json.GetKeyValue("OrderDesc"));
                        resultMap.put("OrderURL", json.GetKeyValue("OrderURL"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));
                        //5、商品明细
                        hashMap = json.GetArrayValue("OrderItems");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("DividedPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("InstallmentCode", json.GetKeyValue("InstallmentCode"));
                        resultMap.put("InstallmentNum", json.GetKeyValue("InstallmentNum"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));

                        hashMap = json.GetArrayValue("OrderItems");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        hashMap.clear();
                        hashMap = json.GetArrayValue("Distribution");
                        if (hashMap.size() == 0) {
                            log.info("分账账户信息为空");
                            resultMap.put("Distribution", null);
                        } else {
                            log.info("分账账户信息明细为:");
                            resultMap.put("Distribution", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("Refund")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("RefundAmount", json.GetKeyValue("RefundAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("MerRefundAccountNo", json.GetKeyValue("MerRefundAccountNo"));
                        resultMap.put("MerRefundAccountName", json.GetKeyValue("MerRefundAccountName"));
                        return resultMap;
                    } else if (payTypeID.equals("AgentPay")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("InstallmentCode", json.GetKeyValue("InstallmentCode"));
                        resultMap.put("InstallmentNum", json.GetKeyValue("InstallmentNum"));
                        resultMap.put("PaymentLinkType", json.GetKeyValue("PaymentLinkType"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("CommodityType", json.GetKeyValue("CommodityType"));
                        resultMap.put("ReceiverAddress", json.GetKeyValue("ReceiverAddress"));
                        resultMap.put("BuyIP", json.GetKeyValue("BuyIP"));
                        resultMap.put("iRspRef", json.GetKeyValue("iRspRef"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        resultMap.put("MerchantRemarks", json.GetKeyValue("MerchantRemarks"));
                        hashMap = json.GetArrayValue("OrderItem");
                        if (hashMap.size() == 0) {
                            log.info("商品明细为空");
                            resultMap.put("OrderItems", null);
                        } else {
                            log.info("商品明细为:");
                            resultMap.put("OrderItems", hashMap);
                        }
                        //4、获取分账账户信息
                        hashMap.clear();
                        hashMap = json.GetArrayValue("Distribution");
                        if (hashMap.size() == 0) {
                            log.info("分账账户信息为空");
                            resultMap.put("Distribution", null);
                        } else {
                            log.info("分账账户信息明细为:");
                            resultMap.put("Distribution", hashMap);
                        }
                        return resultMap;
                    } else if (payTypeID.equals("PreAuthed") || payTypeID.equals("PreAuthCancel")) {
                        resultMap.put("PayTypeID", json.GetKeyValue("PayTypeID"));
                        resultMap.put("OrderNo", json.GetKeyValue("OrderNo"));
                        resultMap.put("OrderDate", json.GetKeyValue("OrderDate"));
                        resultMap.put("OrderTime", json.GetKeyValue("OrderTime"));
                        resultMap.put("OrderAmount", json.GetKeyValue("OrderAmount"));
                        resultMap.put("Status", json.GetKeyValue("Status"));
                        resultMap.put("AcctNo", json.GetKeyValue("AcctNo"));
                        resultMap.put("ReceiveAccount", json.GetKeyValue("ReceiveAccount"));
                        resultMap.put("ReceiveAccName", json.GetKeyValue("ReceiveAccName"));
                        return resultMap;
                    }
                }
            }
        } else {
            //6、商户订单查询失败
            resultMap.put("ReturnCode", ReturnCode);
            resultMap.put("ErrorMessage", ErrorMessage);
            return resultMap;
        }
        return null;
    }

    @Override
    public String getResult(TransferQueryVo transferQueryVo) {
        Map<String, Object> resultMap = new HashMap();
        String queryTpye = "true";
//        if (queryTpye.equals("0")) {
//            queryTpye = "false";
//        } else if (queryTpye.equals("1")) {
//            queryTpye = "true";
//        }
        String payTypeID = transferQueryVo.getPayType();
        String OrderNo = transferQueryVo.getOldRequestSn();
        QueryOrderRequest tQueryRequest = new QueryOrderRequest();
        tQueryRequest.queryRequest.put("PayTypeID", payTypeID);    //设定交易类型
        tQueryRequest.queryRequest.put("OrderNo", OrderNo);    //设定订单编号 （必要信息）
        tQueryRequest.queryRequest.put("QueryDetail", queryTpye);//设定查询方式
        //如果需要专线地址，调用此方法：
        //tQueryRequest.setConnectionFlag(true);
        JSON json = tQueryRequest.postRequest();
        //JSON json = tQueryRequest.extendPostRequest(1);

        String ReturnCode = json.GetKeyValue("ReturnCode");
        String ErrorMessage = json.GetKeyValue("ErrorMessage");

        if (ReturnCode.equals("0000")) {
            log.info("ReturnCode   = [" + ReturnCode + "]");
            log.info("ErrorMessage = [" + ErrorMessage + "]");
            //4、获取结果信息
            String orderInfo = json.GetKeyValue("Order");
            if (orderInfo.length() < 1) {
                log.info("查询结果为空");
                return null;
            } else {
                //1、还原经过base64编码的信息
                Base64 tBase64 = new Base64();
                String orderDetail = new String(tBase64.decode(orderInfo));
                json.setJsonString(orderDetail);
                log.info("订单明细" + orderDetail);
                if (payTypeID.equals("ImmediatePay") || payTypeID.equals("PreAuthPay")) {
                    return json.GetKeyValue("Status");
                } else if (payTypeID.equals("DividedPay")) {
                    return json.GetKeyValue("Status");
                } else if (payTypeID.equals("Refund")) {
                    return json.GetKeyValue("Status");
                } else if (payTypeID.equals("AgentPay")) {
                    return json.GetKeyValue("Status");
                } else if (payTypeID.equals("PreAuthed") || payTypeID.equals("PreAuthCancel")) {
                    return json.GetKeyValue("Status");
                }
            }
        }
        //6、商户订单查询失败
        return ErrorMessage;
}
}
