package com.choosefine.paycenter.channel.abc.api.handler;

import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractRechargeHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-25 16:56
 **/
@Component("ABC_RECHARGE_HANDLER")
public class ABCRechargerHandler extends AbstractRechargeHandler {
    @Autowired
    @Qualifier("abcPayService")
    private PayService abcPayService;

    @PostConstruct
    public void init() {
        this.payService=abcPayService;
    }
}
