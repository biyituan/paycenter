package com.choosefine.paycenter.channel.abc.entity;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-24 17:13
 **/
public class ABCPaymentLinkType {
    public static final String ABC_INTERNET = "1";//internet网接入
    public static final String ABC_MOBILE = "2";//手机网络接入
    public static final String ABC_DIGITAL_TELEVISION = "3";//数字电视
    public static final String ABC_SMART_CLIENT = "4";//智能客户端
}
