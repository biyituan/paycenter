package com.choosefine.paycenter.channel.abc.api.handler;

import com.choosefine.paycenter.pay.api.handler.AbstractPayBackHandler;
import com.choosefine.paycenter.pay.dto.PayBackDto;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-17 11:41
 **/
@Component("ABC_PAY_BACK_HANDLER")
public class ABCPaybackHandler extends AbstractPayBackHandler{
    @Override
    public String getPayErrorMsg(PayBackDto payBackDto) {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    protected String getSign(Map<String, String> params, String payType) {
        return null;
    }

    @Override
    protected boolean isPaySuccess(Map<String, String> params, String payType) {
        return false;
    }
}
