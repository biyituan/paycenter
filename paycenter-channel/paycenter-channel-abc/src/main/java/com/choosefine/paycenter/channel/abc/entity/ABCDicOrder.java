package com.choosefine.paycenter.channel.abc.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦 2017下午2:33:16
 */
@Getter
@Setter
@ToString
public class ABCDicOrder{
	private String PayTypeID=""; // 交易类型
	private String OrderDate=""; // 订单日期
	private String OrderTime=""; // 订单时间
	private String OrderDesc=""; // 订单说明 非必须
	private String orderTimeountDate=""; // 订单支付有效期 非必须，YYYYMMDDHHMMSS(24 小时制)
	private String OrderNo=""; // 交易编号 必须设定
	private String CurrencyCode="156"; // 交易币种 必须设定，156：人民币
	private String OrderAmount=""; // 交易金额必须设定，保留 2 位小数，以“元”为单位
	private String Fee=""; // 手续费金额非必须，保留 2 位小数，以“元”为单位
	private String AccountNo=""; // 支付账户 非必须
	private String OrderURL=""; // 订单地址
	private String ReceiverAddress=""; // 收货地址 非必须
	private String InstallmentMark=""; // 分期标识必须设定，1：分期；0：非分期。交易类型为直接支付或预授权支付时，分期标识不允许输入为
	private String InstallmentNum="";// 分期期数
	private String InstallmentCode=""; // 分期代码 非必须
	private String CommodityType=""; // 商品种类 必须
	private String BuyIP=""; //客户交易 IP 非必须，合法 ip 地址
	private String ExpiredDate=""; //订单保存时间 非必须，以天为单位
}
