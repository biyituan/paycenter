package com.choosefine.paycenter.channel.abc.entity;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-24 17:00
 **/
public class ABCPaymentType {
    public static final String ABC_CARD_PAY = "1";//农行卡支付
    public static final String ABC_INTERNATIONAL_CARD_PAY = "2";//国际卡支付
    public static final String ABC_CREDIT_CRAD_PAY = "3";//农行贷记卡支付
    public static final String ABC_THIRD_PARTY_CROSS_BANK_PAY = "5";//基于第三方的跨行支付
    public static final String ABC_CUP_CROSS_BANK_PAY = "6";//银联跨行支付
    public static final String ABC_PUBLIC_ACCOUNTS_PAY = "7";//对公户
    public static final String ABC_PYAMENT_MERGE_PAY = "A";//支付方式合并(1和3的合并)
}
