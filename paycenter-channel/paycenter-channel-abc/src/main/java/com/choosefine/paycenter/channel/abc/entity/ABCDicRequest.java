package com.choosefine.paycenter.channel.abc.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦
	2017下午3:22:40
 */
@Getter
@Setter
@ToString
public class ABCDicRequest {
	private String PaymentType=""; //支付账户类型必须设定，1：农行卡支付 2：国际卡支付 3：农行贷记卡支付 5:基于第三方的跨行支付 A:支付方式合并 6：银联跨行支付
	private String PaymentLinkType=""; //交易渠道必须设定，1：internet 网络接入 2：手机网络接入3:数字电视网络接入 4:智能客户端
	private String ReceiveAccount=""; //收款方账号 非必须
	private String  ReceiveAccName=""; //收款方户名 非必须
	private String NotifyType=""; //通知方式必须设定，0：URL 页面通知 1：服务器通知
	private String ResultNotifyURL="";//通 知 URL 地址必须设定，合法的 URL地址
	private String MerchantRemarks=""; //附言 非必须
	private String IsBreakAccount=""; //交易是否分账 必须设定，0:否；1:是
	private String SplitAccTemplate=""; //分账模板编号 非必须
	private String UnionPayLinkType=""; //银联跨行移动支付接入方式 非必须，但是如果选择支付帐户类型为 6(银联跨行支付)交易渠道为 2(手机网络接入)，必须设定
}
