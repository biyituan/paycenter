package com.choosefine.paycenter.channel.abc.entity;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-04-25 14:53
 **/
public class ABCOrderStatus {
    public static final String ABC_ORDER_NOTPAY = "01";//未支付
    public static final String ABC_ORDER_NORESPONSE = "02";//无回应
    public static final String ABC_ORDER_HAS_REQUEST = "03";//已请款
    public static final String ABC_ORDER_SUCCESS = "04";//成功
    public static final String ABC_ORDER_REFUNDS = "05";//已退款
    public static final String ABC_ORDER_AUTH_CONFIRM_SUCCESS = "07";//授权确认成功
    public static final String ABC_ORDER_FAIL = "99";//失败
    public static final String ABC_ORDER_AUTH_CANCLE = "00";//授权取消

}
