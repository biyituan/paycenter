package com.choosefine.paycenter.channel.abc.api.impl;

import com.choosefine.paycenter.common.enums.PayType;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-16 19:56
 **/
public enum ABCPayType implements PayType{
    //建行B2C网银支付
    B2C_NB("农行B2CPC支付"),
    //建行B2B网银支付
    B2B_NB("农行B2BPC支付"),
    //建行B2C手机支付
    B2C_APP("农行B2C手机支付");

    private String desc;

    private ABCPayType(String desc) {
        this.desc = desc;
    };

    @Override
    public String getType() {
        return this.name();
    }
}
