package com.choosefine.paycenter.channel.abc.entity;

import com.choosefine.paycenter.channel.abc.entity.ABCDicOrder;
import com.choosefine.paycenter.channel.abc.entity.ABCDicRequest;
import com.choosefine.paycenter.channel.abc.entity.ABCOrderitems;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦
	2017下午3:38:47
 */
@Getter
@Setter
@ToString
/**
 * 支付请求类
 */
public class ABCPaymentRequest {
	private ABCDicOrder abcDicOrder;
	private ABCDicRequest abcDicRequest;
	private ABCOrderitems abcOrderitems;

	public ABCPaymentRequest() {
		this.abcDicOrder = new ABCDicOrder();
		this.abcDicRequest = new ABCDicRequest();
		this.abcOrderitems = new ABCOrderitems();
	}
}
