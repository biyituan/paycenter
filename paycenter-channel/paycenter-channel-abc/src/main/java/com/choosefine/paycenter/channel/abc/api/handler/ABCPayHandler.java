package com.choosefine.paycenter.channel.abc.api.handler;

import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractPayAgencyPayHandler;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.dto.PayOrderDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-17 10:56
 **/
@Slf4j
@Component("ABC_PAY_HANDLER")
public class ABCPayHandler extends AbstractPayAgencyPayHandler{
    @Autowired
    @Qualifier("abcPayService")
    private PayService abcPayService;
    @Override
    protected String getGotoUrl(PayBizzRequestDto payBizzRequest) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        PayOrderDto payOrder = new PayOrderDto();
        payOrder.setOrderId(payBizzRequest.getPaySn());
        payOrder.setAmount(payBizzRequest.getAmount());
        return abcPayService.buildRequest(payOrder,payBizzRequest.getPayType());
    }
}
