package com.choosefine.paycenter.channel.abc.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-03 9:52
 * 农行部分配置参数
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ABCMerchantPara implements Serializable{
    private String TrustPayTrxURL;
    private String TrustPayIETrxURL;
    private String TrustPayCertFile;
    private String TrustStoreFile;
    private String TrustStorePassword;
    private String MerchantID;
    private String MerchantKeyStoreType;
    private String MerchantCertFile;
    private String MerchantCertPassword;
}
