package com.choosefine.paycenter.channel.self.api;

import com.choosefine.paycenter.common.enums.PayType;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/13
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum  SelfPayType implements PayType{
    BALANCE("余额支付");

    private String desc;

    private SelfPayType(String desc){
        this.desc = desc;
    }

    @Override
    public String getType() {
        return this.name();
    }
}
