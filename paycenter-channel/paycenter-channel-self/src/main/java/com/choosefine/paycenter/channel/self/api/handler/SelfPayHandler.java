package com.choosefine.paycenter.channel.self.api.handler;

import com.choosefine.paycenter.account.dto.MainDto;
import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.api.handler.AbstractPayHandler;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.vo.PayResultVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Comments：余额支付处理器
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Component("SELF_PAY_HANDLER")
public class SelfPayHandler extends AbstractPayHandler {
   @Override
   public PayResultVo doProcessSpecific(PayBizzRequestDto payBizzRequest){
            final MainDto mainDto = new MainDto(payBizzRequest.getAccountId(),payBizzRequest.getUserCode(),payBizzRequest.getAccountRealName(),payBizzRequest.getAmount());
            mainDto.setAccountRealName(payBizzRequest.getAccountRealName());
            final List<OrderDto> oppositeDtoList = payBizzRequest.getOrders();
           //校验支付密码是否正确
            getPayVerifyService().checkPayPass(payBizzRequest.getAccountId(),payBizzRequest.getPayPass());
            //校验交易主体账号余额是否充足
            getPayVerifyService().checkMainAccountBalanceEnough(payBizzRequest.getAccountId(),payBizzRequest.getAmount());
            //更新支付单状态为成功（这个不放这里处理,改为使用事务消息）
            //发送支付成功的消息（账户余额的更新交于账户账单模块处理，支付模块只负责发送消息（支付单状态更改为已支付，肯定会发送支付完成的消息））
            sendPaidSuccessMessage(payBizzRequest);
            //余额支付无需回数据
            PayResultVo payResultVo = getPayResultVo(payBizzRequest);
            return payResultVo;
   }

    private PayResultVo getPayResultVo(PayBizzRequestDto payBizzRequest) {
        PayResultVo payResultVo = new PayResultVo();
        payResultVo.setNeedRedirect(false);
        payResultVo.setSn(payBizzRequest.getPaySn());
        payResultVo.setAmount(payBizzRequest.getAmount());
        payResultVo.setUrl(null);
        return payResultVo;
    }
}


