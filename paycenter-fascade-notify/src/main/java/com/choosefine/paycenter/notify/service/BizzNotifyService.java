package com.choosefine.paycenter.notify.service;

import com.choosefine.paycenter.notify.model.BizzNotifyRecord;

/**
 * Comments：通知服务
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface BizzNotifyService {

    public int saveNotifyRecord(BizzNotifyRecord bizzNotifyRecord);

    public void recordChannelBizzSys();

    // 记录异步通知业务系统的任务
    public void recodNotifyBizzSys();

}
