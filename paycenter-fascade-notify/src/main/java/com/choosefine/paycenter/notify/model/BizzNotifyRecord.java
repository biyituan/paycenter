package com.choosefine.paycenter.notify.model;

import com.choosefine.paycenter.common.enums.BizzNotifyStatus;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_bizz_notify_record")
public class BizzNotifyRecord extends BaseModel implements Serializable{
    /**已通知次数*/
    private Integer notifyTimes;
    /**最多通知次数*/
    private Integer limitNotifyTimes;
    /**业务系统流水号*/
    private String bizzSn;
    /**业务系统标识*/
    private BizzSys bizzSys;
    /**下次通知时间*/
    private Long nextNotifyTime;
    /**业务通知状态*/
    private BizzNotifyStatus status;
    /**业务系统通知url*/
    private String notifyUrl;
    /**创建时间*/
    private Timestamp createdAt;
    /**最后修改时间*/
    private Timestamp updatedAt;
}
