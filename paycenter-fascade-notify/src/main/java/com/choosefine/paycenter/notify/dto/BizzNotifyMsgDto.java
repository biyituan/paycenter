package com.choosefine.paycenter.notify.dto;

import com.choosefine.paycenter.common.enums.BizzSys;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Comments：通知业务系统消息Dto
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class BizzNotifyMsgDto {
     /**金额*/
     private BigDecimal amount;
     /**业务系统业务流水号*/
     private String bizzSn;
     /**业务系统标识*/
     private BizzSys bizzSys;
     /**支付流水号*/
     private String paySn;
     /**支付完成时间*/
     private Long paidTime;
     /**结果*/
     private String result;
     /**签名*/
     private String signature;
     /**交易类型*/
     private String tradeType;
}
