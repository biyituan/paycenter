package com.choosefine.paycenter.notify.service;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface BizzCallbackLogService {


    public int recordBizzCallbackLog();

    /**
     * 记录业务系统回调成功
     * @param bizzSn 业务流水号
     * @return 影响记录数
     */
    public int recordBizzCallbackSuccessByBizzSn(String bizzSn);
}
