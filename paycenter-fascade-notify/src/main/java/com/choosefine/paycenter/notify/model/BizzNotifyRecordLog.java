package com.choosefine.paycenter.notify.model;

import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_bizz_nofity_record_log")
public class BizzNotifyRecordLog extends BaseModel implements Serializable {
    /**业务通知记录id*/
    private Long bizzNotifyId;
    /**请求内容*/
    private String request;
    /**响应内容*/
    private String response;
    /**业务系统标识*/
    @Enumerated(EnumType.STRING)
    private BizzSys bizzSys;
    /**业务流水号*/
    private String bizzSn;
    /**http状态*/
    private String httpStatus;
    /**创建时间*/
    private Timestamp createdAt;
    /**最后更新时间*/
    private Timestamp updatedAt;
    /**MQ消息编号*/
    private String mqSn;
}
