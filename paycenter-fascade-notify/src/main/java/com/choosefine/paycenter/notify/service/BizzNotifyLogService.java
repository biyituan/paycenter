package com.choosefine.paycenter.notify.service;

/**
 * Comments：通知日志记录服务
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface BizzNotifyLogService {

    /**
     * 记录通知日志
     * @return
     */
    public int recordNotifyLog();

    /**
     * 记录通知成功
     * @param bizzSn 业务流水号
     * @return
     */
    public int recordNotifySuccessByBizzSn(String bizzSn);
}
