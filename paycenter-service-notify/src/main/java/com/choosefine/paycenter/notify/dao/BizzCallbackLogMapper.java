package com.choosefine.paycenter.notify.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.notify.model.BizzNotifyRecordLog;
import org.apache.ibatis.annotations.Update;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Deprecated
public interface BizzCallbackLogMapper extends BaseMapper<BizzNotifyRecordLog>{
    @Update("UPDATE paycenter_bizz_nofity_record_log SET status = 1 WHERE bizz_sn = #{bizzSn}")
    int bizzNotifyRecordLog(String bizzSn);

}
