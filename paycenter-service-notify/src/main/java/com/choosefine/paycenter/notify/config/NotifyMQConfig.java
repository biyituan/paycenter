package com.choosefine.paycenter.notify.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/3
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Configuration
@ImportResource(locations = {"classpath:conf/notify/spring-mq.xml"})
public class NotifyMQConfig {
}
