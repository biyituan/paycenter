package com.choosefine.paycenter.notify.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.notify.model.BizzNotifyRecord;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Deprecated
public interface BizzNotifyRecordMapper extends BaseMapper<BizzNotifyRecord>{
    @Select("SELECT * FROM paycenter_bizz_notify_record bnr WHERE bizz_sn=#{bizzSn} and bizz_sys=#{bizzSys}")
    BizzNotifyRecord selectByBizzSn(@Param("bizzSn") String bizzSn, @Param("bizzSys") String bizzSys);

}
