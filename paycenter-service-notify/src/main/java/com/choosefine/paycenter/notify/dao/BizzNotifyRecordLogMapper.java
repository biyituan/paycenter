package com.choosefine.paycenter.notify.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.notify.model.BizzNotifyRecordLog;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Deprecated
public interface BizzNotifyRecordLogMapper extends BaseMapper<BizzNotifyRecordLog> {
}
