package com.choosefine.paycenter.reconciliation.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author Ye_Wenda
 * @Date 7/18/2017
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_channel_income_bill")
public class ChannelIncomeBill {

    public static final String COMMENT_SEPARATOR = " $#$ ";

    private Long id;
    private String channelCode;
    private String paySn;
    private String account;
    private BigDecimal amount;
    private Integer status;
    private String comment;
    private Date accountAt;
    private Date transactionAt;
    private Date createdAt;
    private Date deletedAt;
}
