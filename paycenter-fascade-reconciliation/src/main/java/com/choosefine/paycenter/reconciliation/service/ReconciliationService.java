package com.choosefine.paycenter.reconciliation.service;

import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;

import java.util.Date;
import java.util.List;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
public interface ReconciliationService {

    /**
     * 查询工作日进账流水
     * @param day
     * @return
     */
    List<ChannelIncomeBill> getDayBills(Date day);

}
