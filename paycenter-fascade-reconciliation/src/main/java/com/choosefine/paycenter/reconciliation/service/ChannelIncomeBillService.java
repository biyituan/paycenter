package com.choosefine.paycenter.reconciliation.service;

import com.choosefine.paycenter.reconciliation.model.ChannelIncomeBill;

import java.util.List;

/**
 * @Author Ye_Wenda
 * @Date 7/20/2017
 */
public interface ChannelIncomeBillService {

    long save(ChannelIncomeBill channelIncomeBill);

    long saveAll(List<ChannelIncomeBill> channelIncomeBills);

    long update(ChannelIncomeBill channelIncomeBill);

    ChannelIncomeBill get(Long id);

    ChannelIncomeBill getBySn(String sn);

    List<ChannelIncomeBill> getAll();

}
