package com.choosefine.paycenter.reconciliation.dispatcher;


import com.choosefine.paycenter.reconciliation.handler.ReconciliationHandler;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
public interface ReconciliationDispatcher {

    public static final String RECONCILIATION_HANDLER_SUFFIX = "_RECONCILIATION_HANDLER";

    public ReconciliationHandler getReconciliationHandler(String bankType);
}
