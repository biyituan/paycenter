package com.choosefine.paycenter.reconciliation.handler;

import java.util.Date;

/**
 * @Author Ye_Wenda
 * @Date 7/17/2017
 */
public interface ReconciliationHandler {

    /**
     * Get yesterday's account bill from Bank(or 3-party system).
     * @param date
     */
    void prepareIncome(Date date);
    void prepareWithdraw();
}
