package com.choosefine.paycenter.settlement.service.impl;

import com.choosefine.paycenter.pay.service.ChannelService;
import com.choosefine.paycenter.settlement.service.WithDrawBankRoutingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 15:56
 **/
@Service
public class WithDrawBankRoutingServiceImpl implements WithDrawBankRoutingService {
    @Autowired
    ChannelService channelService;

    @Override
    public String chooseChannel(String bankCode) {
        String channel = channelService.findChannelListDistinct(bankCode);
        if(null!=channel) return channel;
        else
        return "CCB";
    }
}
