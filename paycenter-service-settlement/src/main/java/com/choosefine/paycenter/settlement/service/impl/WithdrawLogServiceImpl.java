package com.choosefine.paycenter.settlement.service.impl;

import com.choosefine.paycenter.settlement.dao.WithdrawLogMapper;
import com.choosefine.paycenter.settlement.model.WithdrawLog;
import com.choosefine.paycenter.settlement.service.WithdrawLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Condition;
import tk.mybatis.mapper.entity.Example;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-10 11:39
 **/
@Service
public class WithdrawLogServiceImpl implements WithdrawLogService {
    @Autowired
    WithdrawLogMapper withdrawLogMapper;

    @Override
    public int addLog(WithdrawLog withdrawLog) {
     return  withdrawLogMapper.insertSelective(withdrawLog);
    }

    @Override
    public int updateWithdrawLogByRequestSn(WithdrawLog withdrawLog,String requestSn) {
        Example example =new Example(WithdrawLog.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("requestSn",requestSn);

        return withdrawLogMapper.updateByExampleSelective(withdrawLog,example);
    }
}
