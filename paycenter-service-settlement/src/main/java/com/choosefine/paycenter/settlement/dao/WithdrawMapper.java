package com.choosefine.paycenter.settlement.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.settlement.model.Withdraw;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 11:51
 **/
public interface WithdrawMapper extends BaseMapper<Withdraw> {
    @Select("SELECT * FROM paycenter_withdraw WHERE w_sn=#{wSn}")
    public Withdraw selectByWSn(@Param("wSn") String wSn);

    @Select("SELECT * FROM paycenter_withdraw WHERE status='PROCESSING'")
    public List<Withdraw> selectByStatus();

    @Select("SELECT * FROM paycenter_withdraw WHERE status in('WAITING','PROCESSING') AND account_id=#{accountId}")
    public Withdraw selectWaitingWsByAccId(@Param("accountId") Long accountId);
}
