package com.choosefine.paycenter.settlement.dispatcher.impl;

import com.choosefine.paycenter.pay.api.handler.RechargeHandler;
import com.choosefine.paycenter.settlement.dispatcher.WithdrawDispatcher;
import com.choosefine.paycenter.settlement.handler.WithdrawHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-06 10:50
 **/
@Component
public class WithdrawDispatcherImpl implements WithdrawDispatcher,BeanFactoryAware {
    private BeanFactory beanFactory;
    @Override
    public WithdrawHandler getWithdrawHandler(String bankType) {
        return beanFactory.getBean(bankType.toUpperCase()+WITHDRAW_HANDLER_PREFIX,WithdrawHandler.class);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
