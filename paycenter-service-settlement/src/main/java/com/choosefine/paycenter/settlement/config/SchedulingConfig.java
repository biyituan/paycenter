package com.choosefine.paycenter.settlement.config;

import com.choosefine.paycenter.account.dto.AccountWdDto;
import com.choosefine.paycenter.account.service.AccountService;
import com.choosefine.paycenter.common.enums.WithdrawLogType;
import com.choosefine.paycenter.common.enums.WithdrawStatus;
import com.choosefine.paycenter.pay.dispatcher.TransferQueryDispatcher;
import com.choosefine.paycenter.pay.dto.TransferQueryDto;
import com.choosefine.paycenter.pay.handler.TransferQueryHandler;
import com.choosefine.paycenter.pay.vo.TransferQueryVo;
import com.choosefine.paycenter.settlement.model.Withdraw;
import com.choosefine.paycenter.settlement.model.WithdrawLog;
import com.choosefine.paycenter.settlement.service.WithdrawLogService;
import com.choosefine.paycenter.settlement.service.WithdrawService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 16:25
 **/
@Slf4j
@Configuration
@EnableScheduling // 启用定时任务
public class SchedulingConfig {
    @Autowired
    private WithdrawService withdrawService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private WithdrawLogService withdrawLogService;

    @Autowired
    private TransferQueryDispatcher transferQueryDispatcher;

    AccountWdDto accountDTO = null;

    //@Scheduled(cron = "0/60 * * * * ?") //测试的时候 每60秒执行一次
    @Scheduled(cron = "0 0 1,4,7 * * ?")//生产环境每天的1,4,7点执行
    public void withdrawScheduler() throws Exception {
        log.info("---------------->跨行提现scheduled开始 ");
        List<Withdraw> withdrawList = withdrawService.selectByStatus();
        if (withdrawList.size() > 0) {
            for (Withdraw withdraw : withdrawList) {
                String bankRequestSn = withdraw.getBankRequestSn();
                TransferQueryDto transferQueryDto = new TransferQueryDto();
                transferQueryDto.setOldRequestSn(bankRequestSn);
                TransferQueryHandler transferQueryHandler = transferQueryDispatcher.getTransferQueryHandler(withdraw.getWPayBankCode());
                TransferQueryVo transferQueryVo = transferQueryHandler.buildQuery(transferQueryDto);
                String result = transferQueryHandler.getTransferService().getResult(transferQueryVo);
                if (null != result && result.equals("success")) {
                    withdrawService.updateAfterWithdrawSuccess(withdraw);
                    Withdraw withdraw1 = new Withdraw();
                    withdraw1.setId(withdraw.getId());
                    withdraw1.setStatus(WithdrawStatus.COMPLETE);
                    withdrawService.updateWithdraw(withdraw1);
                    WithdrawLog withdrawLog = new WithdrawLog();
                    withdrawLog.setLogType(WithdrawLogType.SUCCESS);
                    withdrawLog.setMessage("提现成功:提现金额" + withdraw.getAmount() + "元");
                    withdrawLog.setRequestSn(bankRequestSn);
                    withdrawLogService.updateWithdrawLogByRequestSn(withdrawLog, bankRequestSn);
                } else if (null != result && result.equals("processing")) {
                    //处理中的时候什么都不做
                } else {
                    accountDTO = new AccountWdDto();
                    BigDecimal balance = accountService.findBalanceById(withdraw.getAccountId());
                    accountDTO.setAccountId(withdraw.getAccountId());
                    accountDTO.setAvailableBalance(balance);
                    accountDTO.setBalance(balance);
                    accountDTO.setFreezeBalance(new BigDecimal(0.00));
                    accountService.updateAccountBalanceBeforWithDraw(accountDTO);
                    Withdraw withdraw1 = new Withdraw();
                    withdraw1.setId(withdraw.getId());
                    withdraw1.setStatus(WithdrawStatus.FAILURE);
                    withdrawService.updateWithdraw(withdraw1);
                    WithdrawLog withdrawLog = new WithdrawLog();
                    withdrawLog.setMessage("提现失败:" + result);
                    withdrawLog.setLogType(WithdrawLogType.BANK_ERROR);
                    withdrawLog.setRequestSn(bankRequestSn);
                    withdrawLogService.updateWithdrawLogByRequestSn(withdrawLog, bankRequestSn);
                }
            }
        }
        log.info("---------------->跨行提现scheduled结束 ");
    }

}
