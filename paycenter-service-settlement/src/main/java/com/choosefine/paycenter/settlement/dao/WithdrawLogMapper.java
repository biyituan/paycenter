package com.choosefine.paycenter.settlement.dao;

import com.choosefine.paycenter.common.dao.BaseMapper;
import com.choosefine.paycenter.settlement.model.WithdrawLog;
import org.apache.ibatis.annotations.Update;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-10 11:37
 **/
public interface WithdrawLogMapper extends BaseMapper<WithdrawLog> {


}
