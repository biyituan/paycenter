package com.choosefine.paycenter.channel.abc.entity;

import com.choosefine.paycenter.common.enums.PayType;

/**
 * Comments：农行交易类型枚举
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum ABCPayTypeId implements PayType {
    ImmediatePay("直接支付"),

    PreAuthPay("预授权支付"),

    DividedPay("分期支付"),

    AgentPay("授权支付"),

    Refund("退款"),

    DefrayPay("付款"),

    PreAuthed("预授权确认"),

    PreAuthCancel("预授权取消"),

    //农行B2C网银支付
    B2C_NB("农行B2C网银支付"),

    //农行B2B网银支付
    B2B_NB("农行B2B网银支付"),

    //农行B2C手机支付
    B2C_APP("农行B2C手机支付");

    private String desc;

    private ABCPayTypeId(String desc) {
        this.desc = desc;
    }

    public String getType(){
        return this.name();
    }
}

