package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.*;

import javax.persistence.Table;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/6/9
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "paycenter_payment_to_trade_order")
public class PaymentToTradeOrder extends BaseModel{
    /**支付流水号*/
    private String paySn;
    /**业务系统标识*/
    private BizzSys bizzSys;
    /**业务订单号*/
    private String orderSn;
}
