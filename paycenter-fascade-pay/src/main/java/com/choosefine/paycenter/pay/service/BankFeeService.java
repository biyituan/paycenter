package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.BankFee;

import java.math.BigDecimal;

/**
 * 手续费
 * Created by dyy on 2017/7/10.
 */
public interface BankFeeService {
    /**
     * rateType:扣率类型：
     * bankType:银行类型 ABC:农行 CBC：建行
     * operAmount:操作金额
     */
    public BigDecimal getBankFee(Integer rateType,String bankType,BigDecimal operAmount);

}
