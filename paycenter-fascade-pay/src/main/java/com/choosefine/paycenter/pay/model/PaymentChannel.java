package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 支付单与渠道
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_payment_channel")
public class PaymentChannel extends BaseModel implements Serializable {
    /**支付单流水号*/
    private String paySn;
    /**支付渠道交易流水号*/
    private String channelSn;
    /**支付渠道id*/
    private Long channelId;
    /**金额（单位：分）*/
    private BigDecimal amount;
    /**创建时间*/
    private Timestamp createdAt;
    /**最后修改时间*/
    private Timestamp updatedAt;
}
