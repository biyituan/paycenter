package com.choosefine.paycenter.pay.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Transient;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:14
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Bank {
    private String bankCode;
    private String bankName;
    private Integer status;
    @Transient
    private String logo;
    @Transient
    private String logoPc;
}
