package com.choosefine.paycenter.pay.dto;

import lombok.*;

import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/13
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PayOrderDto {
    /**用于提交给银行的订单id*/
    private String orderId;
    /**用于提交给银行请求的订单金额*/
    private BigDecimal amount;
}
