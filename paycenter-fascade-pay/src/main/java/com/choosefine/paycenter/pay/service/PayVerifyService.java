package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.account.dto.OppositeDto;
import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.common.enums.BizzSys;

import java.math.BigDecimal;
import java.util.List;
/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayVerifyService {

    /**
     * 校验交易主体账号是否存在，是否冻结
     * @param accountId
     */
    public void checkMainAccountExistAndIsBlocked(Long accountId);

    /**
     * 校验交易对方是否存在
     * @param ordersDtos
     */
    public void checkOppositeAccounts(List<OrderDto> ordersDto);

    /**
     * 校验交易对方是否存在
     * @param opposites
     */
    public void checkOppositeAccountsByOppo(List<OrderDto> orderDtos);

    //校验交易主体账户余额是否充足
    public void checkMainAccountBalanceEnough(Long accountId,BigDecimal amount);

    /**
     * 校验支付密码是否已经设置
     * @param mainAccount
     */
    public void  checkPayPassIsSet(Account mainAccount);

    /**
     * 校验请求中的业务流水号对应的支付单是否已经支付过了
     * @param bizzSys
     * @param bizzSn
     */
    public void checkPaymentExistsAndAlreadyPaid(BizzSys bizzSys,String bizzSn);

    /**
     *  根据业务流水号查询对应的支付流水号
     * @param bizzSn
     * @return
     */
    public String findPaySnByBizzSn(String bizzSn);

    /**
     * 校验支付单是否已经创建
     * @param bizzSys
     * @param bizzSn
     * @return
     */
    public boolean isPaymentExists(BizzSys bizzSys, String bizzSn);

    /**
     * 检查某个业务流水号对应的支付单是否已经支付过了
     * @return
     */
    public boolean hasAlreadyPaid(BizzSys bizzSys, String bizzSn);

    /**
     * 校验支付密码是否正确
     */
    public void checkPayPass(Long accountId,String payPass);

    /**
     *  校验是否已经支付
     * @param paySn
     * @param bizzSys
     * @param bizzSn
     */
    public void checkHasAlreadyPaid(String paySn,BizzSys bizzSys,String bizzSn);

    public void checkMainAccountExistAndIsBlocked(Account mainAccount);
}
