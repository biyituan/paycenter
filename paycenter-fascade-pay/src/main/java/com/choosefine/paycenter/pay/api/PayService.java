package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.pay.dto.PayOrderDto;
import com.choosefine.paycenter.pay.enums.NotifyType;

import java.util.Map;

/**
 * Comments：支付接口
 * Author：Jay Chang
 * Create Date：2017/4/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayService {

    /**
     * 设置支付配置
     *
     * @param payConfigStorage
     */
    PayService setPayConfigStorage(PayConfigStorage payConfigStorage);

    /**
     * 获取支付配置
     *
     * @return
     */
    PayConfigStorage getPayConfigStorage();

    /**
     * 返回给前端、APP的支付请求（构建到银行的请求）
     * @param payOrderDto
     * @param payType (这里的支付类型,如B2C网银支付、B2B网银支付、APP支付有关，不包括：退款、对账单下载等之类)
     * @return
     */
     String buildRequest(PayOrderDto payOrderDto, String payType);

    /**
     * 验证签名
     * @param params 回调参数
     * @param sign 签名
     * @param payType 支付类型
     * @param notifyType 通知类型
     * @return
     */
    boolean verifySign(Map<String, String> params, String sign, String payType);

    /**
     * 验证签名
     * @param params 回调参数
     * @param sign 签名
     * @param payType 支付类型
     * @param notifyType 通知类型
     * @return
     */
     boolean verifySign(Map<String, String> params, String sign, String payType, NotifyType notifyType);

    /**
     * 对请求源串进行签名
     *
     * @param sourceStr 源串
     * @return  签名值
     */
     String createSign(String sourceStr);

    /**
     * 返回创建的支付订单信息
     *
     * @param order 支付订单
     * @return
     * @see PayOrderDto
     */
    Map orderInfo(PayOrderDto order);

    /**
     * 从回调参数中获取相关参数，构建支付订单PayOrder实例
     * @param params
     * @return
     */
    PayOrderDto getPayOrderFromBackParam(Map<String, String> params);

    /**
     * 从回调参数中获取相关参数，构建支付订单PayOrderDto实例
     * @param params
     * @param payType
     * @return
     */
    PayOrderDto getPayOrderFromBackParam(Map<String, String> params, String payType);

    /**
     * 去银行查询某笔支付流水是否已经支付完成
     * @param sn
     * @return
     */
    boolean isPaySuccess(String sn);
}
