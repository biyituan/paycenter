package com.choosefine.paycenter.pay.api.handler;

import com.choosefine.paycenter.pay.api.PayService;
import com.choosefine.paycenter.pay.dto.PayBizzRequestDto;
import com.choosefine.paycenter.pay.vo.PayResultVo;

/**
 * 支付处理接口
 * Created by yym on 2017/4/30.
 */
public interface PayHandler {

    /**
     * 处理支付请求
     * @param payBizzRequestDto
     */
     PayResultVo doProcess(PayBizzRequestDto payBizzRequestDto) throws Exception;


     PayService getPayService();
}
