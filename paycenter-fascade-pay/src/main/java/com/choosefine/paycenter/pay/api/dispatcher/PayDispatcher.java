package com.choosefine.paycenter.pay.api.dispatcher;

import com.choosefine.paycenter.pay.api.handler.PayBackHandler;
import com.choosefine.paycenter.pay.api.handler.PayHandler;

/**
 * Comments：支付通道分发器
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayDispatcher{

    public static final String PAY_HANDLER_PREFIX = "_PAY_HANDLER";

    public static final String PAY_BACK_HANDLER_PREFIX = "_PAY_BACK_HANDLER";

    /**
     * 根据支付通道获取支付处理器
     * @param channel
     * @return
     */
    public PayHandler getPayHandler(String channel);

    /**
     * 根据支付通道获取支付回调处理器
     * @param channel
     * @return
     */
    public PayBackHandler getPayBackHandler(String channel);
}
