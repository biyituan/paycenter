package com.choosefine.paycenter.pay.handler;

import com.choosefine.paycenter.pay.dto.TransferQueryDto;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import com.choosefine.paycenter.pay.vo.TransferQueryVo;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:32
 **/
public abstract  class AbstractTransferQueryHandler implements TransferQueryHandler{
    protected TransferQueryService transferQueryService;

    @Override
    public TransferQueryVo buildQuery(TransferQueryDto dto) {
        TransferQueryVo transferQueryVo = new TransferQueryVo();
        transferQueryVo.setOldRequestSn(dto.getOldRequestSn());
        transferQueryVo.setRequestSn(String.valueOf(System.currentTimeMillis()));
        if (null!=dto.getPayType()){
            transferQueryVo.setPayType(dto.getPayType());
        }
        return transferQueryVo;
    }
}
