package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.City;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 15:04
 **/
public interface CityService {
    public List<City> selectCityListByProvinceCode(String provinceCode);
}
