package com.choosefine.paycenter.pay.constants;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayConstants {
    /**支付Topic tag PAID*/
    public final static String PAY_TAG_PAY_SUCCESS = "PAY_SUCCESS";
    /**支付失败Topic tag PAID_FAIL*/
    public final static String PAY_TAG_PAY_FAILURE = "PAY_FAILURE";

    /**每笔收取固定手续费*/
    public final static int RATETYPE_CONFIRMED_FEE=1;
    /**同行借记卡充值手续费率*/
    public final static int RATETYPE_SAME_DEBITCARD_FEE=2;
    /**非同行借记卡充值手续费率*/
    public final static int RATETYPE_DIFFERENT_DEBITCARD_FEE=3;
    /**同行贷记卡充值手续费率*/
    public final static int RATETYPE_SAME_CREDITCARD_FEE=4;
    /**非同行贷记卡充值手续费率*/
    public final static int RATETYPE_DIFFERENT_CREDITCARD_FEE=5;
    /**同行提现手续费*/
    public final static int RATETYPE_CASH_COMMISSION=6;
    /**非同行提现手续费*/
    public final static int RATETYPE_NON_ATTENDANCE_RATE=7;

}
