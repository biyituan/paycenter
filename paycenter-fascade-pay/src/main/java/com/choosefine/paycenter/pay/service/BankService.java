package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.Bank;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 16:50
 **/
public interface BankService {
    public List<Bank> selectSupportBankList();

    public String findBankNameByBankCode(String bankCode);
}
