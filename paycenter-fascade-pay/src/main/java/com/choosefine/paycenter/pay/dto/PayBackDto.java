package com.choosefine.paycenter.pay.dto;

import com.choosefine.paycenter.pay.enums.NotifyType;
import lombok.*;

import java.util.Map;

/** 支付回调
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PayBackDto {
    /** 回调的参数*/
    private Map<String,String> params;
    /** 支付通道*/
    private String channel;
    /** 支付类型*/
    private String payType;
    /** 通知类型（同步通知为return,异步通知为notify）*/
    private NotifyType notifyType;
}
