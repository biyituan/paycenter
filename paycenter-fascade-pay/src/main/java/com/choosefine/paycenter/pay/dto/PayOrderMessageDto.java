package com.choosefine.paycenter.pay.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.choosefine.paycenter.account.dto.OrderDto;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Comments：用于消息传输
 * 1.支付成功、支付失败（充值也是用的这个，因为充值实质也是支付）的消息
 * 2.账户账单子系统通过接受该消息体来创建支付单
 * Author：Jay Chang
 * Create Date：2017/5/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class PayOrderMessageDto implements Serializable{
    /**流水号(可以是支付流水号，或者充值流水号)*/
    private String sn;
    /**业务系统标识*/
    private BizzSys bizzSys;
    /**业务系统业务流水号*/
    private String bizzSn;
    /**交易对手*/
    private List<OrderDto> orders;
    /**交易类型*/
    private TradeType tradeType;
    /**账户真实姓名*/
    private String realName;
    /**交易主体租户编码*/
    private String userCode;
    /**账户id*/
    private Long accountId;
    /**交易金额*/
    private BigDecimal amount;
    /**支付通道*/
    private String channel;
    /**支付方式*/
    private String payType;
    /**支付成功标志*/
    private boolean success;
    /**支付失败的错误信息（如果有的话）*/
    private String errorMsg;
    /**完成时间*/
    private Long finishedAt;
}
