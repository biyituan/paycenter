package com.choosefine.paycenter.pay.api.dispatcher;

import com.choosefine.paycenter.pay.api.handler.RechargeHandler;

/**
 * Comments：充值处理分发器
 * Author：Jay Chang
 * Create Date：2017/4/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface RechargeDispatcher{
    public static final String RECHARGE_HANDLER_PREFIX = "_RECHARGE_HANDLER";

    /**
     * 根据支付通道获取充值处理器
     * @param channel
     * @return
     */
    RechargeHandler getRechargeHandler(String channel);
}
