package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.account.dto.RechargeDto;
import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.account.model.Recharge;
import com.choosefine.paycenter.common.enums.RechargeStatus;
import com.choosefine.paycenter.pay.vo.RechargeQueryVo;

/**
 * Comments：充值接口(充值与支付关系紧密，其实质就是支付，故放在pay模块)
 * Author：Jay Chang
 * Create Date：2017/4/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface RechargeService {

    Recharge findByRechargeSn(String sn);

    void saveRecharge(RechargeDto rechargeDto, Account account);

    int rechargeComplted(final String rSn);

    int rechargeFailure(final String rSn);

    String findReturnUrlByRechargeSn(final String rSn);

    RechargeStatus findStatusByRechargeSn(String rSn);

    RechargeQueryVo findRechargeQueryResultByRSn(String rSn);
}
