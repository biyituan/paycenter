package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.BankUnion;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 16:50
 **/
public interface BankUnionService {
    public List<BankUnion> selectBankUnionByCityCodeAndBankCode(String bankCode, String cityCode);
}
