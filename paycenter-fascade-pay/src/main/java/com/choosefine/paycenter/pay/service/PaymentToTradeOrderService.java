package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.pay.model.PaymentToTradeOrder;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:32
 **/
public interface PaymentToTradeOrderService {
    int recordPaymentToTradeOrder(PaymentDto paymentDto,String paySn);

    int savePaymentToTradeOrder(PaymentToTradeOrder paymentToTradeOrder);

    List<String> selectOrderSnByPaySn(String paySn);

    String selectPaySnByBizzSysAndOrderSn(String bizzSys,String orderSn);

    PaymentToTradeOrder findByPaySn(String paySn);

    List<PaymentToTradeOrder> findByOrderSn(String bizzSys,String orderSn);
}
