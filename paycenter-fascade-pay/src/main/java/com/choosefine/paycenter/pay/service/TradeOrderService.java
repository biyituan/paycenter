package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.pay.model.TradeOrder;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-06-09 19:32
 **/
public interface TradeOrderService {
    /**
     * 判断该订单是否是已经是未支付的
     * @param bizzSys
     * @param orderSn
     * @return
     */
    boolean isComplete(String bizzSys,String orderSn);

    int recordTradeOrder(PaymentDto paymentDto);

    TradeOrder findTradeOrderByBizzSysAndOrderSn(BizzSys bizzSys,String orderSn);

    List<TradeOrder> findOrdersByBizzSysAndOrderSns(BizzSys bizzSys, List<String> orderSn);

    List<TradeOrder> findOrdersByBizzSysAndBizzSn(BizzSys bizzSys, String bizzSn);

    int tradeOrderPaid(String bizzSys, String orderSn);

    int updateAsClosedStatusByBizzSysAndOrderSn(BizzSys bizzSys, String orderSn);
}
