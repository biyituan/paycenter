package com.choosefine.paycenter.pay.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 14:14
 **/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BankUnion {
    private String bankUnionCode;
    private String uBankName;
    private String areaCode;
    private String bankCode;
    private String provinceCode;
    private String provinceName;
    private String cityName;
}
