package com.choosefine.paycenter.pay.enums;

import com.choosefine.paycenter.common.enums.BaseEnum;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/17
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum  NotifyType implements BaseEnum{
    RETURN("页面同步通知"),
    NOTIFY("服务器异步通知");

    private String name;

    NotifyType(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}
