package com.choosefine.paycenter.pay.handler;

import com.choosefine.paycenter.pay.dto.TransferQueryDto;
import com.choosefine.paycenter.pay.service.TransferQueryService;
import com.choosefine.paycenter.pay.vo.TransferQueryVo;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 10:52
 **/
public interface TransferQueryHandler {
    public TransferQueryVo buildQuery(TransferQueryDto dto);

    public TransferQueryService getTransferService();
}
