package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.PayStatus;
import com.choosefine.paycenter.pay.dto.ChoosePayTypeDto;
import com.choosefine.paycenter.pay.dto.PayDto;
import com.choosefine.paycenter.pay.model.Payment;
import com.choosefine.paycenter.pay.vo.PayFromAccountBillResultVo;
import com.choosefine.paycenter.pay.vo.PayInfoVo;
import com.choosefine.paycenter.pay.vo.PayResultVo;
import org.springframework.transaction.annotation.Transactional;

/**
 * Comments：支付单服务
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PaymentService {

    public static final String BALANCE_PAY_TYPE = "BALANCE";

    /**
     * 创建支付单
     * @param paymentDto
     * @return
     */
    public String createPayment(PaymentDto paymentDto);

    public Payment findBizzSnAndBizzSysByPaySn(String tradeSn);

    /**
     * 保存支付单
     * @param payment
     * @return
     */
    public int savePayment(Payment payment);

    /**
     * 根据支付单号查询完成时间
     * @param paySn
     * @return
     */
    public Long findFinishedAtByPaySn(String paySn);

    /**
     * 根据业务系统订单号查询完成时间
     * @param bizzsys,ordersn
     * @return
     */
    public Long findFinishedAtByBizzSysAndOrderSn(String bizzsys,String ordersn);

    /**
     * 从账单入口付款
     * @param userCode
     * @param accountBillId
     * @return
     */
    public PayFromAccountBillResultVo createPayment(String userCode, Long accountBillId);


    public @interface CreatePayment{}

    /**
     * 支付页面信息
     * @param userCode
     * @param bizzSys
     * @param bizzSn
     */
    public PayInfoVo findPayInfo(String userCode, BizzSys bizzSys, String bizzSn);


    /**
     * 选择支付方式
     * @param choosePayTypeDto
     * @return
     */
    public int choosePayType(ChoosePayTypeDto choosePayTypeDto);
    public @interface ChoosePayType {}

    /**
     * 具体支付操作（若是网银支付，返回重定向到银行的url）
     * @param payDto
     * @return
     */
    @Transactional
    public PayResultVo doPay(PayDto payDto) throws Exception;


    /**
     *  根据业务流水号查询对应的支付流水号
     * @param bizzSn
     * @return
     */
    public Payment findByBizzSysAndBizzSn(BizzSys bizzSys,String bizzSn);

    /**
     * 更新支付单状态为成功
     * @param paySn
     * @return
     */
    public int updatePaymentPaidSuccess(String paySn);

    /**
     * 更新支付单状态为付款失败
     * @param paySn
     * @return
     */
    public int updatePaymentPaidFailure(String paySn);

    /**
     * 查询支付状态
     * @param bizzSys
     * @param bizzSn
     * @return
     */
    public PayStatus findStatusByBizzSn(BizzSys bizzSys, String bizzSn);

    /**
     * 查询支付状态
     * @param paySn
     * @return
     */
    public PayStatus findStatusByPaySn(String paySn);

    /**
     * 根据paySn查找支付单信息
     * @param paySn
     * @return
     */
    public Payment findByPaySn(String paySn);

    /**
     * 根据paySn查找支付单信息中的同步通知回调url
     * @param paySn
     * @return
     */
    public String findReturnUrlByPaySn(String paySn);

    /**
     * 支付订单存在
     * @param paySn
     * @return
     */
    public boolean isExists(String paySn);
}
