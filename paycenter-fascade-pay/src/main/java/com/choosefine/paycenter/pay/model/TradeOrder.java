package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.TradeOrderStatus;
import com.choosefine.paycenter.common.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Comments：交易订单
 * Author：Jay Chang
 * Create Date：2017/6/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
@Table(name = "paycenter_trade_order")
public class TradeOrder extends BaseModel{
    /**业务系统标识*/
    private BizzSys bizzSys;
    /**业务方业务订单汇总流水号*/
    private String bizzSn;
    /**业务方订单号*/
    private String orderSn;
    /**交易对方的usercode*/
    private String userCode;
    /**交易备注（附言）*/
    private String tradeMemo;
    /**交易金额*/
    private BigDecimal amount;
    /**支付单状态*/
    @Enumerated(EnumType.STRING)
    private TradeOrderStatus status;
    private Timestamp createdAt;
    private Timestamp updatedAt;
}
