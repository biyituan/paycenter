package com.choosefine.paycenter.pay.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:27
 **/
@Getter
@Setter
@ToString
public class TransferQueryVo {
    private String oldRequestSn;
    private String requestSn;
    /**
     * 农行所需参数
     */
    private String payType;
}
