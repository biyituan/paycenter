package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.model.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 支付渠道
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_channel")
public class Channel extends BaseModel implements Serializable {

    /**支付渠道编码*/
    private String channelCode;
    /**行号*/
    private String bankCode;
    /**支付渠道名称(银行名称)*/
    private String bankName;
    /**支付渠道使用状态:1启用;0未启用*/
    @JsonIgnore
    private Integer status;
    /**银行LOGO*/
    private String logo;
    /**PC端银行LOGO（由于PC端银行logo不一样，故添加此字段）*/
    private String logoPc;
    /**银行类型：B2B：企业 B2C：个人*/
    private String payType;
    /**类型：0：企业 1：个人*/
    private Integer type;
}
