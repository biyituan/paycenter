package com.choosefine.paycenter.pay.vo;

import com.choosefine.paycenter.account.vo.AccountBankcardVo;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class PayInfoVo implements Serializable{
    /**需支付的金额*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    /**账户余额*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal availableBalance;
    /**是否已经设置支付密码*/
    private Boolean isSetPayPass;
    /**银行卡信息*/
    private AccountBankcardVo accountBankcardVo;
}
