package com.choosefine.paycenter.pay.service;

import com.alibaba.fastjson.JSONObject;
import org.springframework.transaction.annotation.Transactional;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/5/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayLocalTransactionService {
    //这里认为支付单与账单是关系比较紧密的，故放在一个事务
    public void processPaySuccess(JSONObject payOrderMessage);

    //这里认为支付单与账单是关系比较紧密的，故放在一个事务
    public void processPayFailure(JSONObject payOrderMessage);

    //这里认为充值单与账单是关系比较紧密的，故放在一个事务
    public void processRechargeSuccess(JSONObject payOrderMessage);

    //这里认为充值单与账单是关系比较紧密的，故放在一个事务
    public void processRechargeFailure(JSONObject payOrderMessage);
}
