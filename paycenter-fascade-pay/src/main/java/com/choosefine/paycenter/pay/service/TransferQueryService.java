package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.vo.TransferQueryVo;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:11
 **/
public interface TransferQueryService {
    public <T> T sendTransferQuery(TransferQueryVo transferQueryVo);

    public String getResult(TransferQueryVo transferQueryVo);
}
