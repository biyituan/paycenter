package com.choosefine.paycenter.pay.api.handler;

import com.choosefine.paycenter.account.dto.RechargeDto;
import com.choosefine.paycenter.pay.vo.PayResultVo;

/**
 * 充值处理接口(充值实质为支付，故也放在pay模块)
 * Created by yym on 2017/4/30.
 */
public interface RechargeHandler {
    /**
     * 充值处理
     * @param rechargeDto
     * @return
     */
    public PayResultVo doProcess(RechargeDto rechargeDto);
}
