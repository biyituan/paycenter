package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.common.enums.PayType;

/**
 * Comments：支付配置存储
 * Author：Jay Chang
 * Create Date：2017/4/12
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayConfigStorage {
    public final static String MERCHANT_ID = "MERCHANT_ID";
    public final static String PUBLIC_KEY = "PUBLIC_KEY";
    /**
     * 商户号
     * @return
     */
    String getMerchantId();
    /**
     * 签名方式
     */
    String getSignType();
    /**
     * 公钥(如果所有支付类型统一用一个公钥，那么用这个方法)
     * @return
     */
    String getPublicKey();

    /**
     * 公钥(根据支付类型获取公钥【比如建行的B2C支付，B2B支付的公钥是不一样的】，若不同支付类型，使用不同公钥则用这个方法)
     * @return
     */
    String getPublicKey(PayType payType);

    /**
     * 根据不同支付类型，获取不同该支付类型配置中的值
     * @param payType
     * @param key
     * @return
     */
    String getValue(PayType payType, String key);
}
