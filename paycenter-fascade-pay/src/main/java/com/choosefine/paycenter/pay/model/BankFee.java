package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 手续费
 * Created by dyy on 2017/7/10.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BankFee extends BaseModel implements Serializable {
    /**银行类型*/
    private String bankType;
    /**银行名称*/
    private String bankName;
    /**固定手续费*/
    private BigDecimal confirmedFee;
    /**同行借记卡充值手续费率%*/
    private BigDecimal sameDebitcardFee;
    /**非同行借记卡充值手续费率%*/
    private BigDecimal differentDebitcardFee;
    /**同行贷记卡充值手续费率%*/
    private BigDecimal sameCreditcardFee;
    /**非同行贷记卡充值手续费率%*/
    private BigDecimal differentCreditcardFee;
    /**同行提现手续费（元/笔）*/
    private BigDecimal cashCommission;
    /**非同行提现手续费率*/
    private BigDecimal nonAttendanceRate;
    /**金额区间最小值*/
    private BigDecimal amountRangeMin;
    /**金额区间最大值*/
    private BigDecimal amountRangeMax;

}
