package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.Province;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-08 15:04
 **/
public interface ProvinceService {

    public List<Province> provinceList();

}
