package com.choosefine.paycenter.pay.dto;

import com.choosefine.paycenter.account.dto.PaymentDto;
import com.choosefine.paycenter.common.enums.PayStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * Comments：业务系统的支付请求
 * Author：Jay Chang
 * Create Date：2017/3/8
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ApiModel("支付请求")
public class PayBizzRequestDto extends PaymentDto implements Serializable{
    /**支付密码*/
    @Pattern(regexp = "^\\d{6}$",message = "支付密码必须为6位数字")
    private String payPass;
    @ApiModelProperty("支付通道(即银行)")
    private String channel;
    /**支付单状态*/
    @Enumerated(EnumType.STRING)
    private PayStatus status;
    /**支付类型*/
    private String payType;
    /**可以是支付流水号*/
    private String paySn;
}



