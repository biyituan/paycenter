package com.choosefine.paycenter.pay.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-09 17:41
 **/
@Getter
@Setter
@ToString
public class BankUnionDto {
    private String bankUnionCode;
    private String uBankName;
}
