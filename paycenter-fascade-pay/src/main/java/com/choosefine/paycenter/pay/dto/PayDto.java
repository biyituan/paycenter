package com.choosefine.paycenter.pay.dto;

import com.choosefine.paycenter.common.enums.BizzSys;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class PayDto implements Serializable{
    private String userCode;
    private String payPass;
    @NotNull
    private BizzSys bizzSys;
    @NotBlank
    private String bizzSn;
}
