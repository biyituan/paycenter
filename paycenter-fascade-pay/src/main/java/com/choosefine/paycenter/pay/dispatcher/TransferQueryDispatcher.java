package com.choosefine.paycenter.pay.dispatcher;

import com.choosefine.paycenter.pay.handler.TransferQueryHandler;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:43
 **/
public interface TransferQueryDispatcher {
    public static final String TRANSFER_QUERY_HANDLER_PREFIX = "_TRANSFER_QUERY_HANDLER";

    public TransferQueryHandler getTransferQueryHandler(String channel);

}
