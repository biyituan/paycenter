package com.choosefine.paycenter.pay.service;

import com.choosefine.paycenter.pay.model.Channel;
import java.util.List;

/**
 * Created by Administrator on 2017/4/18.
 */
public interface ChannelService {

    List<Channel> findAllChannelList();

    String findLogoByChannelCode(String channelCode);

    String findLogoByBankCode(String bankCode);

    List<Channel> findChannelList(String payType);

    String findChannelNameByCode(String channelCode);

    String findBankCodeById(Long id);

    String findChannelListDistinct(String bankCode);

    List<Channel> findChannelListDistinct();

    String findChannelCodeByBankCode(String bankCode);

    Channel findChannelByChannelCode(String channelCode);

    Channel findChannelByBankCode(String bankCode);
}
