package com.choosefine.paycenter.pay.model;

import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.common.enums.PayStatus;
import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.choosefine.paycenter.common.model.BaseModel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 支付单
 * Created by Jay Chang on 2017/3/4.
 */
@Setter
@Getter
@ToString
@Table(name = "paycenter_payment")
public class Payment extends BaseModel implements Serializable {
    /**支付单流水号*/
    private String paySn;
    /**业务系统标识*/
    @Enumerated(EnumType.STRING)
    private BizzSys bizzSys;
    /**业务系统业务流水号*/
    private String bizzSn;
    /**账户id*/
    private Long accountId;
    /**账户真实姓名或公司名称*/
    private String accountRealName;
    /**账户的租户编码*/
    private String accountUserCode;
    /**支付通道(即某个银行)*/
    private String channel;
    /**支付类型*/
    private String payType;
    /**支付单状态（支付流水不应该有CLOSED的状态）*/
    @Enumerated(EnumType.STRING)
    private PayStatus status;
    /**金额（单位：分）*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    /**操作员*/
    private String operName;
    /**页面同步通知url*/
    private String returnUrl;
    /**完成时间*/
    private Long finishedAt;
    /**支付单创建时间*/
    private Timestamp createdAt;
    /**支付单最后更新时间*/
    private Timestamp updatedAt;
}
