package com.choosefine.paycenter.pay.api.handler;

import com.choosefine.paycenter.pay.dto.PayBackDto;

/**
 * 支付回调处理接口
 * Created by yym on 2017/4/30.
 */
public interface PayBackHandler {
    /**
     * 处理页面同步回调（不处理状态修改，只重定向客户端浏览器到reutnrUrl）
     */
    String doProcessReturn(final PayBackDto payBackDto);

    /**
     * 处理异步回调(目前规定通过异步回调来更改支付单、充值单、账单状态，更改账户余额，余额日志更新等)
     */
    String doProcessNotify(final PayBackDto payBackDto);

    /**
     * 获取支付失败的原因（如果有的话，没有可的话可以，可以直接返回""）
     * @param payBackDto
     * @return
     */
    String getPayErrorMsg(final PayBackDto payBackDto);

}
