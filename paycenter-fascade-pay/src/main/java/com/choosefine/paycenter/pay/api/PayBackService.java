package com.choosefine.paycenter.pay.api;

import com.choosefine.paycenter.account.dto.OppositeDto;

import java.util.List;

/**
 * Comments：支付回调处理接口
 * Author：Jay Chang
 * Create Date：2017/4/6
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public interface PayBackService {

    /**
     * 处理成功回调
     */
    public void doProcess(String sn);

    /**
     * 处理失败回调
     */
    public void doFailureProcess(String sn);

    /**
     * 更新支付单状态为成功
     *
     * @param paySn
     */
    public void updatePaymentPaidSuccess(String paySn);

    /**
     * 更新支付单状态为失败
     *
     * @param paySn
     */
    public void updatePaymentPaidFail(String paySn);


}
