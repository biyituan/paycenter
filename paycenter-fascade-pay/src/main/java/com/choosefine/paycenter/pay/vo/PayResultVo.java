package com.choosefine.paycenter.pay.vo;

import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/4/6
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class PayResultVo {
    /**浏览器是否需要重定向*/
    private Boolean needRedirect;
    /**充值流水号，或支付流水号*/
    private String sn;
    /**支付的金额*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    private String url;
}
