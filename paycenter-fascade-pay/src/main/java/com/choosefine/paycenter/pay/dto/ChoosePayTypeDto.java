package com.choosefine.paycenter.pay.dto;

import com.choosefine.paycenter.common.dto.BaseDto;
import com.choosefine.paycenter.common.enums.BizzSys;
import com.choosefine.paycenter.pay.service.PaymentService;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/3/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class ChoosePayTypeDto extends BaseDto implements Serializable{
    private String userCode;
    @NotNull(groups = PaymentService.ChoosePayType.class)
    private BizzSys bizzSys;
    @NotBlank(groups = PaymentService.ChoosePayType.class)
    private String bizzSn;
    @NotNull(groups = PaymentService.ChoosePayType.class)
    private String channel;
    @NotNull(groups = PaymentService.ChoosePayType.class)
    private String payType;
    @URL(groups =  PaymentService.ChoosePayType.class)
    private String returnUrl;
}
