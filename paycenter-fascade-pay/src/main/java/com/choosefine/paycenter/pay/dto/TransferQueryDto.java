package com.choosefine.paycenter.pay.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 11:13
 **/
@Getter
@Setter
@ToString
@ApiModel("企业交易转账查询")
public class TransferQueryDto {
    @ApiModelProperty("所查那笔交易的银行流水号")
    private String oldRequestSn;
    @ApiModelProperty("通道")
    private String channel;
    /**
     * 农行所需
     */
    @ApiModelProperty("付款方式（仅农行所需）")
    private String payType;
}
