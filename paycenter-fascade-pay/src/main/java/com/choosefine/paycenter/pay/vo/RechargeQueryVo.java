package com.choosefine.paycenter.pay.vo;

import com.choosefine.paycenter.common.jackson.MoneySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Comments：充值订单查询vo
 * Author：Jay Chang
 * Create Date：2017/4/19
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Getter
@Setter
@ToString
public class RechargeQueryVo {
    /**充值流水号*/
    private String rSn;
    /**充值金额*/
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal amount;
    /**支付通道名称*/
    private String channelName;
    /**充值单状态*/
    private Boolean success;
}
