package com.choosefine.paycenter.settlement.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 10:46
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WithDrawVo {
    private String amount;
    private String withDrawSn;
    private String withDrawBankCardNum;
    private String bankTypeName;
    private String withDrawRealName;
    private String requestSn;
    private String bankUnion;
    private String withdrawBankCode;
}
