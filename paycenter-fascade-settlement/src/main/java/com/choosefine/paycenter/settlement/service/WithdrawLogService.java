package com.choosefine.paycenter.settlement.service;

import com.choosefine.paycenter.settlement.model.WithdrawLog;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-10 11:39
 **/
public interface WithdrawLogService {
    public int addLog(WithdrawLog withdrawLog);


    public int updateWithdrawLogByRequestSn(WithdrawLog withdrawLog,String requestSn);
}
