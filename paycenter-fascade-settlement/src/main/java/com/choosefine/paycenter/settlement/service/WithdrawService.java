package com.choosefine.paycenter.settlement.service;

import com.choosefine.paycenter.account.model.Account;
import com.choosefine.paycenter.settlement.dto.WithdrawDTO;
import com.choosefine.paycenter.settlement.model.Withdraw;
import com.choosefine.paycenter.settlement.vo.WithDrawVo;

import java.util.List;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 11:35
 **/
public interface WithdrawService {
    public long addWithdraw(Withdraw withdraw);

    public long updateWithdraw(Withdraw withdraw);

    public Withdraw selectByWSn(String wSn);

    public int updateAfterWithdrawSuccess(Withdraw withdraw);

    public List<Withdraw> selectByStatus();

    public void checkHasWaittingWithdraw(Long accountId);

}
