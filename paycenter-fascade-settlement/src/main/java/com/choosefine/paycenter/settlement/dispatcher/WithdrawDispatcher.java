package com.choosefine.paycenter.settlement.dispatcher;

import com.choosefine.paycenter.settlement.handler.WithdrawHandler;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-06 10:46
 **/
public interface WithdrawDispatcher {
    public static final String WITHDRAW_HANDLER_PREFIX = "_WITHDRAW_HANDLER";

    public WithdrawHandler getWithdrawHandler(String bankType);
}
