package com.choosefine.paycenter.settlement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-04 20:11
 **/
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawDTO {
    @NotNull(message = "操作人不能为空")
    private String operName;
    @NotNull
    private Long accountId;
    @NotNull
    private String amount;
    @NotNull
    private Long bankCardId;
    @NotNull
    private String password;
    private String ubankName;
    /**
     *大额的时候需要填写(联行号)
     */
    private String bankUnion;
}
