package com.choosefine.paycenter.settlement.service;

import com.choosefine.paycenter.settlement.vo.WithDrawVo;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 16:12
 **/
public interface WithdrawBankService {
    public <T> T  sendWithdrawRequest(WithDrawVo withDrawVo);
}
