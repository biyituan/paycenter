package com.choosefine.paycenter.settlement.service;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-11 15:50
 **/
public interface WithDrawBankRoutingService {
    public String chooseChannel(String bankCardType);
}
