package com.choosefine.paycenter.settlement.handler;

import com.choosefine.paycenter.settlement.dto.WithdrawDTO;
import com.choosefine.paycenter.settlement.vo.WithDrawVo;

/**
 * @author 潘钱钦（qianqinpan@outlook.com）
 * @create 2017-05-05 15:53
 **/
public interface WithdrawHandler {
    public WithDrawVo buildWithDraw(WithdrawDTO withdrawDTO, String userCode);
}
